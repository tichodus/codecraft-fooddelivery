<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $username_missing = false;
    $password_missing = false;
    $incorrect_input  = false;

    if(isset($_POST['submit']))
    {
        if(empty($_POST['username']))
            $username_missing = true;
        else if(empty($_POST['password']))
            $password_missing = true;
        else
        {
            $username = $_POST['username'];
            $password = $_POST['password'];

            $login = login($username, $password);
            if(empty($login) === true)
                $incorrect_input = true;
            else
            {
                $_SESSION['user_id'] = $login;
                header('Location: index.php');
                exit();
            }
        }
    }
    
    $smarty->assign('username_missing', $username_missing);
    $smarty->assign('password_missing', $password_missing);
    $smarty->assign('incorrect_input', $incorrect_input);
    $smarty->display('login.tpl');
    include 'includes/overall/footer.php';
?>