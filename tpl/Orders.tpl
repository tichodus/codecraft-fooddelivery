<!DOCTYPE html>
<html>
<head>
	<title>CodeCraft takmičenje</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/mainstyle.css">

</head>
<body>
	<input id="choose_company" type="hidden" name="choose_company" value="[[$id_company]]">
    <div id="change_info">
        <h3>Generate list of ordered meals</h3>
    </div>
     <button onclick="return_invoice()" class="w3-btn-block w3-sunshine">Click here to generate</button>
     <div id="invoice_display"></div>
     
     

		<style>
		table {
		    font-family: arial, sans-serif;
		    border-collapse: collapse;
		    width: 100%;
		}

		td, th {
		    border: 1px solid #dddddd;
		    text-align: left;
		    padding: 8px;
		}

		tr:nth-child(even) {
		    background-color: #dddddd;
		}
		</style>

     <script type="text/javascript">
     	function selected_company()
     	{
     		var e = document.getElementById("choose_company");
            var choosed_company = e.value;
            return choosed_company;

     	}
     	

     	function return_invoice()
     	{
     		
     		//alert("SAAA");
     		var selected_company1= selected_company();
     		
                var xhttp = new XMLHttpRequest();
			    xhttp.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {
			            document.getElementById("invoice_display").innerHTML = this.responseText;
			        }
			    };
			    xhttp.open("GET", "database/getOrderReport.php?selected_company="+selected_company1+"", true);
			    xhttp.send();

     	}
     	
     	

     </script>
</body>
</html>