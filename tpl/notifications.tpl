<div id="rounded-border-not">
    <h3>Notifications</h3>
</div>
<hr>
[[foreach $alerts as $admin_alerts_id => $alert]]
<div id="user-wrapper[[$alert->user_id]]">
    <div class="row">
        <div class="col-md-2">
            <img src="[[$alert->image_path]]" alt="[[$alert->first_name]]" class="img-responsive" width="100" height="100">
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <b>[[$alert->first_name]] [[$alert->last_name]]</b> requested to join your team.
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="left-button">
                        <button class="accept w3-btn-block w3-fresh" value="[[$alert->user_id]]" id="accept">Accept</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="right-button">
                        <button class="decline w3-btn-block w3-red" value="[[$alert->user_id]]" id="decline">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
[[/foreach]]

<script>
    $(".accept").on('click', function(){
        var user_id = $(this).val();
        var div = "user-wrapper";
        div = div + user_id;

        $.ajax({ type: "POST",
                url: "accept-employee.php",
                data: { user_id: user_id },
                cache: false,
                success: function(response)
                {
                    document.getElementById(div).remove();
                }
        });
    });

    $(".decline").on('click', function(){
        var user_id = $(this).val();
        var div = "user-wrapper";
        div = div + user_id;

        $.ajax({ type: "POST",
                url: "decline-employee.php",
                data: { user_id: user_id },
                cache: false,
                success: function(response)
                {
                    document.getElementById(div).remove();
                }
        });
    });
</script>