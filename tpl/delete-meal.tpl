[[if $no_permission == true]]
    <div style="text-align:center;">
        <h1>403 ERROR</h1>
        <hr>
        <p>Permission denied!</p>
    </div>
[[else]]
    <div id="list-of-users">
        <h3>List of meals</h3>
    </div>

    <form class="w3-container w3-card-4" action="delete-meal.php" method="POST" enctype="multipart/form-data"> 
        <div class="table-responsive"> 
            <table class="table">
                <tr>
                    <th>Title</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Change</th>
                    <th>Delete</th>
                </tr>
                [[foreach $meals as $food_ID => $f]]
                    <tr>
                        <td>[[$f->title]]</td>
                        <td>[[$f->quantity]] [[$f->unit_of_measure]]</td>
                        <td>[[$f->price]] [[$f->price_unit]]</td>
                        <td>
                            <button value="[[$f->food_ID]]" class="w3-btn-block-smaller w3-sunshine"  name="change_button">Change</button>
                        </td>
                        <td>
                            <button value="[[$f->food_ID]]" class="w3-btn-block-smaller w3-red" name="delete_button">Delete</button>
                        </td>
                    </tr>
                [[/foreach]]
            </table>
        </div>
        [[if $all_fields_required == true]]
            <div id="error">
                <h3>All fields marked with <span id="required_fields">*</span> are required. Please try again</h3>
            </div>
        [[else if $quantity_must_be_numeric == true]]
            <div id="error">
                <h3>Quantity must be numeric value. Please try again</h3>
            </div>
        [[else if $price_must_be_numeric == true]]
            <div id="error">
                <h3>Price must be numeric value. Please try again</h3>
            </div>
        [[else if $img_type_not_suported == true]]
            <div id="error">
                <h3>Image type not suported. Please use .png or .jpg pictures</h3>
            </div>
        [[/if]]
        [[if $are_you_sure == true]]
            <div id="error">
                <h3>Are you sure you want to delete [[$meal_title]]?</h3>
                <div class="row">
                    <div class="col-md-6">
                        <button class="w3-btn-block w3-red" name="no_button">No</button>
                    </div>
                    <div class="col-md-6">
                        <button class="w3-btn-block w3-fresh" name="yes_button" value="[[$meal_id]]">Yes</button>
                    </div>
                </div>
            </div>
       [[else]]
            [[if $change == true]]
                <div id="info-for-change">
                    <h4>[[$meal_title]] Information</h4>

                    <label for="title">Title:</label>
                    <input type="text" name="title" value="[[$meal_title]]">

                    <label for="description">Description:</label>
                    <textarea name="description" id="" cols="30" rows="10">[[$meal_description]]</textarea>

                    <label for="quantity">Quantity<span id="required_fields">*</span>:</label>  
                    <input type="text" name="quantity" maxlength="4" value="[[$meal_quantity]]">

                    <label for="unit_of_measure">Unit of measure<span id="required_fields">*</span>:</label>  
                    <select name="unit_of_measure" id="">
                        [[if $meal_unit_of_measure == 'grams']]
                            <option value="grams">Grams</option>
                            <option value="kg">Kg</option>
                            <option value="piece">Piece</option>
                        [[/if]]

                        [[if $meal_unit_of_measure == 'kg']]
                            <option value="kg">Kg</option>
                            <option value="grams">Grams</option>
                            <option value="piece">Piece</option>
                        [[/if]]

                        [[if $meal_unit_of_measure == 'piece']]
                            <option value="piece">Piece</option>
                            <option value="grams">Grams</option>
                            <option value="kg">Kg</option>                        
                        [[/if]]

                    </select>

                    <label for="category">Category<span id="required_fields">*</span>:</label>  
                    <select name="category" id="">
                        <option value="[[$meal_category_id]]">[[$meal_category_type]]</option>
                        [[foreach $categories as $category_id => $category]]
                            [[if $meal_category_id != $category->category_id]]
                                <option value="[[$category->category_id]]">[[$category->type]]</option>
                            [[/if]]
                        [[/foreach]]
                    </select>

                    <label for="price">Price<span id="required_fields">*</span>:</label>  
                    <input type="text" name="price" maxlength="6" value="[[$meal_price]]">

                    <label for="price_unit">Price unit<span id="required_fields">*</span>:</label>  
                    <select name="price_unit" id="">
                        <option value="eur">EUR</option>
                    </select>

                    <label for="image">Picture<span id="required_fields">*</span></label>
                    <input type="file" name="image" id="">

                    <button name="change_final" id="change_final" class="w3-btn-block w3-fresh" value="[[$meal_id]]">Change</button>
                </div>
            [[/if]]
        [[/if]]
    </form>
[[/if]]