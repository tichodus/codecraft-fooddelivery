
<div id="list-of-users">
    <h3>List of users</h3>
</div>

<form class="w3-container w3-card-4" action="list-of-users.php" method="POST"> 
    <div class="table-responsive"> 
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Privileges</th>
                <th>Status</th>
                [[if $company_type == 0]]
                    <th>Spent</th>
                [[/if]]
                <th>Change</th>
                <th>Delete</th>
            </tr>
            [[foreach $users as $user_id=>$user]]
                <tr>
                    <td>[[$user->user_id]]</td>
                    <td>[[$user->username]]</td>
                    <td>[[$user->first_name]]</td>
                    <td>[[$user->last_name]]</td>
                    <td>[[$user->email]]</td>
                    [[if $user->privileges == 0]]
                        <td>Employee</td>
                    [[else]]
                        <td>Admin</td>
                    [[/if]]

                    [[if $user->active == 0]]
                        <td id="unverified">Unverified</td>
                    [[else if $user->active == 1]]
                        <td id="verified">Verified</td>
                    [[/if]]
                    [[if $company_type == 0]]
                        <td>[[$user->spent]]</td>
                    [[/if]]
                    <td>
                        <button value="[[$user->user_id]]" class="w3-btn-block-smaller w3-sunshine"  name="change_button">Change</button>
                    </td>
                    <td>
                        <button value="[[$user->user_id]]" class="w3-btn-block-smaller w3-red" name="delete_button">Delete</button>
                    </td>
                </tr>
            [[/foreach]]
        </table>
    </div>
    [[if $are_you_sure == true]]
        <div id="error">
            <h3>Are you sure you want to delete employee [[$first_name]] [[$last_name]]?</h3>
            <div class="row">
                <div class="col-md-6">
                    <button class="w3-btn-block w3-red" name="no_button">No</button>
                </div>
                <div class="col-md-6">
                    <button class="w3-btn-block w3-fresh" name="yes_button" value="[[$user_id_delete]]">Yes</button>
                </div>
            </div>
        </div>
    [[else]]
        [[if $change == true]]
            <div id="info-for-change">
                <h4>[[$first_name]] Information</h4>

                [[if $all_fields_required == true]]
                    <div id="error">
                        <h3>Some fields are missing. Please try again</h3>
                    </div>
                [[/if]]

                <label for="first_name">First Name:</label>
                <input type="text" name="first_name" value="[[$first_name]]">

                <label for="last_name">Last Name:</label>
                <input type="text" name="last_name" value="[[$last_name]]">

                <label for="username">Username:</label>
                <input type="text" name="username" value="[[$username]]">

                <label for="email">Email:</label>
                <input type="text" name="email" value="[[$email]]">

                <label for="status">Status:</label>
                <select name="status">
                    [[if $status == 0]]
                        <option value="0">Unverified</option>
                        <option value="1">Verified</option>
                    [[else if $status == 1]]
                        <option value="1">Verified</option>
                        <option value="0">Unverified</option>
                    [[/if]]
                </select>

                <button name="change_final" id="change_final" class="w3-btn-block w3-fresh" value="[[$user_id_pom]]">Change</button>
            </div>
        [[/if]]
    [[/if]]
</form>