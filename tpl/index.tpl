<!DOCTYPE html>
<html>
<head>
    <title>CodeCraft takmičenje</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/mainstyle.css">
</head>
<body>
    [[if $not_registered == true]]
        <div class="background-wrapper">
            <div id="home-log-in">                
                <h2>Log in</h2>
                <form action="login.php" method="POST">
                    <input type="text" name="username" id="" placeholder="username"><br>
                    <input type="password" name="password" id="" style="margin-top:10px;" placeholder="password"><br>
                    <button name="submit" class="w3-btn-block w3-fresh" style="width: 20%">Log in</button>
                </form>
                </div>
            </div>
        </div>
    [[else]]
        [[if $ketering_or_it == 0]]
            <div id="rounded-border">        
                <div class="row">
                    <div class="col-md-6">
                        <h3>Choose day for ordering</h3>
                        <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                            [[for $i=1 to $maxDays]]
                                [[if $i eq $today_day+1]]
                                        <option selected="selected" value="[[$i]]">[[$i]]</option>
                                [[else]]
                                        <option value="[[$i]]">[[$i]]</option>
                                [[/if]]
                            [[/for]]
                        </select>
                        <p id='mon_yer' style="display: inline-block;">/[[$today_month]]/[[$today_year]]</p>
                        <br>
                    </div>
                    <div class="col-md-6">
                        Your company allows you to order
                        [[if $type_of_service == 0]]
                            <b><span id="fresh">breakfast</span></b>
                            <div style="display:none;">
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            </div>
                        [[else if $type_of_service == 1]]
                            <b><span id="fresh">lunch</span></b>
                            <div style="display:none;">
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" >
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            </div>
                        [[else]]
                            both <b><span id="fresh">breakfast</span></b> and <b><span id="fresh">breakfast</span></b>
                            <p>Please choose what kind of order do you want</p>
                            <div class="container_automatic">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                    <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                    <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                    <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                </div>
                            </div>
                        [[/if]]

                    </div>
                </div>
            </div>
            <hr>
        [[/if]]

        [[if $number_of_meals < 7]]
            [[for $x=0 to $number_of_meals - 1]]
                [[if ($x % 2) == 0]]
                        <div class="row"> 
                            [[/if]]
                                <div class="col-md-6">
                                    <div id="one-row-index">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img onclick="get([[$meals[$x]->food_ID]], '[[$meals[$x]->title]]')" src="[[$meals[$x]->image]]" width="200" height="200" class="img-responsive" style="width:100%;" />
                                            </div>
                                            [[if $ketering_or_it == 0]]
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=[[$meals[$x]->food_ID]]">[[$meals[$x]->title]]</a></h2>
                                                    <hr>
                                                    <p>Price: [[$meals[$x]->price]] [[$meals[$x]->price_unit]]</p>
                                                    <p>Category: [[$meals[$x]->category]]</p>
                                                    <p>By: <a href="menu.php?id=[[$meals[$x]->pib]]">[[$meals[$x]->company]]</a></p>
                                                    <button type="button" class="add btn btn-default btn-sm" value="[[$meals[$x]->food_ID]]">
                                                        <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                                                    </button>
                                                </div>
                                            [[else]]
                                                <div class="col-md-6">
                                                    <h2>[[$meals[$x]->title]]</h2>
                                                    <hr>
                                                    <p>Price: [[$meals[$x]->price]] [[$meals[$x]->price_unit]]</p>
                                                    <p>Category: [[$meals[$x]->category]]</p>
                                                </div>
                                            [[/if]]
                                        </div>
                                    </div>
                                </div>
                            [[if ($x % 2) == 1 || $x == $number_of_meals - 1]]
                        </div>
                    <hr>
                [[/if]]
            [[/for]]
        [[else]]
            [[for $x=0 to 5]]
                [[if ($x % 2) == 0]]
                        <div class="row"> 
                            [[/if]]
                                <div class="col-md-6">
                                    <div id="one-row-index">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img onclick="get([[$meals[$x]->food_ID]], '[[$meals[$x]->title]]')" src="[[$meals[$x]->image]]" width="200" height="200" class="img-responsive" style="width:100%;" />
                                            </div>
                                            [[if $ketering_or_it == 0]]
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=[[$meals[$x]->food_ID]]">[[$meals[$x]->title]]</a></h2>
                                                    <hr>
                                                    <p>Price: [[$meals[$x]->price]] [[$meals[$x]->price_unit]]</p>
                                                    <p>Category: <a href="index.php?category=[[$meals[$x]->category_id]]">[[$meals[$x]->category]]</a></p>
                                                    <p>By: <a href="menu.php?id=[[$meals[$x]->pib]]">[[$meals[$x]->company]]</a></p>
                                                    <button type="button" class="add btn btn-default btn-sm" value="[[$meals[$x]->food_ID]]">
                                                        <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                                                    </button>
                                                </div>
                                            [[else]]
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=[[$meals[$x]->food_ID]]">[[$meals[$x]->title]]</a></h2>
                                                    <hr>
                                                    <p>Price: [[$meals[$x]->price]] [[$meals[$x]->price_unit]]</p>
                                                    <p>Category: <a href="index.php?category=[[$meals[$x]->category_id]]">[[$meals[$x]->category]]</a></p>
                                                    <p>By: <a href="menu.php?id=[[$meals[$x]->pib]]">[[$meals[$x]->company]]</a></p>
                                                </div>
                                            [[/if]]
                                        </div>
                                    </div>
                                </div>
                            [[if ($x % 2) == 1 || $x == $number_of_meals - 1]]
                        </div>
                    <hr>
                [[/if]]
            [[/for]]
        [[/if]]
    
        <div id="shoping_card_dispay"></div>

        <script>

            $(".add").on('click', function(){
                var food_id = $(this).val();
                var user_id = [[$user_id]];

                var today = [[$today_day]];
                var month = [[$today_month]];
                var year  = [[$today_year]];

                var type_of_service = $('input[type=radio][name=type_of_plan]:checked').val();

                var e = document.getElementById("order_for_date");
                var order_date = e.options[e.selectedIndex].value;
                if (order_date<10) 
                {
                    order_date = 0+order_date;
                }

                $.ajax({ type: "POST",
                        url: "insert-into-cart.php",
                        data: { food_id: food_id, user_id: user_id, order_date: order_date, type_of_service: type_of_service},
                        cache: false,
                        success: function(response)
                        {
                            document.getElementById('badge-change').innerHTML = response;
                        }
                });

            });
    
            $( ".order_for_date_c" ).change(function() {
                var e = document.getElementById("order_for_date");
                var order_date = e.options[e.selectedIndex].value;

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; 
        
                var yyyy = today.getFullYear();
                if(order_date>dd)
                {
                    if (mm<10)
                        document.getElementById("mon_yer").innerHTML = "/0"+mm+"/"+yyyy;
                    else
                        document.getElementById("mon_yer").innerHTML = "/"+mm+"/"+yyyy;
                }
                else
                    document.getElementById("mon_yer").innerHTML = "/"+(mm+1)+"/"+yyyy;
            });
        </script>
    [[/if]]
</body>
</html>