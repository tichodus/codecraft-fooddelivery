[[if $one_meal == true]]
<div class="row">
    <div class="col-md-9">
            <div class="row">
                <div class="col-md-5">
                    <img src="[[$food[0]->image_path]]" alt="[[$food[0]->title]]" class="img-responsive" style="width:100%;">
                </div>
                <div class="col-md-7">
                    [[if $ketering_or_it == 0]]
                        <div class="col-md-6">
                            <h2>[[$food[0]->title]]</h2>
                            <hr>
                            <p>Price: [[$food[0]->price]] [[$food[0]->price_unit]]</p>
                                <fieldset class="rating">
                                    [[if $user_rate == 1]]
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" checked/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[else if $user_rate == 2]]
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" checked/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[else if $user_rate == 3]]
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" checked/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[else if $user_rate == 4]]
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" checked/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[else if $user_rate == 5]]
                                        <input type="radio" id="star5" name="rating" value="5" checked /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[else]]
                                        <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    [[/if]]
                                </fieldset>
                            <p>Quantity: [[$food[0]->quantity]] [[$food[0]->unit_of_measure]]</p>
                            <hr>
                            <button type="button" class="add btn btn-default btn-sm" value="[[$food[0]->food_ID]]">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                            </button>
                        </div>
                    [[else]]
                        <div class="col-md-7">
                            <h2>[[$food[0]->title]]</h2>
                            <hr>
                            <p>Price: [[$food[0]->price]] [[$food[0]->price_unit]]</p>
                            <p>Quantity: [[$food[0]->quantity]]</p>
                            <p>Category: [[$food[0]->category]]</p>
                        </div>
                    [[/if]]
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-top:10px;">
                    <h4>Meal description</h4>
                    <p style="text-align: justify;">[[$food[0]->description]]</p>
                </div>
            </div>
            <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Comments</h4>
                    </div>
                </div>
                [[if $allow_comments == true]]
                <div id="comments-wrapper">      
                    <div class="row">
                        <div class="col-md-12">
                            <textarea name="comment" id="comment" cols="10" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <button name="submit_comment" value="[[$food_id]]" class="submit_comment w3-btn-block w3-sunshine">Post comment</button>
                        </div>
                    </div>
                </div>
                [[/if]]
            <hr>
            [[if $no_comments == true]]
                <p>No new comments</p>
            [[/if]]
            [[foreach $comments as $comment_id => $comment]]
                <div class="row">
                    <div class="col-md-1">
                        <img src="[[$comment->image_path]]" alt="[[$comment->user_first_name]]" width="50" height="50"> 
                    </div>
                    <div class="col-md-11">
                        <h6>[[$comment->user_first_name]] says:</h6>
                        [[$comment->comment]]
                    </div>
                </div>
                <hr>
            [[/foreach]]
    </div>
        <div class="col-md-3">
            [[if $ketering_or_it == 0]]
                <div id="rounded-border">        
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="font-size: 18px;">Choose day for ordering</h3>
                            <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                                [[for $i=1 to $maxDays]]
                                    [[if $i eq $today_day+1]]
                                            <option selected="selected" value="[[$i]]">[[$i]]</option>
                                    [[else]]
                                            <option value="[[$i]]">[[$i]]</option>
                                    [[/if]]
                                [[/for]]
                            </select>
                            <p id='mon_yer' style="display: inline-block;">/[[$today_month]]/[[$today_year]]</p>
                            <br>
                        </div>
                        <div class="col-md-12">
                            [[if $type_of_service == 0]]
                                <b><span id="fresh">breakfast</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            [[else if $type_of_service == 1]]
                                <b><span id="fresh">lunch</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast">
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            [[else]]
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            [[/if]]

                        </div>
                    </div>
                </div>
                <hr>
            [[/if]]
        </div>
</div>
[[else $few_meals == true]]
<div class="row">
    <div class="col-md-9">
        [[foreach $meals as $m]]
            <div class="row">
                <div class="col-md-5">
                    <img src="[[$m->image_path]]" alt="[[$m->title]]" class="img-responsive" style="width:100%;">
                </div>
                <div class="col-md-7">
                    [[if $ketering_or_it == 0]]
                        <div class="col-md-6">
                            <h2><a href="menu.php?foodid=[[$m->food_ID]]">[[$m->title]]</a></h2>
                            <hr>
                            <p>Price: [[$m->price]] [[$m->price_unit]]</p>
                            <p>Category: [[$m->category]]</p>
                            <button type="button" class="add btn btn-default btn-sm" value="[[$m->food_ID]]">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                            </button>
                        </div>
                    [[else]]
                        <div class="col-md-7">
                            <h2><a href="menu.php?foodid=[[$m->food_ID]]">[[$m->title]]</a></h2>
                            <hr>
                            <p>Price: [[$meals[$x]->price]] [[$m->price_unit]]</p>
                            <p>Category: [[$m->category]]</p>
                        </div>
                    [[/if]]
                </div>
            </div>
            <hr>
        [[/foreach]]
    </div>
        <div class="col-md-3">
            [[if $ketering_or_it == 0]]
                <div id="rounded-border">        
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Choose day for ordering</h3>
                            <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                                [[for $i=1 to $maxDays]]
                                    [[if $i eq $today_day+1]]
                                            <option selected="selected" value="[[$i]]">[[$i]]</option>
                                    [[else]]
                                            <option value="[[$i]]">[[$i]]</option>
                                    [[/if]]
                                [[/for]]
                            </select>
                            <p id='mon_yer' style="display: inline-block;">/[[$today_month]]/[[$today_year]]</p>
                            <br>
                        </div>
                        <div class="col-md-12">
                            [[if $type_of_service == 0]]
                                <b><span id="fresh">breakfast</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            [[else if $type_of_service == 1]]
                                <b><span id="fresh">lunch</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast">
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            [[else]]
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            [[/if]]

                        </div>
                    </div>
                </div>
                <hr>
            [[/if]]
        </div>
</div>
[[/if]]

<script>
    $(".submit_comment").on('click', function(){
        var food_id = $(this).val();
        var user_id = [[$user_id]];
        var comment = $('#comment').val();

        $.ajax({ type: "POST",
                url: "insert-comment.php",
                data: { food_id: food_id, user_id: user_id, comment: comment},
                cache: false,
                success: function(response)
                {
                    document.getElementById('comments-wrapper').innerHTML = 'Your comment has been successfully submited';
                }
        });
    });

    $(".add").on('click', function(){
        var food_id = $(this).val();
        var user_id = [[$user_id]];

        var today = [[$today_day]];
        var month = [[$today_month]];
        var year  = [[$today_year]];

        var type_of_service = $('input[type=radio][name=type_of_plan]:checked').val();

        var e = document.getElementById("order_for_date");
        var order_date = e.options[e.selectedIndex].value;
        if (order_date<10) 
        {
            order_date = 0+order_date;
        }

        $.ajax({ type: "POST",
                url: "insert-into-cart.php",
                data: { food_id: food_id, user_id: user_id, order_date: order_date, type_of_service: type_of_service},
                cache: false,
                success: function(response)
                {
                    document.getElementById('badge-change').innerHTML = response;
                }
        });

    });

    $(document).ready(function() {
        $('input[type=radio][name=rating]').change(function() {
            var value = $('input[type=radio][name=rating]:checked').val();
            var food_id = [[$food_id]];
            var user_id = [[$user_id]];

            $.post("rate-meal.php", {value: value, food_id: food_id, user_id: user_id});
    })});
</script>