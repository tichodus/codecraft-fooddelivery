<script>
    $(document).on('click', '#change_button', function(){
        var first_name = $('#first_name').text();
        var last_name = $('#last_name').text();
        var email = $('#email').text();

        $('#change_info').remove();
        var override = '<form action="profile.php" method="POST" enctype="multipart/form-data"><div id="info-for-change"><h3>Change your information</h3> <label for="first_name">First Name:</label>  <input type="text" name="first_name" value="' + first_name + '"> <label for="last_name">Last Name:</label> <input type="text" name="last_name" value="'+ last_name +'"> <label for="old_password">Old password:</label> <input type="password" name="old_password"> <label for="new_password">New password:</label> <input type="password" name="new_password"> <label for="email">Email:</label> <input type="text" name="email" value="'+ email +'"> <label for="image">Image:</label> <input type="file" name="image"> <button name="final_change_button" class="w3-btn-block w3-teal">Submit</button>';
        document.getElementById("change-wrapper").innerHTML = override;
    });

    $(document).ready(function() {
        $('input[type=radio][name=automatic_delivery_radio_buttons]').change(function() {
            var value = $('input[type=radio][name=automatic_delivery_radio_buttons]:checked').val();
            var hidden_user_id = $('#hidden_user_id').val();
            $.post("update_automatic_delivery.php", {hidden_user_id: hidden_user_id, automatic_delivery: value});
    })});
</script>

[[if $redirect == true]]
    <div id="success">
        <h3>You have successfully changed your information.</h3>
    </div>
[[/if]]
[[if $empty_first_name == true]]
    <div id="error">
        <h3>First Name is missing. Please try again</h3>
    </div>
[[else if $empty_last_name == true]]
    <div id="error">
        <h3>Last Name is missing. Please try again</h3>
    </div>
[[else if $empty_email == true]]
    <div id="error">
        <h3>Email is missing. Please try again</h3>
    </div>
[[else if $empty_old_password == true]]
    <div id="error">
        <h3>Old password is missing. Please try again</h3>
    </div>
[[else if $empty_new_password == true]]
    <div id="error">
        <h3>New password is missing. Please try again</h3>
    </div>
[[else if $wrong_old_password == true]]
    <div id="error">
        <h3>Wrong old password. Please try again</h3>
    </div>
[[/if]]

<div id="change-wrapper">
    <div id="change_info">
        <h3>Information</h3>
        <div class="row">
                <div class="col-md-4">
                <label for="first_name">First Name:</label> <div id="first_name">[[$first_name]]</div>
                <br>

                <label for="last_name">Last Name:</label> <div id="last_name">[[$last_name]]</div>
                <br>

                <label for="username">Username:</label> <div id="username">[[$username]]</div>
                <br>

                <label for="email">Email:</label> <div id="email">[[$email]]</div>
                <br>

                <!-- <button name="change_button" id="change_button" class="w3-btn">Change</button> -->
            </div>
            <div class="col-md-4">
                <div class="w3-card-2" style="width:75%; display: block;margin-left: auto;margin-right: auto">
                  <img src="[[$image_path]]?t=[[$time]]" alt="" style="width:100%;">
                  <div class="w3-container w3-center">
                    [[if $privileges == 0]]
                        <h4><b>employee</b></h4>
                        <p>[[$company_name]]</p>
                    [[else]]
                        <h4><b>admin</b></h4>
                        <p>[[$company_name]]</p>
                    [[/if]]
                  </div>
                </div>
            </div>
            [[if $company_type == 0]]
                <div class="col-md-4">
                    <div id="automatic_delivery">
                        <h4>Automatic delivery</h4>
                        <div class="container_automatic">
                            <div class="switch">
                                [[if $automatic_delivery == 0]]
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="0" id="disable" checked>
                                    <label for="disable" class="switch-label switch-label-off">Disable</label>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="1" id="enable">
                                    <label for="enable" class="switch-label switch-label-on">Enable</label>
                                    <input type="hidden" id="hidden_user_id" value="[[$user_id]]">
                                [[else]]
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="0" id="disable" >
                                    <label for="disable" class="switch-label switch-label-off">Disable</label>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="1" id="enable" checked>
                                    <label for="enable" class="switch-label switch-label-on">Enable</label>
                                    <input type="hidden" id="hidden_user_id" value="[[$user_id]]">
                                [[/if]]

                            </div>
                        </div>
                    </div>
                </div>
            [[/if]]
        </div>
        <div class="row">
            <div class="col-md-6">
                <button name="change_button" id="change_button" class="w3-btn">Change</button>
            </div>
        </div>
    </div>
</div>

<script>
    setTimeout(function(){
      $('#success').remove();
    }, 4000);
</script>