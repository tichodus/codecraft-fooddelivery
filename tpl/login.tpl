[[if $username_missing == true]]
    <div id="error">
        <h3>Username is missing. Please try again</h3>
    </div>
[[else if $password_missing == true]]
    <div id="error">
        <h3>Password is missing. Please try again</h3>
    </div>
[[else if $incorrect_input == true]]
    <div id="error">
        <h3>Incorrect username or password. Please try again</h3>
    </div>
[[/if]]
<div id="registration">
    <h3>Login form</h3>
</div>

<form action="login.php" method="POST" id="login">
    <label for="username">Username</label>
    <input type="text" name="username" id="">

    <label for="password">Password</label>
    <input type="password" name="password">
    <button name="submit" class="w3-btn-block w3-sunshine">Log in</button>
</form>