[[if $no_permission == true]]
    <div style="text-align:center;">
        <h1>403 ERROR</h1>
        <hr>
        <p>Permission denied!</p>
    </div>
[[else]]
    <form action="add-new-meal.php" method="POST" enctype="multipart/form-data">
        [[if $all_fields_required == true]]
            <div id="error">
                <h3>All fields marked with <span id="required_fields">*</span> are required. Please try again</h3>
            </div>
        [[else if $quantity_must_be_numeric == true]]
            <div id="error">
                <h3>Quantity must be numeric value. Please try again</h3>
            </div>
        [[else if $price_must_be_numeric == true]]
            <div id="error">
                <h3>Price must be numeric value. Please try again</h3>
            </div>
        [[else if $img_type_not_suported == true]]
            <div id="error">
                <h3>Image type not suported. Please use .png or .jpg pictures</h3>
            </div>
        [[/if]]

        [[if $success == true]]
            <div id="success">
                <h3>You have successfully published new meal to your menu.</h3>
            </div>
        [[/if]]

        <div id="info-for-change">
            <h3>Add new meal to your menu</h3> 

            <label for="title">Title<span id="required_fields">*</span>:</label>  
            <input type="text" name="title">

            <label for="description">Description<span id="required_fields">*</span>:</label>  
            <textarea name="description" id="" cols="30" rows="10"></textarea>

            <label for="quantity">Quantity<span id="required_fields">*</span>:</label>  
            <input type="text" name="quantity" maxlength="4" placeholder="numeric value...">

            <label for="unit_of_measure">Unit of measure<span id="required_fields">*</span>:</label>  
            <select name="unit_of_measure" id="">
                <option value="not_selected">Select unit...</option>
                <option value="grams">Grams</option>
                <option value="kg">Kg</option>
                <option value="piece">Piece</option>
            </select>

            <label for="category">Category<span id="required_fields">*</span>:</label>  
            <select name="category" id="">
                <option value="not_selected">Select category...</option>
                [[foreach $categories as $category_id => $category]]
                    <option value="[[$category->category_id]]">[[$category->type]]</option>
                [[/foreach]]
            </select>

            <label for="price">Price<span id="required_fields">*</span>:</label>  
            <input type="text" name="price" maxlength="6" placeholder="numeric value...">

            <label for="price_unit">Price unit<span id="required_fields">*</span>:</label>  
            <select name="price_unit" id="">
                <option value="eur">EUR</option>
            </select>

            <label for="image">Picture<span id="required_fields">*</span></label>
            <input type="file" name="image" id="">
            
            <button name="submit" class="w3-btn-block w3-sunshine">Submit</button>
        </div>
    </form>
[[/if]]

<script>
    setTimeout(function(){
      $('#success').remove();
    }, 4000);
</script>