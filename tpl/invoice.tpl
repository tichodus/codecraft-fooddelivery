<!DOCTYPE html>
<html>
<head>
	<title>Craft_food - Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/mainstyle.css">
    <script type="text/javascript" src="jspdf/libs/sprintf.js"></script>
	<script type="text/javascript" src="jspdf/jspdf.js"></script>
	<script type="text/javascript" src="jspdf/libs/base64.js"></script>
	<script type="text/javascript" src="tableExport.js"></script>
	<script type="text/javascript" src="jquery.base64.js"></script>

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        select{
            display: inline-block;
            min-width:265px;
            min-height: 45px;
            width: 100%;
            border-width: 3px;
            border-color: rgba(50, 50, 50, 0.14);
            margin: 10px 10px 10px 0px;
        }
    </style>
</head>
<body>
    <div id="change_info">
	   <h3>Make invoice report</h3>
    </div>
	<form name="" action="database/export_pdf.php" method="post">
	<select id="days_display" name="days_display">
		<option value="1">daily</option>
		<option value="7">weekly</option>
		<option value="30">monthly</option>
	</select>
	<select id="choose_company" name="choose_company">
	 [[foreach from=$all_companies item=item_com]]
        <option id="company[[$item_com->pib]]" value="[[$item_com->pib]]">[[$item_com->company_name]]</option>
     [[/foreach]]
     </select>

     	<input type="submit" name="exportPDF" value="Export to pdf" class="w3-btn-block w3-sunshine">
     </form>


     <button onclick="return_invoice()" class="w3-btn-block w3-red">Read</button>
     <hr>
     <div id="invoice_display"></div>
     

     <script type="text/javascript">
     	function selected_company()
     	{
     		var e = document.getElementById("choose_company");
            var choosed_company = e.options[e.selectedIndex].value;
            return choosed_company;

     	}
     	function selected_days()
     	{
     		var e = document.getElementById("days_display");
            var choosed_days = e.options[e.selectedIndex].value;
            return choosed_days;

     	}

     	function return_invoice()
     	{
     		
     		var selected_days1=selected_days();
     		var selected_company1= selected_company();
     		
                var xhttp = new XMLHttpRequest();
			    xhttp.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {
			            document.getElementById("invoice_display").innerHTML = this.responseText;
			        }
			    };
			    xhttp.open("GET", "database/return_invoice.php?selected_days="+selected_days1+"&selected_company="+selected_company1+"", true);
			    xhttp.send();

     	}
     	
     	

     </script>
</body>
</html>