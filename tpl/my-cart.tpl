[[if empty($meals)]]
    <div class="w3-card-2">
        <div id="success">
            <h3>Your cart is empty <span class="white glyphicon glyphicon-shopping-cart"></span></h3>
        </div>
    </div>
[[else if $success == true]]
    <div class="w3-card-2">
        <div id="success">
            <h3><span style="font-size:20px;">Your order has been successfully submited </span><hr> Click on cart to continue shoping... <a href="./index.php"><span class="white glyphicon glyphicon-shopping-cart"></span></a></h3>
        </div>
    </div>
[[else]]
    <div class="w3-card-2">
        <form action="my-cart.php" method="POST">
            [[foreach $meals as $m]]
                <div id="div[[$m[0]->cart_id]]">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="[[$m[0]->image_id]]" alt="[[$m[0]->title]]" width="150" class="img-responsive">
                        </div>
                        <div class="col-md-2">
                            <h3>[[$m[0]->title]]</h3>
                            <p id="duplicate[[$m[0]->cart_id]]">[[$m[0]->duplicate]] X</p>
                            <p id="price[[$m[0]->cart_id]]">price: [[$m[0]->price]] [[$m[0]->price_unit]]</p>
                        </div>
                        <div class="col-md-3">
                            <h3>Order information</h3>
                            <p>Type of service: <b>[[$m[0]->type_of_service]]</b></p>
                            <p>Date: <b>[[$m[0]->date]]</b></p>
                        </div>
                        <div class="col-md-2">
                            <h3>Notes</h3>
                            <textarea name="note[]" id="" cols="3" rows="1"></textarea>
                        </div>                    
                        <div class="col-md-3">
                            <button type="button" name="minus" class="minus btn btn-default btn-sm" value="[[$m[0]->cart_id]]">
                                <span class="glyphicon glyphicon-minus"></span>
                            </button>
                            <button type="button" name="plus" class="plus btn btn-default btn-sm" value="[[$m[0]->cart_id]]">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                            <button name="delete" type="button" class="delete btn btn-default btn-sm" value="[[$m[0]->cart_id]]">
                                <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </div>
                    </div>
                    <hr>
                </div>
            [[/foreach]]

            <div id="final_price"><h2>Final price: <span><span class="count">[[$sum]] </span> eur</span> </h2></div>
            <input type="hidden" id="final_price_button" value="[[$sum]]">

            [[if $sum != 0]]
                <div id="order_button">
                    <button name="order" class="w3-btn-block w3-sunshine">Place your order</button>
                </div>
            [[/if]]

        </form>
    </div>
[[/if]]
<script>
    $('.count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 4000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });

    $(".minus").on('click', function(){
        var food_id = $(this).val();
        var user_id = [[$user_id]];

        var div         = 'div';
        var duplicate   = 'duplicate';
        var price       = 'price';

        var final_price = $('#final_price_button').val();

        price       =   price + food_id;
        duplicate   =   duplicate + food_id;
        div         =   div + food_id;

        $.ajax({ type: "POST",
            url: "delete-only-one-item.php",
            data: { food_id: food_id, user_id: user_id, final_price: final_price },
            dataType: "json",
            success: function(response)
            {
                if(response[3] == true)
                {
                    document.getElementById(div).remove();
                    document.getElementById('badge-change').innerHTML = response[0];
                    document.getElementById('final_price').innerHTML = '<div id="final_price"><h2>Final price: <span><span class="count">' + response[4] + ' ' + response[5] + '</span></span> </h2></div>';
                    $('#final_price_button').val(response[4]);
                    if(response[4] == 0)
                        document.getElementById('order_button').remove();
                }
                else
                {
                    document.getElementById('badge-change').innerHTML = response[0];
                    document.getElementById(duplicate).innerHTML = '<p>' + response[1] + ' X</p>';
                    document.getElementById(price).innerHTML = '<p>' + response[2] + '</p>';
                    document.getElementById('final_price').innerHTML = '<div id="final_price"><h2>Final price: <span><span class="count">' + response[4] + ' ' + response[5] + '</span></span> </h2></div>';
                    $('#final_price_button').val(response[4]);
                    if(response[4] == 0)
                        document.getElementById('order_button').remove();
                }
            }
        });
    });

    $(".delete").on('click', function(){
        var food_id = $(this).val();
        var user_id = [[$user_id]];

        var div         = 'div';
        div             = div + food_id;

        var final_price = $('#final_price_button').val();

        $.ajax({ type: "POST",
            url: "delete-item.php",
            data: { food_id: food_id, user_id: user_id, final_price: final_price },
            dataType: "json",
            success: function(response)
            {
                document.getElementById(div).remove();
                document.getElementById('badge-change').innerHTML = response[0];
                document.getElementById('final_price').innerHTML = '<div id="final_price"><h2>Final price: <span><span class="count">' + response[1] + ' ' + response[2] + '</span></span> </h2></div>';
                $('#final_price_button').val(response[1]);

                if(response[1] == '0')
                    document.getElementById('order_button').remove();
            }
        });
    });

    $(".plus").on('click', function(){
        var food_id = $(this).val();
        var user_id = [[$user_id]];

        var div         = 'div';
        var duplicate   = 'duplicate';
        var price       = 'price';

        var final_price = $('#final_price_button').val();

        price       =   price + food_id;
        duplicate   =   duplicate + food_id;
        div         =   div + food_id;

        $.ajax({ type: "POST",
            url: "add-only-one-item.php",
            data: { food_id: food_id, user_id: user_id, final_price: final_price},
            dataType: "json",
            success: function(response)
            {
                    document.getElementById('badge-change').innerHTML = response[0];
                    document.getElementById(duplicate).innerHTML = '<p>' + response[1] + ' X</p>';
                    document.getElementById(price).innerHTML = '<p>' + response[2] + '</p>';
                    document.getElementById('final_price').innerHTML = '<div id="final_price"><h2>Final price: <span><span class="count">' + response[4] + ' ' + response[5] + '</span></span> </h2></div>';
                    $('#final_price_button').val(response[4]);
                    if(response[4] == 0)
                        document.getElementById('order_button').remove();
            }
        });
    });
</script>
