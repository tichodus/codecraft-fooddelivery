<?php
    include 'core/functions/users.php';
    include 'core/functions/company.php';
    include 'core/functions/images.php';

    $user_id = $_POST['user_id'];
    $response = array();
    $admin_id = $user_id;
    $employees_on_wait = get_alerts($admin_id);

    if($employees_on_wait == NULL)
    {
        $response[0] = 0;
    }
    else
    {
        foreach ($employees_on_wait as $admin_alerts_id => $employee) {
            $employees[] = $employee->user_id;
        }

        foreach ($employees as $emp) {
            $employees_info[] = return_user($emp);
        }
    
        $number_of_employees    = count($employees_info);
        $img                    = $employees_info[0][0]->image;
        $img_path               = get_image($img);

        if($number_of_employees == 1)
            $response[3] = 0;
        else
        {
            $img                = $employees_info[1][0]->image;
            $img_path           = get_image($img);
            $response[3]        = $img_path[0]->image_path;
            $response[4]        = $employees_info[1][0]->first_name . ' ' . $employees_info[1][0]->last_name;
            $response[5]        = $employees_info[1][0]->user_id;
        }
        if($number_of_employees == 2)
            $response[4] = 0;

        $response[0]      = $img_path[0]->image_path;
        $response[1]      = $employees_info[0][0]->first_name . ' ' . $employees_info[0][0]->last_name;
        $response[2]      = $employees_info[0][0]->user_id;
    }


    echo json_encode($response);
?>