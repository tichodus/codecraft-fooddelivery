<?php
    require 'core/db-init.php';
    include 'Food.php';

    function insert_food($title, $description, $quantity, $unit_of_measure, $price, $price_unit, $image_id, $category_id, $user_id, $rate)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO food (title, description, quantity, unit_of_measure, price, price_unit, image_id, category_id, user_id, rate) VALUES ('$title', '$description', $quantity, '$unit_of_measure', $price, '$price_unit', $image_id, $category_id, $user_id, $rate)");
    }

    function return_meals($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM food WHERE user_id = $user_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Food($row['food_ID'], $row['title'], $row['description'], $row['quantity'], $row['unit_of_measure'], $row['price'], $row['price_unit'], $row['image_id'], $row['category_id'], $row['user_id'], $row['rate']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_meal($meal_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM food WHERE food_ID = $meal_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Food($row['food_ID'], $row['title'], $row['description'], $row['quantity'], $row['unit_of_measure'], $row['price'], $row['price_unit'], $row['image_id'], $row['category_id'], $row['user_id'], $row['rate']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_all_meals()
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM food");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Food($row['food_ID'], $row['title'], $row['description'], $row['quantity'], $row['unit_of_measure'], $row['price'], $row['price_unit'], $row['image_id'], $row['category_id'], $row['user_id'], $row['rate']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function update_image($food_id, $image_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE images SET food_id = $food_id WHERE image_id = $image_id");
    }

    function delete_meal($meal_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("DELETE FROM food WHERE food_ID = $meal_id");
    }

    function update_food_rate($sum, $food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE food SET rate = $sum WHERE food_ID = $food_id");
    }

    function update_meal($title, $description, $quantity, $unit_of_measure, $category, $price, $price_unit, $meal_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE food SET title = '$title', description = '$description', quantity = $quantity, unit_of_measure = '$unit_of_measure', category_id = $category, price = $price, price_unit = '$price_unit' WHERE food_ID = $meal_id");
    }
?>