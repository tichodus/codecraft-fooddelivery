<?php 
    require 'core/db-init.php';
    include 'Comment.php';

    function return_comments_for_meal($food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM comments WHERE food_id = $food_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Comment($row['comment_id'], $row['employee_id'], $row['food_id'], $row['comment']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function insert_comment($employee_id, $food_id, $comment)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO comments (employee_id, food_id, comment) VALUES ($employee_id, $food_id, '$comment')");
    }
?>