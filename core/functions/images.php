<?php
    require 'core/db-init.php';
    include 'Image.php';

    function get_image($image_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM images WHERE image_id = $image_id");
            if ($res)
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Image($row['image_id'], $row['image_path'], $row['user_id'], $row['food_id']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function image_exists($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno)
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM `images` WHERE `user_id` = $user_id");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function insert_image($user_id, $path)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
      
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO images (user_id, image_path) VALUES ($user_id, '$path')");
    }

    function insert_image_path($path)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
      
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO images (image_path) VALUES ('$path')");
    }

    function return_images()
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM images");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Image($row['image_id'], $row['image_path'], $row['user_id'], $row['food_id']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function get_img_id_from_user_id($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM images WHERE user_id = $user_id");
            if ($res)
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Image($row['image_id'], $row['image_path'], $row['user_id'], $row['food_id']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function delete_image($img_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("DELETE FROM images WHERE image_id = $img_id");
    }
?>