<?php
    require 'core/db-init.php';
    include 'User.php';

    function user_data($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $data = array();
        $user_id = (int)$user_id;

        $func_num_args = func_num_args();
        $func_get_args = func_get_args();

        if($func_num_args > 1)
        {
            unset($func_get_args[0]);
            
            $fields = '`' . implode('`, `', $func_get_args) . '`';

            $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
            if ($con->connect_errno) 
                print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
            else 
            {
                $res = $con->query("SELECT $fields FROM `users` WHERE `user_id` = $user_id");
                $row = $res->fetch_assoc();
                return $row;
            }
        }
    }

    function register_user($username, $password, $first_name, $last_name, $email, $active, $privileges, $spent, $company)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $password = md5($password);        
              $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO users (username, password, first_name, last_name, email, active, privileges, spent, company) VALUES ('$username', '$password', '$first_name', '$last_name', '$email', $active, $privileges, $spent, $company)");
    }

    function get_admin_id_for_company($my_company)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM users WHERE company = $my_company AND privileges=1");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new User($row['user_id'], $row['username'], $row['first_name'], $row['last_name'], $row['email'], $row['privileges'], $row['active'], $row['spent'], $row['company'], $row['automatic_delivery'], $row['image']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function logged_in()
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        return (isset($_SESSION['user_id'])) ? true : false;
    }

    function logged_in_as_admin($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM `users` WHERE `user_id` = $user_id AND `privileges` = 1");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function login($username, $password)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $password = md5($password);
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT user_id FROM users WHERE username='$username' AND password='$password'");
            $u_id = mysqli_fetch_assoc($res);
            return $u_id['user_id'];
        }
    }

    function user_exists($username)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
        {
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        }
        else 
        {
            $res = $con->query("SELECT * FROM `users` WHERE `username` = '$username'");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function email_exists($email)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM `users` WHERE `email` = '$email'");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }        
    }

    function return_all_users()
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM users");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new User($row['user_id'], $row['username'], $row['first_name'], $row['last_name'], $row['email'], $row['privileges'], $row['active'], $row['spent'], $row['company'], $row['automatic_delivery'], $row['image']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_users($pid)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM users WHERE company=$pid AND privileges=0");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new User($row['user_id'], $row['username'], $row['first_name'], $row['last_name'], $row['email'], $row['privileges'], $row['active'], $row['spent'], $row['company'], $row['automatic_delivery'], $row['image']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function alter_user($username, $first_name, $last_name, $email, $status, $user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno)
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE users SET username='$username', first_name = '$first_name', last_name='$last_name', email = '$email', active = $status WHERE user_id = $user_id");
    }

    function alter_user_img($img_id, $user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE users SET image=$img_id WHERE user_id = $user_id");
    }

    function accept_user($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE users SET active=1 WHERE user_id = $user_id");
    }

    function return_user($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM users WHERE user_id = $user_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new User($row['user_id'], $row['username'], $row['first_name'], $row['last_name'], $row['email'], $row['privileges'], $row['active'], $row['spent'], $row['company'], $row['automatic_delivery'], $row['image']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function alter_user_as_user($user_id, $first_name, $last_name, $password, $email)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("UPDATE users SET first_name='$first_name', last_name='$last_name', password='$password', email='$email' WHERE user_id = $user_id ");
    }

    function delete_user($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("DELETE FROM users WHERE user_id = $user_id");
    }


    function return_all_users_pib($pib)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM users WHERE company=$pib");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new User($row['user_id'], $row['username'], $row['first_name'], $row['last_name'], $row['email'], $row['privileges'], $row['active'], $row['spent'], $row['company'], $row['automatic_delivery'], $row['image']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function automatic_order()
    {
         $host_name      = $GLOBALS['host_name'];
         $db_user        = $GLOBALS['db_user'];
         $db_password    = $GLOBALS['db_password'];
         $db_name        = $GLOBALS['db_name'];

        
         
        $date_array=getdate();
         $time=$date_array['hours'];//vraca trenutno vreme
         $time= $time+2; //plus 2 da se sredi vremenska zona
         
         
         
            if ($time>24) {
                $time=$time-24;
            }
         $users=array();
         $date=date('Y-m-d');


            $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

            
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }
            //echo "$time";
            $query="SELECT user_id FROM users join companies ON users.company=companies.pib  WHERE automatic_delivery=1 AND company_type=0 AND (type_of_plan=0 OR type_of_plan=2) AND $time+3>=breakfast_time";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {
                            
                                $users[]=$row[0];
                            
                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            $users_bool=array();
            $i=0;
            foreach ($users as &$value) {
                
                 $users_bool[$i]=1;

                $query="SELECT date, type_of_meal FROM orders WHERE IDemployee=$value";
                if (mysqli_multi_query($link, $query)) 
                {

                        if ($result = mysqli_use_result($link)) 
                        {
                            
                            while ($row = mysqli_fetch_row($result)) 
                            {
                               // echo $row[1];
                               //echo $row[0];
                                   if($row[0]==$date) 
                                   {
                                    if($row[1]=='breakfast')
                                    {
                                        $users_bool[$i]=0;
                                        break;
                                    }
                                    else
                                    {
                                        $users_bool[$i]=1;
                                    }
                                   }
                                   else
                                   {
                                     $users_bool[$i]=1;
                                   }

                                
                            }
                            mysqli_free_result($result);
                        }
                        
                    
                        if (mysqli_more_results($link))
                            printf("-----------------\n");
            
                }
                $i++;
            }
            
           
            //print_r($users);
            //print_r($users_bool);
            
            $best_meal=array();
            $i=0;
            $bool_naruci=0;
            foreach ($users as &$value) {
                if($users_bool[$i]==1)
                {
                   // echo "bool iznosi $users_bool[$i]";
                   // echo "i iznosi $i";
             $IDemployee=$value;
            $query="SELECT distinct * from orders where IDfood=(select idfood from (select IDfood, IDemployee,count(*) from orders where IDemployee=$IDemployee group by IDfood, IDemployee order by count(*) desc limit 1) as table1) and IDemployee=(select IDemployee from (select IDfood, IDemployee,count(*) from orders where IDemployee=$IDemployee group by IDfood, IDemployee order by count(*) desc limit 1) as table2) limit 1";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {
                            $best_meal=$row;
                            //echo "$row[1]";
                            $bool_naruci=1;

                            
                    

                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            if($bool_naruci==0)
            {
              $query="SELECT distinct * from orders where IDfood=(select idfood from (select IDfood, IDemployee,count(*) from orders where 1 group by IDfood, IDemployee order by count(*) desc limit 1) as table1) and IDemployee=(select IDemployee from (select IDfood, IDemployee,count(*) from orders where 1 group by IDfood, IDemployee order by count(*) desc limit 1) as table2) limit 1";
                if (mysqli_multi_query($link, $query)) 
                {

                        if ($result = mysqli_use_result($link)) 
                        {
                            
                            while ($row = mysqli_fetch_row($result)) 
                            {
                                $best_meal=$row;
                               
                                
                        

                            }
                            mysqli_free_result($result);
                        }
                        
                    
                        if (mysqli_more_results($link))
                            printf("-----------------\n");
            
                }
            }
                //print_r($best_meal);
                $date=date('Ymd');
                //echo "$date";
             $query1="INSERT INTO `orders`( `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, `quantity`) VALUES($best_meal[1], $IDemployee, $date, '$best_meal[4]', $best_meal[5], 'breakfast',$best_meal[7])";
             //echo "$query1";
             
            

                            if(mysqli_query($link, $query1))
                            {
                                //echo "Upisano";
                            }
                           
        }
        $i++;
    }


    }


    function automatic_order_lunch()
    {
         $host_name      = $GLOBALS['host_name'];
         $db_user        = $GLOBALS['db_user'];
         $db_password    = $GLOBALS['db_password'];
         $db_name        = $GLOBALS['db_name'];

        
         
        $date_array=getdate();
         $time=$date_array['hours'];//vraca trenutno vreme
         $time= $time+2; //plus 2 da se sredi vremenska zona
         
         
         
            if ($time>24) {
                $time=$time-24;
            }
         $users=array();
         $date=date('Y-m-d');


            $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

            
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }

            $query="SELECT user_id FROM users join companies ON users.company=companies.pib  WHERE automatic_delivery=1 AND company_type=0 AND (type_of_plan=1 OR type_of_plan=2) AND $time+3>lunch_time";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {
                            
                                $users[]=$row[0];
                            
                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            $users_bool=array();
            $i=0;
            foreach ($users as &$value) {
                
                 $users_bool[$i]=1;

                $query="SELECT date, type_of_meal FROM orders WHERE IDemployee=$value";
                if (mysqli_multi_query($link, $query)) 
                {

                        if ($result = mysqli_use_result($link)) 
                        {
                            
                            while ($row = mysqli_fetch_row($result)) 
                            {
                               // echo $row[1];
                               //echo $row[0];
                                   if($row[0]==$date) 
                                   {
                                    if($row[1]=='lunch')
                                    {
                                        $users_bool[$i]=0;
                                        break;
                                    }
                                    else
                                    {
                                        $users_bool[$i]=1;
                                    }
                                   }
                                   else
                                   {
                                     $users_bool[$i]=1;
                                   }

                                
                            }
                            mysqli_free_result($result);
                        }
                        
                    
                        if (mysqli_more_results($link))
                            printf("-----------------\n");
            
                }
                $i++;
            }
            
           
            //print_r($users);
            //print_r($users_bool);
            
            $best_meal=array();
            $i=0;
            $bool_naruci=0;
            foreach ($users as &$value) {
                if($users_bool[$i]==1)
                {
                   // echo "bool iznosi $users_bool[$i]";
                   // echo "i iznosi $i";
             $IDemployee=$value;
            $query="SELECT distinct * from orders where IDfood=(select idfood from (select IDfood, IDemployee,count(*) from orders where IDemployee=$IDemployee group by IDfood, IDemployee order by count(*) desc limit 1) as table1) and IDemployee=(select IDemployee from (select IDfood, IDemployee,count(*) from orders where IDemployee=$IDemployee group by IDfood, IDemployee order by count(*) desc limit 1) as table2) limit 1";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {
                            $best_meal=$row;
                            //echo "$row[1]";
                            $bool_naruci=1;

                            
                    

                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            if($bool_naruci==0)
            {
            $query="SELECT distinct * from orders where IDfood=(select idfood from (select IDfood, IDemployee,count(*) from orders where 1 group by IDfood, IDemployee order by count(*) desc limit 1) as table1) and IDemployee=(select IDemployee from (select IDfood, IDemployee,count(*) from orders where 1 group by IDfood, IDemployee order by count(*) desc limit 1) as table2) limit 1";
                if (mysqli_multi_query($link, $query)) 
                {

                        if ($result = mysqli_use_result($link)) 
                        {
                            
                            while ($row = mysqli_fetch_row($result)) 
                            {
                                $best_meal=$row;
                               
                                
                        

                            }
                            mysqli_free_result($result);
                        }
                        
                    
                        if (mysqli_more_results($link))
                            printf("-----------------\n");
            
                }
            }
           
                //print_r($best_meal);
                $date=date('Ymd');
             $query1="INSERT INTO `orders`( `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, `quantity`) VALUES($best_meal[1], $IDemployee, $date, '$best_meal[4]', $best_meal[5], 'lunch',$best_meal[7])";
             //echo "$query1";
             
            

                            if(mysqli_query($link, $query1))
                            {
                                //echo "Upisano";
                            }
                           
        }
        $i++;
    }


    }
?>