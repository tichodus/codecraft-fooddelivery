<?php 
    require 'core/db-init.php';
    include 'Cart.php';

    function return_items_from_cart($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM cart WHERE user_id = $user_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Cart($row['cart_id'], $row['user_id'], $row['food_ID'], $row['duplicate'], $row['type_of_service'], $row['date']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_items_with_food_id($food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM cart WHERE food_ID = $food_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Cart($row['cart_id'], $row['user_id'], $row['food_ID'], $row['duplicate'], $row['type_of_service'], $row['date']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function item_exists($food_id, $type_of_service, $order_date)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM `cart` WHERE `food_ID` = $food_id AND `type_of_service` = '$type_of_service' AND `date` = $order_date");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function return_item($user_id, $food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM cart WHERE food_ID = $food_id AND user_id=$user_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Cart($row['cart_id'], $row['user_id'], $row['food_ID'], $row['duplicate'], $row['type_of_service'], $row['date']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_item_cart_id($cart_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM cart WHERE cart_id = $cart_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Cart($row['cart_id'], $row['user_id'], $row['food_ID'], $row['duplicate'], $row['type_of_service'], $row['date']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function delete_from_cart($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("DELETE FROM cart WHERE user_id=$user_id");
    }
?>