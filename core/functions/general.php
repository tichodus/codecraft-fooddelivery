<?php
    function protect_page() //zaštita stranice
    {
        if(logged_in() === false)
        {
            header('Location: 403-error.php');
            exit();
        }
    }

    function protect_page_reverse() //zaštita stranice
    {
        if(logged_in() === true)
        {
            header('Location: 403-error.php');
            exit();
        }
    }

    function output_errors($errors) //štampanje error-a (greške)
    {
        echo "<p style='text-align:center; font-size:20px; text-transform: uppercase;'> Došlo je do greške </span><hr>";
        foreach ($errors as $error) 
            echo "<p style='text-align:center'>", $error, "</p>";
        echo '<hr><p> <span style="color:#DE1B1B;"> NAPOMENA: </span> Polja označena sa <span style="color:#DE1B1B;">*</span> su obavezna!';
    }
?>