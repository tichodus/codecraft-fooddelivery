<?php
    require 'core/db-init.php';
    include 'Company.php';
    include 'Alerts.php';

    function insert_company($company_name, $company_description, $pib, $company_type)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO companies (company_name, company_description, pib, company_type) VALUES ('$company_name', '$company_description', $pib, $company_type)");
    }

    function insert_not_catering_company($company_name, $company_description, $pib, $company_type, $type_of_plan, $breakfast_time, $lunch_time)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO companies (company_name, company_description, pib, company_type, type_of_plan, breakfast_time, lunch_time) VALUES ('$company_name', '$company_description', $pib, $company_type, $type_of_plan, $breakfast_time, $lunch_time)");
    }

    function return_companies()
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Company($row['company_name'], $row['company_description'], $row['pib'], $row['company_type'], $row['type_of_plan'], $row['breakfast_time'], $row['lunch_time']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_company($pib)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies WHERE pib = $pib");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Company($row['company_name'], $row['company_description'], $row['pib'], $row['company_type'], $row['type_of_plan'], $row['breakfast_time'], $row['lunch_time']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_companies_with_type($type)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies WHERE company_type = $type");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Company($row['company_name'], $row['company_description'], $row['pib'], $row['company_type'], $row['type_of_plan'], $row['breakfast_time'], $row['lunch_time']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_all_companies($type)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
        $all_companies=array();
        $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        
        $query = "SELECT * FROM `companies` WHERE company_type=$type";
        if (mysqli_multi_query($link, $query)) 
        {

                if ($result = mysqli_use_result($link)) 
                {
                    
                    while ($row = mysqli_fetch_row($result)) 
                    {

                        $all_companies[]=new Company($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6]);
                        
                    }
                    mysqli_free_result($result);
                }
                
            
                if (mysqli_more_results($link))
                    printf("-----------------\n");
    
        }
        return $all_companies;
    }

    function check_pib($pib)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies WHERE pib=$pib");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function alert_admin_for_new_user($admin_id, $user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO admin_alerts (admin_id, user_id) VALUES ($admin_id, $user_id)");
    }

    function exists_user_in_alert_for_admin($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
        {
            $res = $con->query("SELECT * FROM `admin_alerts` WHERE `user_id` = $user_id");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }

    function get_alerts($admin_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
        {
            $res = $con->query("SELECT * FROM `admin_alerts` WHERE `admin_id` = $admin_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Alerts($row['admin_alerts_id'], $row['admin_id'], $row['user_id']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function delete_user_from_alerts($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];
       
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("DELETE FROM admin_alerts WHERE user_id=$user_id");
    }

    function get_company($pib)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies WHERE pib = $pib");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Company($row['company_name'], $row['company_description'], $row['pib'], $row['company_type'], $row['type_of_plan'], $row['breakfast_time'], $row['lunch_time']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function is_catering_company($pib)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM companies WHERE pib=$pib AND company_type=1");
            if(mysqli_num_rows($res) > 0)
                return true;
            else
                return false;
        }
    }
?>