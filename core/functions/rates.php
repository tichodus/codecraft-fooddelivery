<?php 
    require 'core/db-init.php';
    include 'Rate.php';

    function create_rates_for_user($user_id, $food_id, $rate)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("INSERT INTO rates (user_id, food_id, rate) VALUES ($user_id, $food_id, $rate)");
    }

    function update_rate($user_id, $food_id, $rate)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $res = $con->query("UPDATE rates SET rate = $rate WHERE user_id = $user_id AND food_id = $food_id");
    }

    function return_rate($user_id, $food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM rates WHERE user_id = $user_id AND food_id = $food_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Rate($row['rate_id'], $row['user_id'], $row['food_id'], $row['rate']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function return_rate_for_meal($food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
        {
            $res = $con->query("SELECT * FROM rates WHERE food_id = $food_id");
            if ($res) 
            {
                $niz = array();
                while ($row = $res->fetch_assoc()) 
                {
                    $niz[] = new Rate($row['rate_id'], $row['user_id'], $row['food_id'], $row['rate']);
                }
                $res->close();
                return $niz;
            }
            else
                print ("Query failed");
        }
    }

    function delete_rates($food_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("DELETE FROM rates WHERE food_id = $food_id");   
    }

    function delete_rates_with_user_id($user_id)
    {
        $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else 
            $res = $con->query("DELETE FROM rates WHERE user_id = $user_id");   
    }
?>