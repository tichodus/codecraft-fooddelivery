<?php
    include 'MySmarty.php';
    $smarty = new MySmarty();

    session_start();
    //error_reporting(0);

    require 'functions/general.php';
    require 'functions/users.php';
    require 'functions/company.php';
    require 'functions/images.php';
    require 'functions/category.php';
    require 'functions/food.php';
    require 'functions/delivery_report.php';
    require 'functions/cart.php';
    require 'functions/orders.php';
    require 'functions/rates.php';
    require 'functions/comments.php';

    if(logged_in() === true)
    {
        $sesssion_user_id = $_SESSION['user_id'];
        $user_data = user_data($sesssion_user_id, 'user_id', 'username', 'password', 'first_name', 'last_name', 'email', 'email_code', 'active', 'privileges', 'spent', 'company', 'automatic_delivery', 'image');

        if($user_data['active'] == 0)
        {
            session_destroy();
            header('Location: not-activated.php');
            exit();
        }
    }

    $errors = array();
?>