<?php
    class Food
    {
        public $food_ID;
        public $title;
        public $description;
        public $quantity;
        public $unit_of_measure;
        public $price;
        public $price_unit;
        public $image_id;
        public $category_id;
        public $user_id;
        public $rate;

        public function __construct($food_ID, $title="", $description="", $quantity, $unit_of_measure="", $price, $price_unit="", $image_id, $category_id, $user_id, $rate)
        {
            $this->food_ID          =   $food_ID;
            $this->title            =   $title;
            $this->description      =   $description;
            $this->quantity         =   $quantity;
            $this->unit_of_measure  =   $unit_of_measure;
            $this->price            =   $price;
            $this->price_unit       =   $price_unit;
            $this->image_id         =   $image_id;
            $this->category_id      =   $category_id;
            $this->user_id          =   $user_id;
            $this->rate             =   $rate;
        }
    }
?>