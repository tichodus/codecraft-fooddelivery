<?php
    class Rate
    {
        public $rate_id;
        public $user_id;
        public $food_id;
        public $rate;

        public function __construct($rate_id, $user_id, $food_id, $rate)
        {
            $this->rate_id   =   $rate_id;
            $this->user_id   =   $user_id;
            $this->food_id   =   $food_id;
            $this->rate      =   $rate;
        }
    }
?>