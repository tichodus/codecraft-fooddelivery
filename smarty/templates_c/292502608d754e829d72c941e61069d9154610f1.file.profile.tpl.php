<?php /* Smarty version Smarty-3.1.13, created on 2016-10-23 12:28:58
         compiled from "tpl/profile.tpl" */ ?>
<?php /*%%SmartyHeaderCode:113700567857de6db6968bd6-59470017%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '292502608d754e829d72c941e61069d9154610f1' => 
    array (
      0 => 'tpl/profile.tpl',
      1 => 1477239939,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '113700567857de6db6968bd6-59470017',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57de6db6a0fd16_55812968',
  'variables' => 
  array (
    'redirect' => 0,
    'empty_first_name' => 0,
    'empty_last_name' => 0,
    'empty_email' => 0,
    'empty_old_password' => 0,
    'empty_new_password' => 0,
    'wrong_old_password' => 0,
    'first_name' => 0,
    'last_name' => 0,
    'username' => 0,
    'email' => 0,
    'image_path' => 0,
    'time' => 0,
    'privileges' => 0,
    'company_name' => 0,
    'company_type' => 0,
    'automatic_delivery' => 0,
    'user_id' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57de6db6a0fd16_55812968')) {function content_57de6db6a0fd16_55812968($_smarty_tpl) {?><script>
    $(document).on('click', '#change_button', function(){
        var first_name = $('#first_name').text();
        var last_name = $('#last_name').text();
        var email = $('#email').text();

        $('#change_info').remove();
        var override = '<form action="profile.php" method="POST" enctype="multipart/form-data"><div id="info-for-change"><h3>Change your information</h3> <label for="first_name">First Name:</label>  <input type="text" name="first_name" value="' + first_name + '"> <label for="last_name">Last Name:</label> <input type="text" name="last_name" value="'+ last_name +'"> <label for="old_password">Old password:</label> <input type="password" name="old_password"> <label for="new_password">New password:</label> <input type="password" name="new_password"> <label for="email">Email:</label> <input type="text" name="email" value="'+ email +'"> <label for="image">Image:</label> <input type="file" name="image"> <button name="final_change_button" class="w3-btn-block w3-teal">Submit</button>';
        document.getElementById("change-wrapper").innerHTML = override;
    });

    $(document).ready(function() {
        $('input[type=radio][name=automatic_delivery_radio_buttons]').change(function() {
            var value = $('input[type=radio][name=automatic_delivery_radio_buttons]:checked').val();
            var hidden_user_id = $('#hidden_user_id').val();
            $.post("update_automatic_delivery.php", {hidden_user_id: hidden_user_id, automatic_delivery: value});
    })});
</script>

<?php if ($_smarty_tpl->tpl_vars['redirect']->value==true){?>
    <div id="success">
        <h3>You have successfully changed your information.</h3>
    </div>
<?php }?>
<?php if ($_smarty_tpl->tpl_vars['empty_first_name']->value==true){?>
    <div id="error">
        <h3>First Name is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['empty_last_name']->value==true){?>
    <div id="error">
        <h3>Last Name is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['empty_email']->value==true){?>
    <div id="error">
        <h3>Email is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['empty_old_password']->value==true){?>
    <div id="error">
        <h3>Old password is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['empty_new_password']->value==true){?>
    <div id="error">
        <h3>New password is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['wrong_old_password']->value==true){?>
    <div id="error">
        <h3>Wrong old password. Please try again</h3>
    </div>
<?php }?>

<div id="change-wrapper">
    <div id="change_info">
        <h3>Information</h3>
        <div class="row">
                <div class="col-md-4">
                <label for="first_name">First Name:</label> <div id="first_name"><?php echo $_smarty_tpl->tpl_vars['first_name']->value;?>
</div>
                <br>

                <label for="last_name">Last Name:</label> <div id="last_name"><?php echo $_smarty_tpl->tpl_vars['last_name']->value;?>
</div>
                <br>

                <label for="username">Username:</label> <div id="username"><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</div>
                <br>

                <label for="email">Email:</label> <div id="email"><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</div>
                <br>

                <!-- <button name="change_button" id="change_button" class="w3-btn">Change</button> -->
            </div>
            <div class="col-md-4">
                <div class="w3-card-2" style="width:75%; display: block;margin-left: auto;margin-right: auto">
                  <img src="<?php echo $_smarty_tpl->tpl_vars['image_path']->value;?>
?t=<?php echo $_smarty_tpl->tpl_vars['time']->value;?>
" alt="" style="width:100%;">
                  <div class="w3-container w3-center">
                    <?php if ($_smarty_tpl->tpl_vars['privileges']->value==0){?>
                        <h4><b>employee</b></h4>
                        <p><?php echo $_smarty_tpl->tpl_vars['company_name']->value;?>
</p>
                    <?php }else{ ?>
                        <h4><b>admin</b></h4>
                        <p><?php echo $_smarty_tpl->tpl_vars['company_name']->value;?>
</p>
                    <?php }?>
                  </div>
                </div>
            </div>
            <?php if ($_smarty_tpl->tpl_vars['company_type']->value==0){?>
                <div class="col-md-4">
                    <div id="automatic_delivery">
                        <h4>Automatic delivery</h4>
                        <div class="container_automatic">
                            <div class="switch">
                                <?php if ($_smarty_tpl->tpl_vars['automatic_delivery']->value==0){?>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="0" id="disable" checked>
                                    <label for="disable" class="switch-label switch-label-off">Disable</label>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="1" id="enable">
                                    <label for="enable" class="switch-label switch-label-on">Enable</label>
                                    <input type="hidden" id="hidden_user_id" value="<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
">
                                <?php }else{ ?>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="0" id="disable" >
                                    <label for="disable" class="switch-label switch-label-off">Disable</label>
                                    <input type="radio" class="switch-input" name="automatic_delivery_radio_buttons" value="1" id="enable" checked>
                                    <label for="enable" class="switch-label switch-label-on">Enable</label>
                                    <input type="hidden" id="hidden_user_id" value="<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
">
                                <?php }?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php }?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <button name="change_button" id="change_button" class="w3-btn">Change</button>
            </div>
        </div>
    </div>
</div>

<script>
    setTimeout(function(){
      $('#success').remove();
    }, 4000);
</script><?php }} ?>