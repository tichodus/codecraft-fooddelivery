<?php /* Smarty version Smarty-3.1.13, created on 2016-10-23 12:28:32
         compiled from "tpl/list-of-users.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3577945557de6e24d763b3-69523994%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8a4842e8cfebe51397c60583a96235fa2a3a33cd' => 
    array (
      0 => 'tpl/list-of-users.tpl',
      1 => 1477239937,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3577945557de6e24d763b3-69523994',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57de6e24e588a5_02279969',
  'variables' => 
  array (
    'company_type' => 0,
    'users' => 0,
    'user' => 0,
    'are_you_sure' => 0,
    'first_name' => 0,
    'last_name' => 0,
    'user_id_delete' => 0,
    'change' => 0,
    'all_fields_required' => 0,
    'username' => 0,
    'email' => 0,
    'status' => 0,
    'user_id_pom' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57de6e24e588a5_02279969')) {function content_57de6e24e588a5_02279969($_smarty_tpl) {?>
<div id="list-of-users">
    <h3>List of users</h3>
</div>

<form class="w3-container w3-card-4" action="list-of-users.php" method="POST"> 
    <div class="table-responsive"> 
        <table class="table">
            <tr>
                <th>ID</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Privileges</th>
                <th>Status</th>
                <?php if ($_smarty_tpl->tpl_vars['company_type']->value==0){?>
                    <th>Spent</th>
                <?php }?>
                <th>Change</th>
                <th>Delete</th>
            </tr>
            <?php  $_smarty_tpl->tpl_vars['user'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['user']->_loop = false;
 $_smarty_tpl->tpl_vars['user_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['users']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['user']->key => $_smarty_tpl->tpl_vars['user']->value){
$_smarty_tpl->tpl_vars['user']->_loop = true;
 $_smarty_tpl->tpl_vars['user_id']->value = $_smarty_tpl->tpl_vars['user']->key;
?>
                <tr>
                    <td><?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['user']->value->username;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['user']->value->first_name;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['user']->value->last_name;?>
</td>
                    <td><?php echo $_smarty_tpl->tpl_vars['user']->value->email;?>
</td>
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->privileges==0){?>
                        <td>Employee</td>
                    <?php }else{ ?>
                        <td>Admin</td>
                    <?php }?>

                    <?php if ($_smarty_tpl->tpl_vars['user']->value->active==0){?>
                        <td id="unverified">Unverified</td>
                    <?php }elseif($_smarty_tpl->tpl_vars['user']->value->active==1){?>
                        <td id="verified">Verified</td>
                    <?php }?>
                    <?php if ($_smarty_tpl->tpl_vars['company_type']->value==0){?>
                        <td><?php echo $_smarty_tpl->tpl_vars['user']->value->spent;?>
</td>
                    <?php }?>
                    <td>
                        <button value="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" class="w3-btn-block-smaller w3-sunshine"  name="change_button">Change</button>
                    </td>
                    <td>
                        <button value="<?php echo $_smarty_tpl->tpl_vars['user']->value->user_id;?>
" class="w3-btn-block-smaller w3-red" name="delete_button">Delete</button>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
    <?php if ($_smarty_tpl->tpl_vars['are_you_sure']->value==true){?>
        <div id="error">
            <h3>Are you sure you want to delete employee <?php echo $_smarty_tpl->tpl_vars['first_name']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['last_name']->value;?>
?</h3>
            <div class="row">
                <div class="col-md-6">
                    <button class="w3-btn-block w3-red" name="no_button">No</button>
                </div>
                <div class="col-md-6">
                    <button class="w3-btn-block w3-fresh" name="yes_button" value="<?php echo $_smarty_tpl->tpl_vars['user_id_delete']->value;?>
">Yes</button>
                </div>
            </div>
        </div>
    <?php }else{ ?>
        <?php if ($_smarty_tpl->tpl_vars['change']->value==true){?>
            <div id="info-for-change">
                <h4><?php echo $_smarty_tpl->tpl_vars['first_name']->value;?>
 Information</h4>

                <?php if ($_smarty_tpl->tpl_vars['all_fields_required']->value==true){?>
                    <div id="error">
                        <h3>Some fields are missing. Please try again</h3>
                    </div>
                <?php }?>

                <label for="first_name">First Name:</label>
                <input type="text" name="first_name" value="<?php echo $_smarty_tpl->tpl_vars['first_name']->value;?>
">

                <label for="last_name">Last Name:</label>
                <input type="text" name="last_name" value="<?php echo $_smarty_tpl->tpl_vars['last_name']->value;?>
">

                <label for="username">Username:</label>
                <input type="text" name="username" value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
">

                <label for="email">Email:</label>
                <input type="text" name="email" value="<?php echo $_smarty_tpl->tpl_vars['email']->value;?>
">

                <label for="status">Status:</label>
                <select name="status">
                    <?php if ($_smarty_tpl->tpl_vars['status']->value==0){?>
                        <option value="0">Unverified</option>
                        <option value="1">Verified</option>
                    <?php }elseif($_smarty_tpl->tpl_vars['status']->value==1){?>
                        <option value="1">Verified</option>
                        <option value="0">Unverified</option>
                    <?php }?>
                </select>

                <button name="change_final" id="change_final" class="w3-btn-block w3-fresh" value="<?php echo $_smarty_tpl->tpl_vars['user_id_pom']->value;?>
">Change</button>
            </div>
        <?php }?>
    <?php }?>
</form><?php }} ?>