<?php /* Smarty version Smarty-3.1.13, created on 2016-09-17 23:11:54
         compiled from "tpl\notifications.tpl" */ ?>
<?php /*%%SmartyHeaderCode:300257dd9714a98c74-19564612%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a2748346a3e63057d4c92aa4082a698c5c137234' => 
    array (
      0 => 'tpl\\notifications.tpl',
      1 => 1474141760,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '300257dd9714a98c74-19564612',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57dd9714b2f120_60190927',
  'variables' => 
  array (
    'alerts' => 0,
    'alert' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57dd9714b2f120_60190927')) {function content_57dd9714b2f120_60190927($_smarty_tpl) {?><div id="rounded-border-not">
    <h3>Notifications</h3>
</div>
<hr>
<?php  $_smarty_tpl->tpl_vars['alert'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['alert']->_loop = false;
 $_smarty_tpl->tpl_vars['admin_alerts_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['alerts']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['alert']->key => $_smarty_tpl->tpl_vars['alert']->value){
$_smarty_tpl->tpl_vars['alert']->_loop = true;
 $_smarty_tpl->tpl_vars['admin_alerts_id']->value = $_smarty_tpl->tpl_vars['alert']->key;
?>
<div id="user-wrapper<?php echo $_smarty_tpl->tpl_vars['alert']->value->user_id;?>
">
    <div class="row">
        <div class="col-md-2">
            <img src="<?php echo $_smarty_tpl->tpl_vars['alert']->value->image_path;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['alert']->value->first_name;?>
" class="img-responsive" width="100" height="100">
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12">
                    <b><?php echo $_smarty_tpl->tpl_vars['alert']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['alert']->value->last_name;?>
</b> requested to join your team.
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="left-button">
                        <button class="accept w3-btn-block w3-fresh" value="<?php echo $_smarty_tpl->tpl_vars['alert']->value->user_id;?>
" id="accept">Accept</button>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="right-button">
                        <button class="decline w3-btn-block w3-red" value="<?php echo $_smarty_tpl->tpl_vars['alert']->value->user_id;?>
" id="decline">Decline</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
</div>
<?php } ?>

<script>
    $(".accept").on('click', function(){
        var user_id = $(this).val();
        var div = "user-wrapper";
        div = div + user_id;

        $.ajax({ type: "POST",
                url: "accept-employee.php",
                data: { user_id: user_id },
                cache: false,
                success: function(response)
                {
                    document.getElementById(div).remove();
                }
        });
    });

    $(".decline").on('click', function(){
        var user_id = $(this).val();
        var div = "user-wrapper";
        div = div + user_id;

        $.ajax({ type: "POST",
                url: "decline-employee.php",
                data: { user_id: user_id },
                cache: false,
                success: function(response)
                {
                    document.getElementById(div).remove();
                }
        });
    });
</script><?php }} ?>