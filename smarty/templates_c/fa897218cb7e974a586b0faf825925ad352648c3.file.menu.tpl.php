<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 10:05:19
         compiled from "tpl\menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:358557dd8717045086-67044511%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'fa897218cb7e974a586b0faf825925ad352648c3' => 
    array (
      0 => 'tpl\\menu.tpl',
      1 => 1474193026,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '358557dd8717045086-67044511',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57dd87182a57d7_60302132',
  'variables' => 
  array (
    'one_meal' => 0,
    'food' => 0,
    'ketering_or_it' => 0,
    'user_rate' => 0,
    'allow_comments' => 0,
    'food_id' => 0,
    'no_comments' => 0,
    'comments' => 0,
    'comment' => 0,
    'maxDays' => 0,
    'i' => 0,
    'today_day' => 0,
    'today_month' => 0,
    'today_year' => 0,
    'type_of_service' => 0,
    'few_meals' => 0,
    'meals' => 0,
    'm' => 0,
    'x' => 0,
    'user_id' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57dd87182a57d7_60302132')) {function content_57dd87182a57d7_60302132($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['one_meal']->value==true){?>
<div class="row">
    <div class="col-md-9">
            <div class="row">
                <div class="col-md-5">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['food']->value[0]->image_path;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['food']->value[0]->title;?>
" class="img-responsive" style="width:100%;">
                </div>
                <div class="col-md-7">
                    <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                        <div class="col-md-6">
                            <h2><?php echo $_smarty_tpl->tpl_vars['food']->value[0]->title;?>
</h2>
                            <hr>
                            <p>Price: <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->price_unit;?>
</p>
                                <fieldset class="rating">
                                    <?php if ($_smarty_tpl->tpl_vars['user_rate']->value==1){?>
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" checked/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }elseif($_smarty_tpl->tpl_vars['user_rate']->value==2){?>
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" checked/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }elseif($_smarty_tpl->tpl_vars['user_rate']->value==3){?>
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" checked/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }elseif($_smarty_tpl->tpl_vars['user_rate']->value==4){?>
                                        <input type="radio" id="star5" name="rating" value="5"  /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" checked/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }elseif($_smarty_tpl->tpl_vars['user_rate']->value==5){?>
                                        <input type="radio" id="star5" name="rating" value="5" checked /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }else{ ?>
                                        <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                        <input type="radio" id="star4" name="rating" value="4" /><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                        <input type="radio" id="star3" name="rating" value="3" /><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                        <input type="radio" id="star2" name="rating" value="2" /><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                        <input type="radio" id="star1" name="rating" value="1" /><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                    <?php }?>
                                </fieldset>
                            <p>Quantity: <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->quantity;?>
 <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->unit_of_measure;?>
</p>
                            <hr>
                            <button type="button" class="add btn btn-default btn-sm" value="<?php echo $_smarty_tpl->tpl_vars['food']->value[0]->food_ID;?>
">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                            </button>
                        </div>
                    <?php }else{ ?>
                        <div class="col-md-7">
                            <h2><?php echo $_smarty_tpl->tpl_vars['food']->value[0]->title;?>
</h2>
                            <hr>
                            <p>Price: <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->price_unit;?>
</p>
                            <p>Quantity: <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->quantity;?>
</p>
                            <p>Category: <?php echo $_smarty_tpl->tpl_vars['food']->value[0]->category;?>
</p>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="margin-top:10px;">
                    <h4>Meal description</h4>
                    <p style="text-align: justify;"><?php echo $_smarty_tpl->tpl_vars['food']->value[0]->description;?>
</p>
                </div>
            </div>
            <hr>
                <div class="row">
                    <div class="col-md-12">
                        <h4>Comments</h4>
                    </div>
                </div>
                <?php if ($_smarty_tpl->tpl_vars['allow_comments']->value==true){?>
                <div id="comments-wrapper">      
                    <div class="row">
                        <div class="col-md-12">
                            <textarea name="comment" id="comment" cols="10" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-9"></div>
                        <div class="col-md-3">
                            <button name="submit_comment" value="<?php echo $_smarty_tpl->tpl_vars['food_id']->value;?>
" class="submit_comment w3-btn-block w3-sunshine">Post comment</button>
                        </div>
                    </div>
                </div>
                <?php }?>
            <hr>
            <?php if ($_smarty_tpl->tpl_vars['no_comments']->value==true){?>
                <p>No new comments</p>
            <?php }?>
            <?php  $_smarty_tpl->tpl_vars['comment'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['comment']->_loop = false;
 $_smarty_tpl->tpl_vars['comment_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['comments']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['comment']->key => $_smarty_tpl->tpl_vars['comment']->value){
$_smarty_tpl->tpl_vars['comment']->_loop = true;
 $_smarty_tpl->tpl_vars['comment_id']->value = $_smarty_tpl->tpl_vars['comment']->key;
?>
                <div class="row">
                    <div class="col-md-1">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['comment']->value->image_path;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['comment']->value->user_first_name;?>
" width="50" height="50"> 
                    </div>
                    <div class="col-md-11">
                        <h6><?php echo $_smarty_tpl->tpl_vars['comment']->value->user_first_name;?>
 says:</h6>
                        <?php echo $_smarty_tpl->tpl_vars['comment']->value->comment;?>

                    </div>
                </div>
                <hr>
            <?php } ?>
    </div>
        <div class="col-md-3">
            <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                <div id="rounded-border">        
                    <div class="row">
                        <div class="col-md-12">
                            <h3 style="font-size: 18px;">Choose day for ordering</h3>
                            <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['maxDays']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['maxDays']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                    <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['today_day']->value+1){?>
                                            <option selected="selected" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                    <?php }else{ ?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                    <?php }?>
                                <?php }} ?>
                            </select>
                            <p id='mon_yer' style="display: inline-block;">/<?php echo $_smarty_tpl->tpl_vars['today_month']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['today_year']->value;?>
</p>
                            <br>
                        </div>
                        <div class="col-md-12">
                            <?php if ($_smarty_tpl->tpl_vars['type_of_service']->value==0){?>
                                <b><span id="fresh">breakfast</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            <?php }elseif($_smarty_tpl->tpl_vars['type_of_service']->value==1){?>
                                <b><span id="fresh">lunch</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast">
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            <?php }?>

                        </div>
                    </div>
                </div>
                <hr>
            <?php }?>
        </div>
</div>
<?php }else{ ?>
<div class="row">
    <div class="col-md-9">
        <?php  $_smarty_tpl->tpl_vars['m'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['m']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['meals']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['m']->key => $_smarty_tpl->tpl_vars['m']->value){
$_smarty_tpl->tpl_vars['m']->_loop = true;
?>
            <div class="row">
                <div class="col-md-5">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['m']->value->image_path;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['m']->value->title;?>
" class="img-responsive" style="width:100%;">
                </div>
                <div class="col-md-7">
                    <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                        <div class="col-md-6">
                            <h2><a href="menu.php?foodid=<?php echo $_smarty_tpl->tpl_vars['m']->value->food_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value->title;?>
</a></h2>
                            <hr>
                            <p>Price: <?php echo $_smarty_tpl->tpl_vars['m']->value->price;?>
 <?php echo $_smarty_tpl->tpl_vars['m']->value->price_unit;?>
</p>
                            <p>Category: <?php echo $_smarty_tpl->tpl_vars['m']->value->category;?>
</p>
                            <button type="button" class="add btn btn-default btn-sm" value="<?php echo $_smarty_tpl->tpl_vars['m']->value->food_ID;?>
">
                                <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                            </button>
                        </div>
                    <?php }else{ ?>
                        <div class="col-md-7">
                            <h2><a href="menu.php?foodid=<?php echo $_smarty_tpl->tpl_vars['m']->value->food_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['m']->value->title;?>
</a></h2>
                            <hr>
                            <p>Price: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['m']->value->price_unit;?>
</p>
                            <p>Category: <?php echo $_smarty_tpl->tpl_vars['m']->value->category;?>
</p>
                        </div>
                    <?php }?>
                </div>
            </div>
            <hr>
        <?php } ?>
    </div>
        <div class="col-md-3">
            <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                <div id="rounded-border">        
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Choose day for ordering</h3>
                            <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                                <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['maxDays']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['maxDays']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                    <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['today_day']->value+1){?>
                                            <option selected="selected" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                    <?php }else{ ?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                    <?php }?>
                                <?php }} ?>
                            </select>
                            <p id='mon_yer' style="display: inline-block;">/<?php echo $_smarty_tpl->tpl_vars['today_month']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['today_year']->value;?>
</p>
                            <br>
                        </div>
                        <div class="col-md-12">
                            <?php if ($_smarty_tpl->tpl_vars['type_of_service']->value==0){?>
                                <b><span id="fresh">breakfast</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            <?php }elseif($_smarty_tpl->tpl_vars['type_of_service']->value==1){?>
                                <b><span id="fresh">lunch</span></b>
                                <div style="display:none;">
                                    <div class="container_automatic">
                                        <div class="switch">
                                            <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast">
                                            <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                            <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                            <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            <?php }?>

                        </div>
                    </div>
                </div>
                <hr>
            <?php }?>
        </div>
</div>
<?php }?>

<script>
    $(".submit_comment").on('click', function(){
        var food_id = $(this).val();
        var user_id = <?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
;
        var comment = $('#comment').val();

        $.ajax({ type: "POST",
                url: "insert-comment.php",
                data: { food_id: food_id, user_id: user_id, comment: comment},
                cache: false,
                success: function(response)
                {
                    document.getElementById('comments-wrapper').innerHTML = 'Your comment has been successfully submited';
                }
        });
    });

    $(".add").on('click', function(){
        var food_id = $(this).val();
        var user_id = <?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
;

        var today = <?php echo $_smarty_tpl->tpl_vars['today_day']->value;?>
;
        var month = <?php echo $_smarty_tpl->tpl_vars['today_month']->value;?>
;
        var year  = <?php echo $_smarty_tpl->tpl_vars['today_year']->value;?>
;

        var type_of_service = $('input[type=radio][name=type_of_plan]:checked').val();

        var e = document.getElementById("order_for_date");
        var order_date = e.options[e.selectedIndex].value;
        if (order_date<10) 
        {
            order_date = 0+order_date;
        }

        $.ajax({ type: "POST",
                url: "insert-into-cart.php",
                data: { food_id: food_id, user_id: user_id, order_date: order_date, type_of_service: type_of_service},
                cache: false,
                success: function(response)
                {
                    document.getElementById('badge-change').innerHTML = response;
                }
        });

    });

    $(document).ready(function() {
        $('input[type=radio][name=rating]').change(function() {
            var value = $('input[type=radio][name=rating]:checked').val();
            var food_id = <?php echo $_smarty_tpl->tpl_vars['food_id']->value;?>
;
            var user_id = <?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
;

            $.post("rate-meal.php", {value: value, food_id: food_id, user_id: user_id});
    })});
</script><?php }} ?>