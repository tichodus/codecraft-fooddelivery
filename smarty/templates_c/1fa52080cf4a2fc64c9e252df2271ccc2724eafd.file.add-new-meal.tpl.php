<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 08:42:31
         compiled from "tpl\add-new-meal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1833357d991d58d3207-11509714%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1fa52080cf4a2fc64c9e252df2271ccc2724eafd' => 
    array (
      0 => 'tpl\\add-new-meal.tpl',
      1 => 1474186944,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1833357d991d58d3207-11509714',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57d991d596aad2_65587210',
  'variables' => 
  array (
    'no_permission' => 0,
    'all_fields_required' => 0,
    'quantity_must_be_numeric' => 0,
    'price_must_be_numeric' => 0,
    'img_type_not_suported' => 0,
    'success' => 0,
    'categories' => 0,
    'category' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57d991d596aad2_65587210')) {function content_57d991d596aad2_65587210($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['no_permission']->value==true){?>
    <div style="text-align:center;">
        <h1>403 ERROR</h1>
        <hr>
        <p>Permission denied!</p>
    </div>
<?php }else{ ?>
    <form action="add-new-meal.php" method="POST" enctype="multipart/form-data">
        <?php if ($_smarty_tpl->tpl_vars['all_fields_required']->value==true){?>
            <div id="error">
                <h3>All fields marked with <span id="required_fields">*</span> are required. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['quantity_must_be_numeric']->value==true){?>
            <div id="error">
                <h3>Quantity must be numeric value. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['price_must_be_numeric']->value==true){?>
            <div id="error">
                <h3>Price must be numeric value. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['img_type_not_suported']->value==true){?>
            <div id="error">
                <h3>Image type not suported. Please use .png or .jpg pictures</h3>
            </div>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['success']->value==true){?>
            <div id="success">
                <h3>You have successfully published new meal to your menu.</h3>
            </div>
        <?php }?>

        <div id="info-for-change">
            <h3>Add new meal to your menu</h3> 

            <label for="title">Title<span id="required_fields">*</span>:</label>  
            <input type="text" name="title">

            <label for="description">Description<span id="required_fields">*</span>:</label>  
            <textarea name="description" id="" cols="30" rows="10"></textarea>

            <label for="quantity">Quantity<span id="required_fields">*</span>:</label>  
            <input type="text" name="quantity" maxlength="4" placeholder="numeric value...">

            <label for="unit_of_measure">Unit of measure<span id="required_fields">*</span>:</label>  
            <select name="unit_of_measure" id="">
                <option value="not_selected">Select unit...</option>
                <option value="grams">Grams</option>
                <option value="kg">Kg</option>
                <option value="piece">Piece</option>
            </select>

            <label for="category">Category<span id="required_fields">*</span>:</label>  
            <select name="category" id="">
                <option value="not_selected">Select category...</option>
                <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['category_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['category_id']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
                    <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->category_id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->type;?>
</option>
                <?php } ?>
            </select>

            <label for="price">Price<span id="required_fields">*</span>:</label>  
            <input type="text" name="price" maxlength="6" placeholder="numeric value...">

            <label for="price_unit">Price unit<span id="required_fields">*</span>:</label>  
            <select name="price_unit" id="">
                <option value="eur">EUR</option>
            </select>

            <label for="image">Picture<span id="required_fields">*</span></label>
            <input type="file" name="image" id="">
            
            <button name="submit" class="w3-btn-block w3-sunshine">Submit</button>
        </div>
    </form>
<?php }?>

<script>
    setTimeout(function(){
      $('#success').remove();
    }, 4000);
</script><?php }} ?>