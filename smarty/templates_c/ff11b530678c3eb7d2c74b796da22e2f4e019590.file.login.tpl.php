<?php /* Smarty version Smarty-3.1.13, created on 2016-10-23 13:08:37
         compiled from "tpl/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:164487295957de6d6ac97ff8-75440284%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ff11b530678c3eb7d2c74b796da22e2f4e019590' => 
    array (
      0 => 'tpl/login.tpl',
      1 => 1477239936,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '164487295957de6d6ac97ff8-75440284',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57de6d6ace1fd6_88596715',
  'variables' => 
  array (
    'username_missing' => 0,
    'password_missing' => 0,
    'incorrect_input' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57de6d6ace1fd6_88596715')) {function content_57de6d6ace1fd6_88596715($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['username_missing']->value==true){?>
    <div id="error">
        <h3>Username is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['password_missing']->value==true){?>
    <div id="error">
        <h3>Password is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['incorrect_input']->value==true){?>
    <div id="error">
        <h3>Incorrect username or password. Please try again</h3>
    </div>
<?php }?>
<div id="registration">
    <h3>Login form</h3>
</div>

<form action="login.php" method="POST" id="login">
    <label for="username">Username</label>
    <input type="text" name="username" id="">

    <label for="password">Password</label>
    <input type="password" name="password">
    <button name="submit" class="w3-btn-block w3-sunshine">Log in</button>
</form><?php }} ?>