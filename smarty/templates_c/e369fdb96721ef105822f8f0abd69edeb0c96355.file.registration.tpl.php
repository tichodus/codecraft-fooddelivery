<?php /* Smarty version Smarty-3.1.13, created on 2016-10-23 13:07:46
         compiled from "tpl/registration.tpl" */ ?>
<?php /*%%SmartyHeaderCode:193263197357de6e763edb05-89334502%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e369fdb96721ef105822f8f0abd69edeb0c96355' => 
    array (
      0 => 'tpl/registration.tpl',
      1 => 1477239939,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '193263197357de6e763edb05-89334502',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57de6e7646db71_01391281',
  'variables' => 
  array (
    'companies' => 0,
    'go_to_page_two' => 0,
    'success_personal' => 0,
    'admin_first_name' => 0,
    'success_company' => 0,
    'company_exists' => 0,
    'pib' => 0,
    'all_fields_required' => 0,
    'username_exists' => 0,
    'username' => 0,
    'email_exists' => 0,
    'email' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57de6e7646db71_01391281')) {function content_57de6e7646db71_01391281($_smarty_tpl) {?><script>
    $(document).ready(function() {
        $('input[type=radio][name=type]').change(function() {
            var value = $('input[type=radio][name=type]:checked').val();
            var companies = <?php echo $_smarty_tpl->tpl_vars['companies']->value;?>
;

            if(value == 'personal')
            {
                var draw = '<div id="personal_input"><label for="first_name">First Name<span id="required_fields">*</span>:</label><input type="text" name="first_name"> <label for="last_name">Last Name:</label> <input type="text" name="last_name"> <label for="username">Username<span id="required_fields">*</span>:</label> <input type="text" name="username"> <label for="password">Password<span id="required_fields">*</span>:</label> <input type="password" name="password"> <label for="email">Email<span id="required_fields">*</span>:</label> <input type="text" name="email"> <label for="my_company">Select company that you work for<span id="required_fields">*</span></label><select name="my_company" id="my_company"> <option value="0"> Not selected...</option></select> <button name="submit" class="w3-btn-block w3-teal" id="submit_button" value="1">Submit</button> </div>';
                document.getElementById("info-for-change").innerHTML = draw;

                $.each(companies, function(key, v) {   
                    $('#my_company')
                        .append($("<option></option>")
                        .attr("value",key)
                        .text(v)); 
                });
            }
            else if(value == 'company')
            {
                $('#personal_input').remove();
                var draw = '<div id="personal_input"><label for="company_name">Company Name<span id="required_fields">*</span>:</label> <input type="text" name="company_name"> <label for="company_description">Company description</label> <textarea name="company_description" cols="30" rows="10"></textarea> <label for="pib">PIB<span id="required_fields">*</span></label> <input type="text" name="pib" maxlength="9"> <label for="first_name">First Name<span id="required_fields">*</span>:</label> <input type="text" name="first_name"> <label for="last_name">Last Name:</label> <input type="text" name="last_name"> <label for="username">Username<span id="required_fields">*</span>:</label> <input type="text" name="username"> <label for="password">Password<span id="required_fields">*</span>:</label> <input type="password" name="password"> <label for="email">Email<span id="required_fields">*</span>:</label> <input type="text" name="email"> <label for="catering">Are you a catering company?</label><div class="radio"><input id="yes" type="radio" name="is_it_catering" value="yes"><label for="yes">Yes</label><input id="no" type="radio" name="is_it_catering" value="no"><label for="no">No</label></div> <div id="button-change-div"><button name="submit" class="w3-btn-block w3-teal" id="submit_button" value="2">Submit</button></div></div>';
                document.getElementById("info-for-change").innerHTML = draw;
            }
        })
        $('input[type=radio][name=type_of_plan]').change(function() {
            var value_two = $('input[type=radio][name=type_of_plan]:checked').val();
            document.getElementById("delivery-title").innerHTML = "<h4>Choose delivery time</h4>"
            if(value_two == 'breakfast')
            {
                document.getElementById("delivery-time").innerHTML = "<label for='breakfast'>Breakfast</label><select name='breakfast_time' id='breakfast_time'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option></select> h<hr>";
                document.getElementById("final-button").innerHTML = '<button name="final_submit" class="w3-btn-block w3-teal">Submit</button>';
            }
            else if(value_two == 'lunch')
            {
                document.getElementById("delivery-time").innerHTML = "<label for='lunch'>Lunch</label><select name='lunch_time' id='lunch_time'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option></select> h<hr>";
                document.getElementById("final-button").innerHTML = '<button name="final_submit" class="w3-btn-block w3-teal">Submit</button>';
            }
            else
            {
                document.getElementById("delivery-time").innerHTML = "<label for='breakfast'>Breakfast</label><select name='breakfast_time' id='breakfast_time'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option></select> h<div id='padding-label'><label for='lunch'>Lunch</label><select name='lunch_time' id='lunch_time'><option value='1'>1</option><option value='2'>2</option><option value='3'>3</option><option value='4'>4</option><option value='5'>5</option><option value='6'>6</option><option value='7'>7</option><option value='8'>8</option><option value='9'>9</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option></select> h</div><hr>";
                document.getElementById("final-button").innerHTML = '<button name="final_submit" class="w3-btn-block w3-teal">Submit</button>';
            }
        })
    });

    $(document).on('change','input[type=radio][name=is_it_catering]', function() {
        var value_one = $('input[type=radio][name=is_it_catering]:checked').val();
        if(value_one == 'no')
            document.getElementById("button-change-div").innerHTML = '<button name="next" class="w3-btn-block w3-teal" id="next_button" value="2">Next</button>';
        else if(value_one == 'yes')
            document.getElementById("button-change-div").innerHTML = '<button name="submit" class="w3-btn-block w3-teal" id="submit_button" value="2">Submit</button>';
    });
</script>

<div class="w3-card-2">
    <?php if ($_smarty_tpl->tpl_vars['go_to_page_two']->value==true){?>
        <form action="registration.php" method="POST">
            <div class="radio-block">
                <h4>What catering plan do you want?</h4>
                <input id="breakfast" type="radio" name="type_of_plan" value="breakfast">
                <label for="breakfast">Breakfast</label>
                <input id="lunch" type="radio" name="type_of_plan" value="lunch">
                <label for="lunch">Lunch</label>
                <input id="breakfast_and_lunch" type="radio" name="type_of_plan" value="breakfast_and_lunch">
                <label for="breakfast_and_lunch">Both breakfast and lunch</label>
                <hr>
            </div>
            <div id="delivery-title"></div>
            <div id="delivery-time"></div>
            <div id="final-button"></div>
        </form>
    <?php }else{ ?>
        <?php if ($_smarty_tpl->tpl_vars['success_personal']->value==true){?>
            <div id="success">
                <h3>You have successfully created account. Now you need to be approved by your company administrator <span id="sunshine"> <?php echo $_smarty_tpl->tpl_vars['admin_first_name']->value;?>
 </span></h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['success_company']->value==true){?>
            <div id="success">
                <h3>You have successfully created account for your company. Congratulations!</h3>
            </div>
        <?php }else{ ?>
            <?php if ($_smarty_tpl->tpl_vars['company_exists']->value==true){?>
                <div id="error">
                    <h3>Company with <span id="required_fields"><?php echo $_smarty_tpl->tpl_vars['pib']->value;?>
</span> PIB already exists. Please try again</h3>
                </div>
            <?php }elseif($_smarty_tpl->tpl_vars['all_fields_required']->value==true){?>
                <div id="error">
                    <h3>All fields marked with <span id="required_fields">*</span> are required. Please try again</h3>
                </div>
            <?php }elseif($_smarty_tpl->tpl_vars['username_exists']->value==true){?>
                <div id="error">
                    <h3>Username <span id="required_fields"><?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</span> already exists. Please try again</h3>
                </div>
            <?php }elseif($_smarty_tpl->tpl_vars['email_exists']->value==true){?>
                <div id="error">
                    <h3>Email <span id="required_fields"><?php echo $_smarty_tpl->tpl_vars['email']->value;?>
</span> already exists. Please try again</h3>
                </div>
            <?php }?>
            <div id="registration">
                <h3>Registration form</h3>
            </div>

            <div class="radio">
                <h4>What kind of registration form do you need?</h4>
                <input id="personal" type="radio" name="type" value="personal">
                <label for="personal">Personal</label>
                <input id="company" type="radio" name="type" value="company">
                <label for="company">Company</label>
                <hr>
            </div>

            <form action="registration.php" method="POST">
                <div id="info-for-change"> </div>
            </form>
        <?php }?>
    <?php }?>
</div><?php }} ?>