<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 09:33:24
         compiled from "tpl\client_invoice.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1421757dde2f2ba2af1-84416239%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a55d49d2d77b1fb879ebbf5afb053675cebe92d6' => 
    array (
      0 => 'tpl\\client_invoice.tpl',
      1 => 1474191125,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1421757dde2f2ba2af1-84416239',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57dde2f32e3fd6_64459072',
  'variables' => 
  array (
    'all_users_comp' => 0,
    'item_com' => 0,
    'id_company' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57dde2f32e3fd6_64459072')) {function content_57dde2f32e3fd6_64459072($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<title>Catering</title>

        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        select{
            display: inline-block;
            min-width:265px;
            min-height: 45px;
            width: 100%;
            border-width: 3px;
            border-color: rgba(50, 50, 50, 0.14);
            margin: 10px 10px 10px 0px;
        }
        </style> 
</head>
<body>

<form name="" action="database/export_pdf_client.php" method="post">
<div class="row">
    <div class="col-md-12">
        <div id="change_info">
            <h3>Look at users invoices</h3>
        </div>
    </div>
</div>
<div class="choser_days">
<p >Chose previous days:</p>
	<select id="days_display" name="days_display">
		<option value="1">daily</option>
		<option value="7">weekly</option>
		<option value="30">monthly</option>
	</select>
	</div>
	<div class="choser_employee">
	<p>Chose employee:</p>
	<select id="choosed_employee" class="choosed_employee" name="choosed_employee">
	<option  value="0" selected="selected">All</option>
		<?php  $_smarty_tpl->tpl_vars['item_com'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_com']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['all_users_comp']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_com']->key => $_smarty_tpl->tpl_vars['item_com']->value){
$_smarty_tpl->tpl_vars['item_com']->_loop = true;
?>
        <option  value="<?php echo $_smarty_tpl->tpl_vars['item_com']->value->user_id;?>
"><?php echo $_smarty_tpl->tpl_vars['item_com']->value->first_name;?>
 <?php echo $_smarty_tpl->tpl_vars['item_com']->value->last_name;?>
</option>
     <?php } ?>
	</select>
	</div>
	<input id="choose_company" type="hidden" name="choose_company" value="<?php echo $_smarty_tpl->tpl_vars['id_company']->value;?>
">
    <input type="submit" name="exportPDF" value="exportPDF" class="w3-btn-block w3-sunshine">
    
     </form>

     <button onclick="return_invoice()" class="read w3-btn-block w3-red">Read</button>
     
     <div id="invoice_display"></div>
     
     
     <script type="text/javascript">
     	function selected_company()
     	{
     		
     		var e = document.getElementById("choose_company");
            var choosed_company = e.value;
            return choosed_company;


     	}
     	function selected_user()
     	{
     		var e = document.getElementById("choosed_employee");
            var choosed_employee = e.value;
            return choosed_employee;
     	}
     	function selected_days()
     	{
     		var e = document.getElementById("days_display");
            var choosed_days = e.options[e.selectedIndex].value;
            return choosed_days;

     	}

     	function return_invoice()
     	{
     		
     		var selected_days1=selected_days();
     		var selected_company1= selected_company();
     		var selected_employee=selected_user();
     		
                var xhttp = new XMLHttpRequest();
			    xhttp.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {
			            document.getElementById("invoice_display").innerHTML = this.responseText;
			        }
			    };
			    xhttp.open("GET", "database/return_invoice_client.php?selected_days="+selected_days1+"&selected_company="+selected_company1+"&selected_user="+selected_employee+"", true);
			    xhttp.send();

     	}
     	</script>
</body>
</html><?php }} ?>