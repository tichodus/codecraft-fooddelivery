<?php /* Smarty version Smarty-3.1.13, created on 2016-09-17 23:20:00
         compiled from "tpl\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:499957ddc8c18910e7-77365441%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'db97a3f8af80d260372ece8474eb31d980e7a586' => 
    array (
      0 => 'tpl\\index.tpl',
      1 => 1474154382,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '499957ddc8c18910e7-77365441',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57ddc8c35b7498_31939728',
  'variables' => 
  array (
    'not_registered' => 0,
    'ketering_or_it' => 0,
    'maxDays' => 0,
    'i' => 0,
    'today_day' => 0,
    'today_month' => 0,
    'today_year' => 0,
    'type_of_service' => 0,
    'number_of_meals' => 0,
    'x' => 0,
    'meals' => 0,
    'user_id' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57ddc8c35b7498_31939728')) {function content_57ddc8c35b7498_31939728($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
    <title>CodeCraft takmičenje</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/mainstyle.css">
</head>
<body>
    <?php if ($_smarty_tpl->tpl_vars['not_registered']->value==true){?>
        <div class="background-wrapper">
            <div id="home-log-in">                
                <h2>Log in</h2>
                <form action="login.php" method="POST">
                    <input type="text" name="username" id="" placeholder="username"><br>
                    <input type="password" name="password" id="" style="margin-top:10px;" placeholder="password"><br>
                    <button name="submit" class="w3-btn-block w3-fresh" style="width: 20%">Log in</button>
                </form>
                </div>
            </div>
        </div>
    <?php }else{ ?>
        <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
            <div id="rounded-border">        
                <div class="row">
                    <div class="col-md-6">
                        <h3>Choose day for ordering</h3>
                        <select id="order_for_date" class="order_for_date_c" style="display: inline-block;">
                            <?php $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int)ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['maxDays']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['maxDays']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0){
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++){
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration == 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration == $_smarty_tpl->tpl_vars['i']->total;?>
                                <?php if ($_smarty_tpl->tpl_vars['i']->value==$_smarty_tpl->tpl_vars['today_day']->value+1){?>
                                        <option selected="selected" value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                <?php }else{ ?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</option>
                                <?php }?>
                            <?php }} ?>
                        </select>
                        <p id='mon_yer' style="display: inline-block;">/<?php echo $_smarty_tpl->tpl_vars['today_month']->value;?>
/<?php echo $_smarty_tpl->tpl_vars['today_year']->value;?>
</p>
                        <br>
                    </div>
                    <div class="col-md-6">
                        Your company allows you to order
                        <?php if ($_smarty_tpl->tpl_vars['type_of_service']->value==0){?>
                            <b><span id="fresh">breakfast</span></b>
                            <div style="display:none;">
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            </div>
                        <?php }elseif($_smarty_tpl->tpl_vars['type_of_service']->value==1){?>
                            <b><span id="fresh">lunch</span></b>
                            <div style="display:none;">
                                <div class="container_automatic">
                                    <div class="switch">
                                        <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" >
                                        <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                        <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch" checked>
                                        <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                    </div>
                                </div>
                            </div>
                        <?php }else{ ?>
                            both <b><span id="fresh">breakfast</span></b> and <b><span id="fresh">breakfast</span></b>
                            <p>Please choose what kind of order do you want</p>
                            <div class="container_automatic">
                                <div class="switch">
                                    <input type="radio" class="switch-input" name="type_of_plan" value="breakfast" id="breakfast" checked>
                                    <label for="breakfast" class="switch-label switch-label-off">Breakfast</label>

                                    <input type="radio" class="switch-input" name="type_of_plan" value="lunch" id="lunch">
                                    <label for="lunch" class="switch-label switch-label-on">Lunch</label>
                                </div>
                            </div>
                        <?php }?>

                    </div>
                </div>
            </div>
            <hr>
        <?php }?>

        <?php if ($_smarty_tpl->tpl_vars['number_of_meals']->value<7){?>
            <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int)ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? $_smarty_tpl->tpl_vars['number_of_meals']->value-1+1 - (0) : 0-($_smarty_tpl->tpl_vars['number_of_meals']->value-1)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0){
for ($_smarty_tpl->tpl_vars['x']->value = 0, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++){
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
                <?php if (($_smarty_tpl->tpl_vars['x']->value%2)==0){?>
                        <div class="row"> 
                            <?php }?>
                                <div class="col-md-6">
                                    <div id="one-row-index">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img onclick="get(<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
, '<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
')" src="<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->image;?>
" width="200" height="200" class="img-responsive" style="width:100%;" />
                                            </div>
                                            <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
</a></h2>
                                                    <hr>
                                                    <p>Price: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price_unit;?>
</p>
                                                    <p>Category: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category;?>
</p>
                                                    <p>By: <a href="menu.php?id=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->pib;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->company;?>
</a></p>
                                                    <button type="button" class="add btn btn-default btn-sm" value="<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
">
                                                        <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                                                    </button>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="col-md-6">
                                                    <h2><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
</h2>
                                                    <hr>
                                                    <p>Price: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price_unit;?>
</p>
                                                    <p>Category: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category;?>
</p>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php if (($_smarty_tpl->tpl_vars['x']->value%2)==1||$_smarty_tpl->tpl_vars['x']->value==$_smarty_tpl->tpl_vars['number_of_meals']->value-1){?>
                        </div>
                    <hr>
                <?php }?>
            <?php }} ?>
        <?php }else{ ?>
            <?php $_smarty_tpl->tpl_vars['x'] = new Smarty_Variable;$_smarty_tpl->tpl_vars['x']->step = 1;$_smarty_tpl->tpl_vars['x']->total = (int)ceil(($_smarty_tpl->tpl_vars['x']->step > 0 ? 5+1 - (0) : 0-(5)+1)/abs($_smarty_tpl->tpl_vars['x']->step));
if ($_smarty_tpl->tpl_vars['x']->total > 0){
for ($_smarty_tpl->tpl_vars['x']->value = 0, $_smarty_tpl->tpl_vars['x']->iteration = 1;$_smarty_tpl->tpl_vars['x']->iteration <= $_smarty_tpl->tpl_vars['x']->total;$_smarty_tpl->tpl_vars['x']->value += $_smarty_tpl->tpl_vars['x']->step, $_smarty_tpl->tpl_vars['x']->iteration++){
$_smarty_tpl->tpl_vars['x']->first = $_smarty_tpl->tpl_vars['x']->iteration == 1;$_smarty_tpl->tpl_vars['x']->last = $_smarty_tpl->tpl_vars['x']->iteration == $_smarty_tpl->tpl_vars['x']->total;?>
                <?php if (($_smarty_tpl->tpl_vars['x']->value%2)==0){?>
                        <div class="row"> 
                            <?php }?>
                                <div class="col-md-6">
                                    <div id="one-row-index">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <img onclick="get(<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
, '<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
')" src="<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->image;?>
" width="200" height="200" class="img-responsive" style="width:100%;" />
                                            </div>
                                            <?php if ($_smarty_tpl->tpl_vars['ketering_or_it']->value==0){?>
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
</a></h2>
                                                    <hr>
                                                    <p>Price: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price_unit;?>
</p>
                                                    <p>Category: <a href="index.php?category=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category_id;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category;?>
</a></p>
                                                    <p>By: <a href="menu.php?id=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->pib;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->company;?>
</a></p>
                                                    <button type="button" class="add btn btn-default btn-sm" value="<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
">
                                                        <span class="glyphicon glyphicon-shopping-cart"></span> Add to Cart
                                                    </button>
                                                </div>
                                            <?php }else{ ?>
                                                <div class="col-md-6">
                                                    <h2><a href="menu.php?foodid=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->food_ID;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->title;?>
</a></h2>
                                                    <hr>
                                                    <p>Price: <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price;?>
 <?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->price_unit;?>
</p>
                                                    <p>Category: <a href="index.php?category=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category_id;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->category;?>
</a></p>
                                                    <p>By: <a href="menu.php?id=<?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->pib;?>
"><?php echo $_smarty_tpl->tpl_vars['meals']->value[$_smarty_tpl->tpl_vars['x']->value]->company;?>
</a></p>
                                                </div>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            <?php if (($_smarty_tpl->tpl_vars['x']->value%2)==1||$_smarty_tpl->tpl_vars['x']->value==$_smarty_tpl->tpl_vars['number_of_meals']->value-1){?>
                        </div>
                    <hr>
                <?php }?>
            <?php }} ?>
        <?php }?>
    
        <div id="shoping_card_dispay"></div>

        <script>

            $(".add").on('click', function(){
                var food_id = $(this).val();
                var user_id = <?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
;

                var today = <?php echo $_smarty_tpl->tpl_vars['today_day']->value;?>
;
                var month = <?php echo $_smarty_tpl->tpl_vars['today_month']->value;?>
;
                var year  = <?php echo $_smarty_tpl->tpl_vars['today_year']->value;?>
;

                var type_of_service = $('input[type=radio][name=type_of_plan]:checked').val();

                var e = document.getElementById("order_for_date");
                var order_date = e.options[e.selectedIndex].value;
                if (order_date<10) 
                {
                    order_date = 0+order_date;
                }

                $.ajax({ type: "POST",
                        url: "insert-into-cart.php",
                        data: { food_id: food_id, user_id: user_id, order_date: order_date, type_of_service: type_of_service},
                        cache: false,
                        success: function(response)
                        {
                            document.getElementById('badge-change').innerHTML = response;
                        }
                });

            });
    
            $( ".order_for_date_c" ).change(function() {
                var e = document.getElementById("order_for_date");
                var order_date = e.options[e.selectedIndex].value;

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth()+1; 
        
                var yyyy = today.getFullYear();
                if(order_date>dd)
                {
                    if (mm<10)
                        document.getElementById("mon_yer").innerHTML = "/0"+mm+"/"+yyyy;
                    else
                        document.getElementById("mon_yer").innerHTML = "/"+mm+"/"+yyyy;
                }
                else
                    document.getElementById("mon_yer").innerHTML = "/"+(mm+1)+"/"+yyyy;
            });
        </script>
    <?php }?>
</body>
</html><?php }} ?>