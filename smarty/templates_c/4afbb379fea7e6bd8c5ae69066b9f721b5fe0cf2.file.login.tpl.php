<?php /* Smarty version Smarty-3.1.13, created on 2016-09-14 08:48:24
         compiled from "tpl\login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1128257d6f09cd102c7-31676285%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '4afbb379fea7e6bd8c5ae69066b9f721b5fe0cf2' => 
    array (
      0 => 'tpl\\login.tpl',
      1 => 1473842110,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1128257d6f09cd102c7-31676285',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57d6f09cd16115_52569142',
  'variables' => 
  array (
    'username_missing' => 0,
    'password_missing' => 0,
    'incorrect_input' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57d6f09cd16115_52569142')) {function content_57d6f09cd16115_52569142($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['username_missing']->value==true){?>
    <div id="error">
        <h3>Username is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['password_missing']->value==true){?>
    <div id="error">
        <h3>Password is missing. Please try again</h3>
    </div>
<?php }elseif($_smarty_tpl->tpl_vars['incorrect_input']->value==true){?>
    <div id="error">
        <h3>Incorrect username or password. Please try again</h3>
    </div>
<?php }?>
<div id="registration">
    <h3>Login form</h3>
</div>

<form action="login.php" method="POST" id="login">
    <label for="username">Username</label>
    <input type="text" name="username" id="">

    <label for="password">Password</label>
    <input type="password" name="password">
    <button name="submit" class="w3-btn-block w3-sunshine">Log in</button>
</form><?php }} ?>