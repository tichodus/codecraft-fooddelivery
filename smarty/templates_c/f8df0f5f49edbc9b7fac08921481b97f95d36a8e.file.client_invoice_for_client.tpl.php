<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 09:33:51
         compiled from "tpl\client_invoice_for_client.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1877857de5e140ab933-31980153%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f8df0f5f49edbc9b7fac08921481b97f95d36a8e' => 
    array (
      0 => 'tpl\\client_invoice_for_client.tpl',
      1 => 1474191230,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1877857de5e140ab933-31980153',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57de5e141afbc8_11402924',
  'variables' => 
  array (
    'user_id' => 0,
    'id_company' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57de5e141afbc8_11402924')) {function content_57de5e141afbc8_11402924($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<title>Catering</title>
        <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        select{
            display: inline-block;
            min-width:265px;
            min-height: 45px;
            width: 100%;
            border-width: 3px;
            border-color: rgba(50, 50, 50, 0.14);
            margin: 10px 10px 10px 0px;
        }
        </style> 

</head>
<body>

<form name="" action="database/export_pdf_client.php" method="post">
<div class="row">
    <div class="col-md-12">
        <div id="change_info">
            <h3>Your invoices</h3>
        </div>
    </div>
</div>
<div class="choser_days">
<p >Chose previous days:</p>
	<select id="days_display" name="days_display">
		<option value="1">daily</option>
		<option value="7">weekly</option>
		<option value="30">monthly</option>
	</select>
	</div>
	<input id="choose_employee" type="hidden" name="choosed_employee" value="<?php echo $_smarty_tpl->tpl_vars['user_id']->value;?>
">
	<input id="choose_company" type="hidden" name="choose_company" value="<?php echo $_smarty_tpl->tpl_vars['id_company']->value;?>
">
    <input type="submit" name="exportPDF" value="Export to PDF" class="w3-btn-block w3-sunshine">
    
     </form>

     <button onclick="return_invoice()" class="read w3-btn-block w3-red">Read</button>
     
     <div id="invoice_display"></div>
     

     <script type="text/javascript">
     	function selected_company()
     	{
     		
     		var e = document.getElementById("choose_company");
            var choosed_company = e.value;
            
            return choosed_company;


     	}
     	function selected_user()
     	{
     		
     		var e = document.getElementById("choose_employee");
            var choosed_employee = e.value;
            
            return choosed_employee;
     	}
     	function selected_days()
     	{
     		
     		var e = document.getElementById("days_display");
            var choosed_days = e.options[e.selectedIndex].value;
            
            return choosed_days;

     	}

     	function return_invoice()
     	{
     		
     		var selected_days1=selected_days();
     		var selected_company1= selected_company();
     		var selected_employee=selected_user();
     		//alert(selected_days1);
     		//alert(selected_company1);
     		//alert(selected_employee);
     		
                var xhttp = new XMLHttpRequest();
			    xhttp.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {
			            document.getElementById("invoice_display").innerHTML = this.responseText;
			        }
			    };
			    xhttp.open("GET", "database/return_invoice_client_f_client.php?selected_days="+selected_days1+"&selected_company="+selected_company1+"&selected_user="+selected_employee+"", true);
			    xhttp.send();

     	}
     	</script>
</body>
</html><?php }} ?>