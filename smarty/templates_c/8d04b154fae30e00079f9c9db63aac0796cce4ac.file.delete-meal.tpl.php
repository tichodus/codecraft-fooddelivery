<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 09:50:53
         compiled from "tpl\delete-meal.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1631057d9adf479bc53-02479585%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8d04b154fae30e00079f9c9db63aac0796cce4ac' => 
    array (
      0 => 'tpl\\delete-meal.tpl',
      1 => 1474192223,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1631057d9adf479bc53-02479585',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57d9adf479f9d1_82046931',
  'variables' => 
  array (
    'no_permission' => 0,
    'meals' => 0,
    'f' => 0,
    'all_fields_required' => 0,
    'quantity_must_be_numeric' => 0,
    'price_must_be_numeric' => 0,
    'img_type_not_suported' => 0,
    'are_you_sure' => 0,
    'meal_title' => 0,
    'meal_id' => 0,
    'change' => 0,
    'meal_description' => 0,
    'meal_quantity' => 0,
    'meal_unit_of_measure' => 0,
    'meal_category_id' => 0,
    'meal_category_type' => 0,
    'categories' => 0,
    'category' => 0,
    'meal_price' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57d9adf479f9d1_82046931')) {function content_57d9adf479f9d1_82046931($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['no_permission']->value==true){?>
    <div style="text-align:center;">
        <h1>403 ERROR</h1>
        <hr>
        <p>Permission denied!</p>
    </div>
<?php }else{ ?>
    <div id="list-of-users">
        <h3>List of meals</h3>
    </div>

    <form class="w3-container w3-card-4" action="delete-meal.php" method="POST" enctype="multipart/form-data"> 
        <div class="table-responsive"> 
            <table class="table">
                <tr>
                    <th>Title</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Change</th>
                    <th>Delete</th>
                </tr>
                <?php  $_smarty_tpl->tpl_vars['f'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['f']->_loop = false;
 $_smarty_tpl->tpl_vars['food_ID'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['meals']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['f']->key => $_smarty_tpl->tpl_vars['f']->value){
$_smarty_tpl->tpl_vars['f']->_loop = true;
 $_smarty_tpl->tpl_vars['food_ID']->value = $_smarty_tpl->tpl_vars['f']->key;
?>
                    <tr>
                        <td><?php echo $_smarty_tpl->tpl_vars['f']->value->title;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['f']->value->quantity;?>
 <?php echo $_smarty_tpl->tpl_vars['f']->value->unit_of_measure;?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['f']->value->price;?>
 <?php echo $_smarty_tpl->tpl_vars['f']->value->price_unit;?>
</td>
                        <td>
                            <button value="<?php echo $_smarty_tpl->tpl_vars['f']->value->food_ID;?>
" class="w3-btn-block-smaller w3-sunshine"  name="change_button">Change</button>
                        </td>
                        <td>
                            <button value="<?php echo $_smarty_tpl->tpl_vars['f']->value->food_ID;?>
" class="w3-btn-block-smaller w3-red" name="delete_button">Delete</button>
                        </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <?php if ($_smarty_tpl->tpl_vars['all_fields_required']->value==true){?>
            <div id="error">
                <h3>All fields marked with <span id="required_fields">*</span> are required. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['quantity_must_be_numeric']->value==true){?>
            <div id="error">
                <h3>Quantity must be numeric value. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['price_must_be_numeric']->value==true){?>
            <div id="error">
                <h3>Price must be numeric value. Please try again</h3>
            </div>
        <?php }elseif($_smarty_tpl->tpl_vars['img_type_not_suported']->value==true){?>
            <div id="error">
                <h3>Image type not suported. Please use .png or .jpg pictures</h3>
            </div>
        <?php }?>
        <?php if ($_smarty_tpl->tpl_vars['are_you_sure']->value==true){?>
            <div id="error">
                <h3>Are you sure you want to delete <?php echo $_smarty_tpl->tpl_vars['meal_title']->value;?>
?</h3>
                <div class="row">
                    <div class="col-md-6">
                        <button class="w3-btn-block w3-red" name="no_button">No</button>
                    </div>
                    <div class="col-md-6">
                        <button class="w3-btn-block w3-fresh" name="yes_button" value="<?php echo $_smarty_tpl->tpl_vars['meal_id']->value;?>
">Yes</button>
                    </div>
                </div>
            </div>
       <?php }else{ ?>
            <?php if ($_smarty_tpl->tpl_vars['change']->value==true){?>
                <div id="info-for-change">
                    <h4><?php echo $_smarty_tpl->tpl_vars['meal_title']->value;?>
 Information</h4>

                    <label for="title">Title:</label>
                    <input type="text" name="title" value="<?php echo $_smarty_tpl->tpl_vars['meal_title']->value;?>
">

                    <label for="description">Description:</label>
                    <textarea name="description" id="" cols="30" rows="10"><?php echo $_smarty_tpl->tpl_vars['meal_description']->value;?>
</textarea>

                    <label for="quantity">Quantity<span id="required_fields">*</span>:</label>  
                    <input type="text" name="quantity" maxlength="4" value="<?php echo $_smarty_tpl->tpl_vars['meal_quantity']->value;?>
">

                    <label for="unit_of_measure">Unit of measure<span id="required_fields">*</span>:</label>  
                    <select name="unit_of_measure" id="">
                        <?php if ($_smarty_tpl->tpl_vars['meal_unit_of_measure']->value=='grams'){?>
                            <option value="grams">Grams</option>
                            <option value="kg">Kg</option>
                            <option value="piece">Piece</option>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['meal_unit_of_measure']->value=='kg'){?>
                            <option value="kg">Kg</option>
                            <option value="grams">Grams</option>
                            <option value="piece">Piece</option>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['meal_unit_of_measure']->value=='piece'){?>
                            <option value="piece">Piece</option>
                            <option value="grams">Grams</option>
                            <option value="kg">Kg</option>                        
                        <?php }?>

                    </select>

                    <label for="category">Category<span id="required_fields">*</span>:</label>  
                    <select name="category" id="">
                        <option value="<?php echo $_smarty_tpl->tpl_vars['meal_category_id']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['meal_category_type']->value;?>
</option>
                        <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['category']->_loop = false;
 $_smarty_tpl->tpl_vars['category_id'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['categories']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
$_smarty_tpl->tpl_vars['category']->_loop = true;
 $_smarty_tpl->tpl_vars['category_id']->value = $_smarty_tpl->tpl_vars['category']->key;
?>
                            <?php if ($_smarty_tpl->tpl_vars['meal_category_id']->value!=$_smarty_tpl->tpl_vars['category']->value->category_id){?>
                                <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value->category_id;?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value->type;?>
</option>
                            <?php }?>
                        <?php } ?>
                    </select>

                    <label for="price">Price<span id="required_fields">*</span>:</label>  
                    <input type="text" name="price" maxlength="6" value="<?php echo $_smarty_tpl->tpl_vars['meal_price']->value;?>
">

                    <label for="price_unit">Price unit<span id="required_fields">*</span>:</label>  
                    <select name="price_unit" id="">
                        <option value="eur">EUR</option>
                    </select>

                    <label for="image">Picture<span id="required_fields">*</span></label>
                    <input type="file" name="image" id="">

                    <button name="change_final" id="change_final" class="w3-btn-block w3-fresh" value="<?php echo $_smarty_tpl->tpl_vars['meal_id']->value;?>
">Change</button>
                </div>
            <?php }?>
        <?php }?>
    </form>
<?php }?><?php }} ?>