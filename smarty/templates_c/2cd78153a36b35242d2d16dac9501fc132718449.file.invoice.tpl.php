<?php /* Smarty version Smarty-3.1.13, created on 2016-09-18 09:18:00
         compiled from "tpl\invoice.tpl" */ ?>
<?php /*%%SmartyHeaderCode:250857dbf21795c4c2-55855188%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2cd78153a36b35242d2d16dac9501fc132718449' => 
    array (
      0 => 'tpl\\invoice.tpl',
      1 => 1474188687,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '250857dbf21795c4c2-55855188',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.13',
  'unifunc' => 'content_57dbf217c0e4d8_72160065',
  'variables' => 
  array (
    'all_companies' => 0,
    'item_com' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_57dbf217c0e4d8_72160065')) {function content_57dbf217c0e4d8_72160065($_smarty_tpl) {?><!DOCTYPE html>
<html>
<head>
	<title>Craft_food - Invoice</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/mainstyle.css">
    <script type="text/javascript" src="jspdf/libs/sprintf.js"></script>
	<script type="text/javascript" src="jspdf/jspdf.js"></script>
	<script type="text/javascript" src="jspdf/libs/base64.js"></script>
	<script type="text/javascript" src="tableExport.js"></script>
	<script type="text/javascript" src="jquery.base64.js"></script>

    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
        select{
            display: inline-block;
            min-width:265px;
            min-height: 45px;
            width: 100%;
            border-width: 3px;
            border-color: rgba(50, 50, 50, 0.14);
            margin: 10px 10px 10px 0px;
        }
    </style>
</head>
<body>
    <div id="change_info">
	   <h3>Make invoice report</h3>
    </div>
	<form name="" action="database/export_pdf.php" method="post">
	<select id="days_display" name="days_display">
		<option value="1">daily</option>
		<option value="7">weekly</option>
		<option value="30">monthly</option>
	</select>
	<select id="choose_company" name="choose_company">
	 <?php  $_smarty_tpl->tpl_vars['item_com'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item_com']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['all_companies']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item_com']->key => $_smarty_tpl->tpl_vars['item_com']->value){
$_smarty_tpl->tpl_vars['item_com']->_loop = true;
?>
        <option id="company<?php echo $_smarty_tpl->tpl_vars['item_com']->value->pib;?>
" value="<?php echo $_smarty_tpl->tpl_vars['item_com']->value->pib;?>
"><?php echo $_smarty_tpl->tpl_vars['item_com']->value->company_name;?>
</option>
     <?php } ?>
     </select>

     	<input type="submit" name="exportPDF" value="Export to pdf" class="w3-btn-block w3-sunshine">
     </form>


     <button onclick="return_invoice()" class="w3-btn-block w3-red">Read</button>
     <hr>
     <div id="invoice_display"></div>
     

     <script type="text/javascript">
     	function selected_company()
     	{
     		var e = document.getElementById("choose_company");
            var choosed_company = e.options[e.selectedIndex].value;
            return choosed_company;

     	}
     	function selected_days()
     	{
     		var e = document.getElementById("days_display");
            var choosed_days = e.options[e.selectedIndex].value;
            return choosed_days;

     	}

     	function return_invoice()
     	{
     		
     		var selected_days1=selected_days();
     		var selected_company1= selected_company();
     		
                var xhttp = new XMLHttpRequest();
			    xhttp.onreadystatechange = function() {
			        if (this.readyState == 4 && this.status == 200) {
			            document.getElementById("invoice_display").innerHTML = this.responseText;
			        }
			    };
			    xhttp.open("GET", "database/return_invoice.php?selected_days="+selected_days1+"&selected_company="+selected_company1+"", true);
			    xhttp.send();

     	}
     	
     	

     </script>
</body>
</html><?php }} ?>