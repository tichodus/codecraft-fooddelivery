<?php
	include 'core/init.php';
	include 'includes/overall/header.php';
	
	$user_id = $user_data['user_id'];
	$user_dat=return_user($user_id);
	$id_company=$user_dat[0]->company;

	if (logged_in_as_admin($user_id) && is_catering_company($id_company)) 
	{
		$all_companies = return_all_companies(0);

		$smarty->assign('all_companies',$all_companies);
		$smarty->display('invoice.tpl');
	}
    include 'includes/overall/footer.php';

?>