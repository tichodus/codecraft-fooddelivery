<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $first_name         = $user_data['first_name'];
    $last_name          = $user_data['last_name'];
    $username           = $user_data['username'];
    $password           = $user_data['password'];
    $email              = $user_data['email'];
    $privileges         = $user_data['privileges'];
    $spent              = $user_data['spent'];
    $company            = $user_data['company'];
    $automatic_delivery = $user_data['automatic_delivery'];
    $image_id           = $user_data['image'];
    $image              = get_image($image_id);
    $image_path         = $image[0]->image_path;

    $company = get_company($company);
    $company_name = $company[0]->company_name;
    $company_description = $company[0]->company_description;
    $company_pib = $company[0]->pib;
    $company_type = $company[0]->company_type;

    $empty_first_name   = false;
    $empty_last_name    = false;
    $empty_email        = false;
    $empty_old_password = false;
    $empty_new_password = false;
    $wrong_old_password = false;
    $redirect           = false;

    if(isset($_POST['final_change_button']))
    {
        if(empty($_POST['first_name']))
            $empty_first_name = true;
        else if(empty($_POST['last_name']))
            $empty_last_name = true;
        else if(empty($_POST['old_password']))
            $empty_old_password = true;
        else if(empty($_POST['new_password']))
            $empty_new_password = true;
        else if(empty($_POST['email']))
            $empty_email = true;
        else if($password != md5($_POST['old_password']))
            $wrong_old_password = true;
        else
        {
            $user_id            = $user_data['user_id'];
            $first_name_final   = $_POST['first_name'];
            $last_name_final    = $_POST['last_name'];
            $password_final     = md5($_POST['new_password']);
            $email_final        = $_POST['email'];

            $new_image_name     = $_FILES['image']['name'];
            $new_image_type     = $_FILES['image']['type'];
            if($new_image_type == 'image/png')
                $extension = '.png';
            else if($new_image_type == 'image/jpeg')
                $extension = '.jpg';
            $new_image_size     = $_FILES['image']['size'];
            $new_image_tmp_name = $_FILES['image']['tmp_name'];

            if(empty($new_image_tmp_name))
                alter_user_as_user($user_id, $first_name_final, $last_name_final, $password_final, $email_final);
            else
            {
                if(image_exists($user_id) == false)
                {
                    move_uploaded_file($new_image_tmp_name, "images/users/" . $username . "$extension");
                    $image_path = "images/users/" . $username . "$extension";
                    insert_image($user_id, $image_path);
                    alter_user_as_user($user_id, $first_name_final, $last_name_final, $password_final, $email_final);
                    $img_id = get_img_id_from_user_id($user_id);
                    $img_id = $img_id[0]->image_id;
                    alter_user_img($img_id, $user_id);
                }
                $img_id = get_img_id_from_user_id($user_id);
                $img_id = $img_id[0]->image_id;
                alter_user_img($img_id, $user_id);
                move_uploaded_file($new_image_tmp_name, "images/users/" . $username . "$extension");
                $image_path = "images/users/" . $username . "$extension";
                alter_user_as_user($user_id, $first_name_final, $last_name_final, $password_final, $email_final);
            }
            $redirect = true;
            $image              = get_image($image_id);
        }
    }
    
    $user_id = $user_data['user_id'];

    $smarty->assign('user_id', $user_id);
    $smarty->assign('first_name', $first_name);
    $smarty->assign('last_name', $last_name);
    $smarty->assign('username', $username);
    $smarty->assign('password', $password);
    $smarty->assign('email', $email);
    $smarty->assign('privileges', $privileges);
    $smarty->assign('spent', $spent);
    $smarty->assign('company', $company);
    $smarty->assign('automatic_delivery', $automatic_delivery);

    $smarty->assign('empty_first_name', $empty_first_name);
    $smarty->assign('empty_last_name', $empty_last_name);
    $smarty->assign('empty_email', $empty_email);
    $smarty->assign('empty_old_password', $empty_old_password);
    $smarty->assign('empty_new_password', $empty_new_password);
    $smarty->assign('time', time());
    $smarty->assign('wrong_old_password', $wrong_old_password);
    $smarty->assign('redirect', $redirect);

    $smarty->assign('company_type', $company_type);
    $smarty->assign('company_name', $company_name);
    $smarty->assign('image_path', $image_path);

    $smarty->display('profile.tpl');

    include 'includes/overall/footer.php';    
?>