<?php
    class User
    {
        public $user_id;
        public $username;
        public $first_name;
        public $last_name;
        public $email;
        public $privileges;
        public $active;
        public $spent;
        public $company;
        public $automatic_delivery;
        public $image;

        public function __construct($user_id, $username="", $first_name="", $last_name="", $email="", $privileges, $active, $spent, $company, $automatic_delivery, $image="")
        {
            $this->user_id              =   $user_id;
            $this->username             =   $username;
            $this->first_name           =   $first_name;
            $this->last_name            =   $last_name;
            $this->email                =   $email;
            $this->privileges           =   $privileges;
            $this->active               =   $active;
            $this->spent                =   $spent;
            $this->company              =   $company;
            $this->automatic_delivery   =   $automatic_delivery;
            $this->image                =   $image;
        }
    }
?>