<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    if(logged_in() === true)
    {
        automatic_order();
        automatic_order_lunch();

        $i = 0;
        $username = $user_data['username'];
        $not_registered = false;
        include('database/return_food.php');
        include('getImage.php');

        $user_id    = $user_data['user_id'];
        $company_id = $user_data['company'];

        $meals              = return_all_meals();

        foreach ($meals as $food_ID => $meal) {
            $pom = get_image($meal->image_id);
            $pom_1 = return_category($meal->category_id);
            $pom_2 = return_user($meal->user_id);
            $pom_3 = return_company($pom_2[0]->company);

            $meal->image = $pom[0]->image_path;
            $meal->category = $pom_1[0]->type;
            $meal->company = $pom_3[0]->company_name;
            $meal->pib = $pom_3[0]->pib;
        }

        $number_of_meals    = count($meals);

        $company        = get_company($company_id);
        $ketering_or_it = $company[0]->company_type;
        /*
            AKO JE $ketering_or_it == 0  ---> NEKA IT FIRMA
            AKO JE $ketering_or_it == 1  ---> KETERING KOMPANIJA
        */

        $type_of_service    = $company[0]->type_of_plan;
        /*
            AKO JE $type_of_service == 0  ---> samo doručak može za korisnika
            AKO JE $type_of_service == 1  ---> samo ručak može za korisnika
            AKO JE $type_of_service == 2  ---> MOŽE OBA!!!
        */
       
        $enable_only_breakfast = false;
        $enable_only_lunch = false;
        $enable_both = false;

        if($type_of_service == 0)
            $enable_only_breakfast = true;
        else if($type_of_service == 1)
            $enable_only_lunch = true;
        else
            $enable_both = true;

        $company_id = $user_data['company'];
        $delivery_reports = get_delivery_report($company_id);

        $today_month    =   date('m');
        $maxDays        =   date('t');
        $today_day      =   date('d');
        $today_year     =   date('Y');
        $counter        =   0;


        $smarty->assign('meals', $meals);
        $smarty->assign('number_of_meals', $number_of_meals);
        $smarty->assign('ketering_or_it', $ketering_or_it);
        $smarty->assign('i', $i);

        $smarty->assign('enable_only_breakfast', $enable_only_breakfast);
        $smarty->assign('enable_only_lunch', $enable_only_lunch);
        $smarty->assign('enable_both', $enable_both);

        $smarty->assign('type_of_service', $type_of_service);
        
        $smarty->assign('food_array', $food_array);
        $smarty->assign('today_month', $today_month);
        $smarty->assign('maxDays', $maxDays);
        $smarty->assign('today_day', $today_day);
        $smarty->assign('today_year', $today_year);
        $smarty->assign('counter', $counter);
        $smarty->assign('user_id', $user_id);
        $smarty->assign('company_id', $company_id);
        $smarty->assign('username', $username);
    }
    else
        $not_registered = true;
    $smarty->assign('not_registered', $not_registered);

    $smarty->display('index.tpl');
    include 'includes/overall/footer.php';
?>