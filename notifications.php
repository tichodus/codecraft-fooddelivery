<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $user_id = $user_data['user_id'];
    $alerts = get_alerts($user_id);

    foreach ($alerts as $admin_alerts_id => $alert) {
        $user = return_user($alert->user_id);
        $alert->first_name = $user[0]->first_name;
        $alert->last_name = $user[0]->last_name;

        $image = get_image($user[0]->image);
        $alert->image_path = $image[0]->image_path;
    }

    $smarty->assign('alerts', $alerts);
    $smarty->display('notifications.tpl');
    include 'includes/overall/footer.php';
?>