<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $success = false;

    $items = return_items_from_cart($user_data['user_id']);
    $meals = array();
    $meals_id = array();

    $user_id = $user_data['user_id'];

    foreach ($items as $cart_id => $item) {
        $meals[] = return_meal($item->food_ID);
        $item->flag = 0;
    }

    $br_items = count($items);

    $sum = 0;
    foreach ($meals as $m) 
    {
        foreach ($items as $cart_id => $c)
        {
            if($c->food_ID == $m[0]->food_ID && $c->flag == 0)
            {
                $m[0]->quantity         = $m[0]->quantity * $c->duplicate;
                $m[0]->price            = $m[0]->price * $c->duplicate;
                
                $duplicate[]            = $c->duplicate;
                $type_of_service[]      = $c->type_of_service;
                $date[]                 = $c->date;
                $item_id[]              = $c->cart_id;
                $c->flag = 1;
            }
        }
        $pom = get_image($m[0]->image_id);
        $pom1 = $pom[0]->image_path;
        $m[0]->image_id = $pom1;
        $sum += $m[0]->price;
    }

    $i = 0;
    $counter = count($meals);
    foreach ($meals as $m) 
    {
        if($i <= $counter)
        {
            $m[0]->type_of_service = $type_of_service[$i];
            $m[0]->date            = $date[$i];
            $m[0]->duplicate       = $duplicate[$i];
            $m[0]->cart_id         = $item_id[$i];
            $i++;
        }
    }

    $j = 0;
    if(isset($_POST['order']))
    {
        $orders_by_this_user = return_items_from_cart($user_id);

        $company_id = $user_data['company'];

        foreach ($orders_by_this_user as $cart_id => $item) {
            $food = return_meal($item->food_ID);
            $user_d = return_user($food[0]->user_id);

            $day = substr($item->date, -2);
            $month = substr($item->date, -5, 2);
            $year = substr($item->date, -10, 4);

            $notes = $_POST['note'];

            $no_of_notes = count($notes);
            $flags = array();

            for ($i=0; $i < $no_of_notes; $i++) { 
                $flags[$i]['flag'] = 0;
            }

            $item->note = $notes[$j];
            $j++;

            $date = $year . $month . $day;

            insert_order($item->food_ID, $item->user_id, $date, $company_id, $item->type_of_service, $item->duplicate, $item->note);

            delete_from_cart($user_id);
        }
        $success = true;
    }

    $smarty->assign('meals', $meals);
    $smarty->assign('success', $success);
    $smarty->assign('user_id', $user_id);
    $smarty->assign('sum', $sum);
    $smarty->display('my-cart.tpl');
    include 'includes/overall/footer.php';
?>