<?php 
    include 'core/functions/rates.php';
    include 'core/functions/food.php';

    $value      = $_POST['value'];
    $food_id    = $_POST['food_id'];
    $user_id    = $_POST['user_id'];

    update_rate($user_id, $food_id, $value);

    $this_meal = return_rate_for_meal($food_id);
    $number_of_ratings = count($this_meal);

    $sum = 0;
    foreach ($this_meal as $rate_id => $m) {
        $sum += $m->rate;
    }

    $sum = round($sum / $number_of_ratings);
    update_food_rate($sum, $food_id);
?>