<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $companies  = return_companies();
    $meals      = return_all_meals();

    $company_exists = false;
    $all_fields_required = false;
    $username_exists = false;
    $email_exists = false;
    $success_personal = false;
    $success_company = false;
    $admin_first_name = '';
    $username = '';
    $email = '';
    $pib = 0;
    $go_to_page_two = false;

    $first_name             = '';
    $last_name              = '';
    $username               = '';
    $password               = '';
    $email                  = '';
    $company_name           = '';
    $company_description    = '';
    $company_type           = 0;
    $pib                    = 0;
    $company_type           = 0;

    $breakfast_time         = 0;

    $lunch_time             = 0;

    if(isset($_POST['submit']))
    {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $email = $_POST['email'];
        $username = $_POST['username'];
        $password = $_POST['password'];

        if(empty($first_name) || empty($username) || empty($password) || empty($email))
            $all_fields_required = true;

        if($_POST['submit'] == 1)
        {
            //personal
            $my_company = $_POST['my_company'];
            if($my_company == 0)
                $all_fields_required = true;
            else if(user_exists($username) == true)
                $username_exists = true;
            else if(email_exists($email) == true)
                $email_exists = true;
            else
            {
                register_user($username, $password, $first_name, $last_name, $email, 0, 0, 0, $my_company);

                $users = return_all_users();
                $no_of_users = count($users);
                $last_user = $users[$no_of_users - 1];

                $companyyy = return_company($my_company);
                if($companyyy[0]->company_type == 0)
                {
                    foreach ($meals as $meal) {
                        create_rates_for_user($last_user->user_id, $meal->food_ID, 0);
                    }
                }

                $admin = get_admin_id_for_company($my_company);
                $admin_id = $admin[0]->user_id;
                $admin_first_name = $admin[0]->first_name;

                $user = return_all_users();
                $user_counter = count($user);
                $user_id = $user[$user_counter-1]->user_id;

                alert_admin_for_new_user($admin_id, $user_id);

                $success_personal = true;
                header( "refresh:3;url=index.php" );
            }
        }
        else
        {
            //company
            $company_name = $_POST['company_name'];
            $company_description = $_POST['company_description'];
            $pib = $_POST['pib'];

            if(empty($company_name) || empty($pib) || empty($_POST['is_it_catering']))
                $all_fields_required = true;
            else if(check_pib($pib) == true)
                $company_exists = true;
            else if($_POST['is_it_catering'] == 'yes')
            {
                $company_type = 1;
                insert_company($company_name, $company_description, $pib, $company_type);
                register_user($username, $password, $first_name, $last_name, $email, 1, 1, 0, $pib);

                $success_company = true;
                header( "refresh:3;url=index.php" );
            }
            else if($_POST['is_it_catering'] == 'no')
            {
                $company_type = 0;
                insert_company($company_name, $company_description, $pib, $company_type);
                register_user($username, $password, $first_name, $last_name, $email, 1, 1, 0, $pib);

                $success_company = true;
                header( "refresh:3;url=index.php" );
            }
        }
    }

    if(isset($_POST['next']))
    {
        $first_name     = $_POST['first_name'];
        $last_name      = $_POST['last_name'];
        $email          = $_POST['email'];
        $username       = $_POST['username'];
        $password       = $_POST['password'];

        $company_name = $_POST['company_name'];
        $company_description = $_POST['company_description'];
        $pib = $_POST['pib'];

        if(empty($first_name) || empty($username) || empty($password) || empty($email) || empty($company_name) || empty($pib))
            $all_fields_required = true;

        else if(check_pib($pib) == true)
            $company_exists = true;
        else if($_POST['is_it_catering'] == 'yes')
        {
            $company_type = 1;
            $go_to_page_two = true;
        }
        else if($_POST['is_it_catering'] == 'no')
        {
            $company_type = 0;
            $go_to_page_two = true;
        }

        $_SESSION['first_name']             = $first_name;
        $_SESSION['last_name']              = $last_name;
        $_SESSION['username']               = $username;
        $_SESSION['email']                  = $email;
        $_SESSION['password']               = $password;
        $_SESSION['company_name']           = $company_name;
        $_SESSION['company_description']    = $company_description;
        $_SESSION['pib']                    = $pib;
        $_SESSION['company_type']           = $company_type;
    }

    if(isset($_POST['final_submit']))
    {
        $first_name             = $_SESSION['first_name'];
        $last_name              = $_SESSION['last_name'];
        $username               = $_SESSION['username'];
        $email                  = $_SESSION['email'];
        $password               = $_SESSION['password'];
        $company_name           = $_SESSION['company_name'];
        $company_description    = $_SESSION['company_description'];
        $pib                    = $_SESSION['pib'];
        $company_type           = $_SESSION['company_type'];

        $type_of_plan_pom       = $_POST['type_of_plan'];
        if($type_of_plan_pom == 'breakfast')
            $type_of_plan = 0;  //samo dorucak
        else if($type_of_plan_pom == 'lunch')
            $type_of_plan = 1;  //samo rucak
        else if($type_of_plan_pom == 'breakfast_and_lunch')
            $type_of_plan = 2;  //i dorucak i rucak

        if(isset($_POST['breakfast_time']))
            $breakfast_time         = $_POST['breakfast_time'];

        if(isset($_POST['lunch_timec']))
            $lunch_time             = $_POST['lunch_time'];

        insert_not_catering_company($company_name, $company_description, $pib, $company_type, $type_of_plan, $breakfast_time, $lunch_time);
        register_user($username, $password, $first_name, $last_name, $email, 1, 1, 0, $pib);
        $success_company = true;
    }

    $pom_niz = array();
    foreach ($companies as $c) {
        $pom_niz[$c->pib] = $c->company_name;
    }

    $companies = json_encode($pom_niz);

    $smarty->assign('companies', $companies);
    $smarty->assign('company_exists', $company_exists);
    $smarty->assign('all_fields_required', $all_fields_required);
    $smarty->assign('pib', $pib);
    $smarty->assign('username_exists', $username_exists);
    $smarty->assign('username', $username);
    $smarty->assign('email_exists', $email_exists);
    $smarty->assign('email', $email);
    $smarty->assign('success_personal', $success_personal);
    $smarty->assign('success_company', $success_company);
    $smarty->assign('admin_first_name', $admin_first_name);
    $smarty->assign('go_to_page_two', $go_to_page_two);
    $smarty->display('registration.tpl');
    include 'includes/overall/footer.php';
?>