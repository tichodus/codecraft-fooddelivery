<nav class="navbar navbar-default" id="nav">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>      
               
      </button>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="./index.php">Home</a></li>
        <?php
          if (logged_in() === true)
          {
            $user_id = $_SESSION['user_id'];
            $company_pib = return_user($user_id);
            $company_pib = $company_pib[0]->company;
        ?>
          
        <?php
            if(logged_in_as_admin($user_id) === true && is_catering_company($company_pib) == true)
            {
              //ADMIN KETERING KOMPANIJE
        ?>
              <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
                <li class=""><a href="./list-of-users.php">List of users</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class=""><a href="./add-new-meal.php">Add new meal</a></li>
                    <li class=""><a href="./delete-meal.php">Change / Delete meal</a></li>
                  </ul>
                </li>
                <li class=""><a href="./all_meal.php">Reports</a></li>
                <li class=""><a href="./invoice.php">Invoice</a></li>
              </ul>
        <?php
            }
            else if(logged_in_as_admin($user_id) === true && is_catering_company($company_pib) == false)
            {
              $catering_companies = return_companies_with_type(1);
              //ADMIN NEKE DRUGE (NE KETERING) KOMPANIJE
        ?>
                <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
                <li class=""><a href="./list-of-users.php">List of users</a></li>
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
        <?php
                foreach ($catering_companies as $pib => $company) {
                  echo '<li class=""><a href="./menu.php?id=' . $company->pib .'">' . $company->company_name . '</a></li>';
                }
        ?>
                  </ul>
                </li>
                <li class=""><a href="./client_invoice.php">Invoice</a></li>
              </ul>
        <?php
            }
            else if(logged_in_as_admin($user_id) === false && is_catering_company($company_pib) == true)
            {
              //KUVAR
        ?>
              <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
                <li class=""><a href="./Orders.php">Orders</a></li>
              </ul>
        <?php
            }
            else
            {
              $catering_companies = return_companies_with_type(1);
              //$user_id = 0;
        ?>
                <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
                <li class="dropdown">
                  <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu
                  <span class="caret"></span></a>
                  <ul class="dropdown-menu">
        <?php
                foreach ($catering_companies as $pib => $company) {
                  echo '<li class=""><a href="./menu.php?id=' . $company->pib .'">' . $company->company_name . '</a></li>';
                }
        ?>
                  </ul>
                </li>
                <li class=""><a href="./client_invoice_for_client.php">Invoice</a></li>
              <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
            </ul>
        <?php
            }
            include 'widgets/logout.php';
          }
          else
          {
            $user_id = 0;
        ?>
              <input type="hidden" value="<?php echo $user_id; ?>" id="hidden_user_id">
            </ul>
        <?php
            include 'widgets/login.php';
          }
        ?>
    </div>
  </div>
</nav>