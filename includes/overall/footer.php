        </div>
    </div>
<?php
    include 'includes/footer.php';
?>
<script>
    $( document ).ready(function() {
        var user_id = $("#hidden_user_id").val();

        if(user_id != 0)
        {
            $.ajax({ type: "POST",
            url: "get-notifications.php",
            data: { user_id: user_id },
            dataType: "json",
            success: function(response)
            {
                if(response[0] == 0)
                {
                    var final_response = '<div class="row"><div class="col-md-12"><div id="notification_name"> <p>No new notifications </p></div></div></div>';
                    document.getElementById("notification-wrapper").innerHTML = final_response;
                    $('#see-all-wrapper').remove();
                }
                else if(response[3] == 0)
                {
                    var final_response = '<div id="wrapper'+ response[2] +'"><div class="row"><div class="col-md-3"><div id="notification_picture"> <img src="' + response[0] + '" width="60" height="60"></div></div><div class="col-md-9"><div id="notification_name"><b>' + response[1] + '</b> <p>requested joining your team</p></div><br></div></div><div class="row"><div class="col-md-6"><div id="left-button"><button class="w3-btn-block w3-fresh" value="' + response[2] + '" id="accept">Accept</button></div></div><div class="col-md-6"><div id="right-button"><button class="w3-btn-block w3-red" value="' + response[2] + '" id="decline">Decline</button></div></div></div></div>';
                    document.getElementById("notification-wrapper").innerHTML = final_response;
                    $('#notification-glyphicon').addClass('red');
                }
                else
                {
                    var final_response = '<div id="wrapper'+ response[2] +'"><div class="row"><div class="col-md-3"><div id="notification_picture"> <img src="' + response[0] + '" width="60" height="60"></div></div><div class="col-md-9"><div id="notification_name"><b>' + response[1] + '</b> <p>requested joining your team</p></div><br></div></div><div class="row"><div class="col-md-6"><div id="left-button"><button class="w3-btn-block w3-fresh" value="' + response[2] +'" id="accept">Accept</button></div></div><div class="col-md-6"><div id="right-button"><button class="w3-btn-block w3-red" value="' + response[2] +'" id="decline">Decline</button></div></div></div></div><hr><div id="wrapper'+ response[5] +'"><div class="row"><div class="col-md-3"><div id="notification_picture"> <img src="' + response[3] + '" width="60" height="60"></div></div><div class="col-md-9"><div id="notification_name"><b>' + response[4] + '</b> <p>requested joining your team</p></div><br></div></div><div class="row"><div class="col-md-6"><div id="left-button"><button class="w3-btn-block w3-fresh" value="' + response[5] + '" id="accept">Accept</button></div></div><div class="col-md-6"><div id="right-button"><button class="w3-btn-block w3-red" value="' + response[5] + '" id="decline">Decline</button></div></div></div></div>';
                    document.getElementById("notification-wrapper").innerHTML = final_response;
                }
            },
            error: function(ts) { alert(ts.responseText) }
        });
        }
    });

    $(document).on('click','#accept', function() {
        var user_id = $("#accept").val();
        var div = 'wrapper';

        div = div + user_id;

        $.ajax({ type: "POST",
            url: "accept-employee.php",
            data: { user_id: user_id },
        });

        document.getElementById(div).remove();
    });

    $(document).on('click','#decline', function() {
        var user_id = $("#decline").val();
        var div = 'wrapper';

        div = div + user_id;

        $.ajax({ type: "POST",
            url: "decline-employee.php",
            data: { user_id: user_id },
        });

        document.getElementById(div).remove();
    });
</script>
</body>
</html>

