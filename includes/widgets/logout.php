<ul class="nav navbar-nav navbar-right">

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span id="notification-glyphicon" class="glyphicon glyphicon-exclamation-sign"></span> <span class="caret"></span></a>
            <ul id="login-dp" class="dropdown-menu">
                <li>
                    <div class="row" id="notification-container">
                        <div class="col-md-12">
                            <div class="form-group">
                               <label class="sr-only"></label><div id="notification-main-title">Notifications</div><hr>
                               <div id="notification-wrapper">
                                 <div class="row">
                                     <div class="col-md-2">
                                         <div id="notification_name"> </div>
                                     </div>
                                     <div class="col-md-10">
                                         <div id="notificationsBody" class="notifications"></div>
                                     </div>
                                 </div>
                                 <div class="row">
                                     <div class="col-md-12">
                                         
                                     </div>
                                 </div>
                               </div>
                               <hr>
                            </div>
                            <div id="see-all-wrapper">
                                <div class="form-group">
                                    <form action="./notifications.php">
                                        <a href="notifications.php"><button type="submit" class="btn btn-primary raised btn-block">See all</button></a>
                                    </form>
                                </div>
                            </div>
                </li>
            </ul>
    </li>

<?php
    if(is_catering_company($company_pib) == false)
    {
        $number_of_items = return_items_from_cart($user_data['user_id']);
        $number_of_items = count($number_of_items);
?>
        <li><a href="my-cart.php" id="cart"><i class="fa fa-shopping-cart"></i><span class="sunshine glyphicon glyphicon-shopping-cart"></span>  <span class="sunshine-bg badge" id="badge-change"><?php echo $number_of_items ?></span></a></li>
<?php
    }
?>      
    <li class=""><a href="./profile.php">Profile</a></li>
    <li class=""><a href="./logout.php">Log out</a></li>
</ul>

