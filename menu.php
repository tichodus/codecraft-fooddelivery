<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    include 'comments_allowed.php';

    $company_id     = $user_data['company'];
    $company        = get_company($company_id);
    $ketering_or_it = $company[0]->company_type;

    $no_comments = false;
    $allow_comments = false;

    $comments = array();

    $food = array();
    $user_rate = 0;
    $ketering_or_it = $company[0]->company_type;
    /*
        AKO JE $ketering_or_it == 0  ---> NEKA IT FIRMA
        AKO JE $ketering_or_it == 1  ---> KETERING KOMPANIJA
    */

    $type_of_service    = $company[0]->type_of_plan;
    /*
        AKO JE $type_of_service == 0  ---> samo doručak može za korisnika
        AKO JE $type_of_service == 1  ---> samo ručak može za korisnika
        AKO JE $type_of_service == 2  ---> MOŽE OBA!!!
    */

    $few_meals = false;
    $one_meal = false;

    $food_id = 0;

    $meals = array();
    $images = array();

    if(isset($_GET['id']))
    {
        $one_meal = false;
        $few_meals = true;
        $pib = $_GET['id'];
        $admin = get_admin_id_for_company($pib);

        $user_id = $user_data['user_id'];

        $admin_id = $admin[0]->user_id;
        $meals = return_meals($admin_id);

        foreach ($meals as $m) {
            $image = get_image($m->image_id);
            $images[] = $image[0]->image_path;

            $category = return_category($m->category_id);
            $categories[] = $category[0]->type;
        }

        $j = 0;
        foreach ($meals as $m) {
            $m->image_path = $images[$j];
            $m->category = $categories[$j];
            $j++;
        }
    }
    else if(isset($_GET['foodid']))
    {
        $user_id = $user_data['user_id'];

        $one_meal = true;
        $few_meals = false;

        $food_id = $_GET['foodid'];
        $food = return_meal($food_id);

        $comments = return_comments_for_meal($food_id);

        foreach ($comments as $comment_id => $comment) {
            $user = return_user($comment->employee_id);
            $image = get_image($user[0]->image);
            $comment->image_path = $image[0]->image_path;
            $comment->user_first_name = $user[0]->first_name;
        }

        if(empty($comments))
            $no_comments = true;
        else
            $no_comments = false;

        $rate = return_rate($user_id, $food_id);
        if(!empty($rate))
            $user_rate = $rate[0]->rate;

        $image_pom = get_image($food[0]->image_id);
        $food[0]->image_path = $image_pom[0]->image_path;

        $category_pom = return_category($food[0]->category_id);
        $food[0]->category = $category_pom[0]->type;

        if(comments_allowed($user_id, $food_id) == true || comments_allowed_lunch($user_id, $food_id))
            $allow_comments = true;
        else
            $allow_comments = false;
    }

    if(isset($_POST['submit_comment']))
    {
        $comment = $_POST['comment'];

        $user_id = $user_data['user_id'];
        $food_id = $_POST['submit_comment'];

        insert_comment($user_id, $food_id, $comment);
    }

    $today_month    =   date('m');
    $maxDays        =   date('t');
    $today_day      =   date('d');
    $today_year     =   date('Y');

    $smarty->assign('today_month', $today_month);
    $smarty->assign('maxDays', $maxDays);
    $smarty->assign('no_comments', $no_comments);
    $smarty->assign('user_rate', $user_rate);
    $smarty->assign('one_meal', $one_meal);
    $smarty->assign('few_meals', $few_meals);
    $smarty->assign('allow_comments', $allow_comments);
    $smarty->assign('food', $food);
    $smarty->assign('food_id', $food_id);
    $smarty->assign('today_day', $today_day);
    $smarty->assign('today_year', $today_year);
    $smarty->assign('meals', $meals);
    $smarty->assign('comments', $comments);
    $smarty->assign('type_of_service', $type_of_service);
    $smarty->assign('user_id', $user_id);
    $smarty->assign('ketering_or_it', $ketering_or_it);
    $smarty->display('menu.tpl');
    include 'includes/overall/footer.php';
?>