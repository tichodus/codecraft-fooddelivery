<?php
    require 'core/db-init.php';
    class FoodOrders
    {
        public $food_ID;
        public $title;
        public $description;
        public $quantity;
        public $unit_of_measure;
        public $price;
        public $price_unit;
        public $image_id;
        public $category_id;
        public $user_id;
        public $ID;
        public $IDfood;
        public $IDemployee;
        public $date;
        public $notes;
        public $company_id;
        public $type_of_meal;
        public $quantity_orders;

        public function __construct($food_ID, $title="", $description="", $quantity, $unit_of_measure="", $price, $price_unit="", $image_id, $category_id, $user_id, $ID, $IDfood, $IDemployee, $date, $notes, $company_id, $type_of_meal, $quantity_orders)
        {
            $this->food_ID          =   $food_ID;
            $this->title            =   $title;
            $this->description      =   $description;
            $this->quantity         =   $quantity;
            $this->unit_of_measure  =   $unit_of_measure;
            $this->price            =   $price;
            $this->price_unit       =   $price_unit;
            $this->image_id         =   $image_id;
            $this->category_id      =   $category_id;
            $this->user_id          =   $user_id;

            $this->$ID              =   $ID;
            $this->IDfood           =   $IDfood;
            $this->IDemployee       =   $IDemployee;
            $this->date             =   $date;
            $this->notes            =   $notes;
            $this->company_id       =   $company_id;
            $this->type_of_meal     =   $type_of_meal;
            $this->quantity_orders  =   $quantity_orders;
        }
        public function to_string()
        {
            return $title;
        }
        
    }
    function return_food_orders($days, $company)
    {
         $host_name      = $GLOBALS['host_name'];
         $db_user        = $GLOBALS['db_user'];
         $db_password    = $GLOBALS['db_password'];
         $db_name        = $GLOBALS['db_name'];

         $selected_days   =$days;
         $selected_company=$company;
         
         $date=date('Ymd');
         $days_ago = date('Ymd', strtotime('-'.$selected_days.' days', strtotime($date)));
         $invoice_food=array();


            $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

            
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }

            $query="SELECT `food_ID`, `title`, `description`, food.quantity, `unit_of_measure`, `price`, `price_unit`, `image_id`, `category_id`, `user_id`, `ID`, `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, orders.quantity FROM food JOIN orders ON food.food_ID=orders.IDfood AND company_id=$selected_company AND date BETWEEN $days_ago AND $date  ORDER BY orders.date";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {

                            $invoice_food[]=new FoodOrders($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8], $row[9], $row[10], $row[11], $row[12], $row[13], $row[14], $row[15], $row[16], $row[17]);
                            
                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            mysqli_close($link);
            return $invoice_food;
    }


    function return_all_ordered($days, $company)
    {
         $host_name      = $GLOBALS['host_name'];
         $db_user        = $GLOBALS['db_user'];
         $db_password    = $GLOBALS['db_password'];
         $db_name        = $GLOBALS['db_name'];

         $selected_days   =$days;
         $selected_company=$company;
         
         $date=date('Ymd');
         $days_after = date('Ymd', strtotime('+'.$selected_days.' days', strtotime($date)));
         $invoice_food=array();


            $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

            
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }

            $query="SELECT `food_ID`, `title`, `description`, food.quantity, `unit_of_measure`, `price`, `price_unit`, `image_id`, `category_id`, food.user_id, `ID`, `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, orders.quantity  FROM `orders` JOIN food ON IDfood=food_id JOIN users ON food.user_id=users.user_id AND users.company=$selected_company AND date BETWEEN $date AND $days_after ORDER BY food_ID ASC";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {

                            $invoice_food[]=new FoodOrders($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8], $row[9], $row[10],$row[11], $row[12], $row[13], $row[14], $row[15], $row[16], $row[17]);
                            
                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            mysqli_close($link);
            return $invoice_food;
    }

    function getOrderyReport($company){



    $date_array=getdate();
    $time=$date_array['hours'];//vraca trenutno vreme
    $time= $time+2; //plus 2 da se sredi vremenska zona
    $time_type='am';
    $date_today=date("Ymd");//trenutni datum
    $date_tommorow=$date_today+1;//sutrasnji datum
    //$array=[];
    $ind=0;
    if ($time>24) {
        $time=$time-24;
    }
    //echo $time;
   // echo $date_tommorow;
    //echo $time+1;
    //die();

    $host_name      = $GLOBALS['host_name'];
    $db_user        = $GLOBALS['db_user'];
    $db_password    = $GLOBALS['db_password'];
    $db_name        = $GLOBALS['db_name'];

    $invoice_food=array();


            $link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

            
            if (mysqli_connect_errno()) {
                printf("Connect failed: %s\n", mysqli_connect_error());
                exit();
            }

            $query="SELECT `food_ID`, `title`, `description`, food.quantity, `unit_of_measure`, `price`, `price_unit`, `image_id`, `category_id`, food.user_id, `ID`, `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, orders.quantity FROM orders inner join food on food_ID=IDfood inner join users on food.user_id = users.user_id inner join companies on pib = company_id WHERE company=$company AND date =$date_today AND (breakfast_time>=$time+3 OR lunch_time>=$time+3) OR date=$date_tommorow;";
            if (mysqli_multi_query($link, $query)) 
            {

                    if ($result = mysqli_use_result($link)) 
                    {
                        
                        while ($row = mysqli_fetch_row($result)) 
                        {

                            $invoice_food[]=new FoodOrders($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8], $row[9], $row[10],$row[11], $row[12], $row[13], $row[14], $row[15], $row[16], $row[17]);
                            
                        }
                        mysqli_free_result($result);
                    }
                    
                
                    if (mysqli_more_results($link))
                        printf("-----------------\n");
        
            }
            mysqli_close($link);
            return $invoice_food;
    }
?>