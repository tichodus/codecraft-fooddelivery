<?php
    include 'core/functions/users.php';
    include 'core/functions/company.php';

    $user_id = $_POST['user_id'];

    delete_user_from_alerts($user_id);
    delete_user($user_id);
?>