<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    $user_id = 0;
    $user_id_pom = 0;
    $user_id_delete = 0;
    $first_name = '';
    $last_name = '';
    $username = '';
    $email = '';
    $privileges = 0;
    $status = 0;
    $spent = 0;
    $change = false;
    $all_fields_required = false;
    $are_you_sure = false;

    $users = return_users($user_data['company']);
    $company = get_company($user_data['company']);
    $company_type = $company[0]->company_type;

    if(isset($_POST['change_button']))
    {
        $user = return_user($_POST['change_button']);

        $user_id_pom    = $_POST['change_button'];
        $first_name     = $user[0]->first_name;
        $last_name      = $user[0]->last_name;
        $username       = $user[0]->username;
        $email          = $user[0]->email;
        $privileges     = $user[0]->privileges;
        $status         = $user[0]->active;
        $spent          = $user[0]->spent;
        $change         = true;
    }

    if(isset($_POST['delete_button']))
    {
        $user = return_user($_POST['delete_button']);
        $user_id_delete = $user[0]->user_id;
        $first_name = $user[0]->first_name;
        $last_name = $user[0]->last_name;
        $are_you_sure = true;
    }

    if(isset($_POST['no_button']))
    {
        $are_you_sure = false;
        $change = true;
    }

    if(isset($_POST['yes_button']))
    {
        $user_id = $_POST['yes_button'];
        if(exists_user_in_alert_for_admin($user_id) == true)
            delete_user_from_alerts($user_id);
        $user_pom = return_user($user_id);
        if($user_pom[0]->image != 1)
            delete_image($user_pom[0]->image);
        delete_user($user_id);
        delete_rates_with_user_id($user_id);
        $users = return_users($user_data['company']);
    }

    if(isset($_POST['change_final']))
    {
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $username = $_POST['username'];
        $email = $_POST['email'];

        if(empty($first_name) || empty($last_name) || empty($username) || empty($email))
        {
            $all_fields_required = true;
            $change = true;
        }
        else
        {
            $user_id = $_POST['change_final'];
            $status = $_POST['status'];
            alter_user($username, $first_name, $last_name, $email, $status, $user_id);

            if($status == 1 && exists_user_in_alert_for_admin($user_id) == true)
            {
                //verifikovan je => izbaci ga iz alerts for admin tabele
                delete_user_from_alerts($user_id);
            }
            $users = return_users($user_data['company']);
        }
    }

    $smarty->assign('users', $users);
    $smarty->assign('company_type',$company_type);

    $smarty->assign('user_id', $user_id);
    $smarty->assign('user_id_pom', $user_id_pom);
    $smarty->assign('first_name', $first_name);
    $smarty->assign('last_name', $last_name);
    $smarty->assign('username', $username);
    $smarty->assign('email', $email);
    $smarty->assign('privileges', $privileges);
    $smarty->assign('status', $status);
    $smarty->assign('spent', $spent);

    $smarty->assign('change', $change);

    $smarty->assign('all_fields_required', $all_fields_required);
    $smarty->assign('are_you_sure', $are_you_sure);
    $smarty->assign('user_id_delete', $user_id_delete);
    $smarty->display('list-of-users.tpl');
    include 'includes/overall/footer.php';
?>