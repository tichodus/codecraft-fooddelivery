<?php 
    include 'core/functions/cart.php';
    include 'core/functions/food.php';

    $user_id        = $_POST['user_id'];
    $cart_id        = $_POST['food_id'];
    $final_price    = $_POST['final_price'];

    $host_name      = $GLOBALS['host_name'];
    $db_user        = $GLOBALS['db_user'];
    $db_password    = $GLOBALS['db_password'];
    $db_name        = $GLOBALS['db_name'];

    $food = return_item_cart_id($cart_id);
    $food_id = $food[0]->food_ID;

    $meal                   = return_item_cart_id($cart_id);
    $number_of_same_meals   = $meal[0]->duplicate;

    $meal_pom               = return_meal($food_id);
    $meal_price             = $meal_pom[0]->price;
    $meal_price_unit        = $meal_pom[0]->price_unit;

    $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
    if ($con->connect_errno) 
        print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
    else
        $result = $con->query("DELETE FROM cart WHERE cart_id = $cart_id");

    $no = return_items_from_cart($user_id);
    $counter = count($no);
    $response[0] = $counter;

    if($counter == 0)
        $response[1] = 0;
    else
    {
        $final_price = $final_price - ($meal_price * $number_of_same_meals);
        $response[1] = $final_price;
        $response[2] = $meal_price_unit;
    }
    echo json_encode($response);
?>