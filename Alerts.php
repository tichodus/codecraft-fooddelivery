<?php
    class Alerts
    {
        public $admin_alerts_id;
        public $admin_id;
        public $user_id;

        public function __construct($admin_alerts_id, $admin_id, $user_id)
        {
            $this->admin_alerts_id      =   $admin_alerts_id;
            $this->admin_id             =   $admin_id;
            $this->user_id              =   $user_id;
        }
    }
?>