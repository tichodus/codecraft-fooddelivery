/*
    ime baze: catering
    usernme: root
    password:
*/

/* TABELE KREIRATI ISTIM REDOSLEDOM KAO STO SU OVDE NAVEDENE!!! */

CREATE TABLE `companies` (
    `company_name`              varchar(100) NOT NULL,
    `company_description`       varchar(500) NOT NULL,
    `pib`                       int(11) NOT NULL,
    `company_type`              int(1) NOT NULL,
    `type_of_plan`              int(1),
    `breakfast_time`            int(2),
    `lunch_time`                int(2),
    PRIMARY KEY  (`pib`)
) ENGINE=InnoDB;

CREATE TABLE `users` (
    `user_id`               int (11) NOT NULL auto_increment,
    `username`              varchar(32) NOT NULL,
    `password`              varchar(32) NOT NULL,
    `first_name`            varchar(32) NOT NULL,
    `last_name`             varchar(32) NOT NULL,
    `email`                 varchar(1024) NOT NULL,
    `email_code`            varchar(32),
    `active`                int(1) DEFAULT 0,
    `privileges`            int(1) DEFAULT 0,
    `spent`                 int (5) DEFAULT 0,
    `company`               int(11) NOT NULL,
    `automatic_delivery`    int(1) DEFAULT 0,
    `image`                 int(11) DEFAULT 1,
    FOREIGN KEY (company) REFERENCES companies(pib),
    PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `admin_alerts` (
    `admin_alerts_id`           int(11) NOT NULL auto_increment,
    `admin_id`                  int(11) NOT NULL,
    `user_id`                   int(11) NOT NULL,
    FOREIGN KEY (admin_id) REFERENCES users(user_id),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    PRIMARY KEY  (`admin_alerts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `categories` (
    `category_id`               int(11) NOT NULL auto_increment,
    `type`                      varchar(50) NOT NULL,
    PRIMARY KEY  (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

INSERT INTO categories (category_id, type) VALUES
(1, 'salad'),
(2, 'sandwich'),
(3, 'bakery'),
(4, 'pasta'),
(5, 'sweet'),
(6, 'drink'),
(7, 'cooked meal');

CREATE TABLE `food` (
    `food_ID`         int(11) NOT NULL auto_increment,
    `title`           varchar(100) NOT NULL,
    `description`     varchar(2000) DEFAULT NULL,
    `quantity`        int(4)  NOT NULL,
    `unit_of_measure` varchar(20) NOT NULL,
    `price`           int(6) NOT NULL,
    `price_unit`      varchar(20) NOT NULL,
    `image_id`        int(11) NOT NULL,
    `category_id`     int(11) NOT NULL,
    `user_id`         int(11) NOT NULL,
    `rate`            int(1) NOT NUll,
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    PRIMARY KEY  (`food_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `images` (
    `image_id`                  int(11) NOT NULL auto_increment,
    `image_path`                varchar(200) NOT NULL,
    `user_id`                   int(11),
    `food_id`                   int(11),
    FOREIGN KEY (user_id) REFERENCES users(user_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id),
    PRIMARY KEY  (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

INSERT INTO images (image_id, image_path) VALUES (1, 'images/users/profile.png');

CREATE TABLE `orders` (
    `ID`            int(11) NOT NULL auto_increment,
    `IDfood`        int(11) NOT NULL,
    `IDemployee`    int(11) NOT NULL,
    `date`          date NOT NULL,
    `notes`         varchar(1000),
    `company_id`    int(11) NOT NULL,
    `type_of_meal`  varchar(30) NOT NULL,
    `quantity`      int(11) NOT NULL,
    PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `cart` (
    `cart_id`           int(11) NOT NULL auto_increment,
    `user_id`           int(11) NOT NULL,
    `food_ID`           int(11) NOT NULL,
    `duplicate`         int(11) NOT NULL,
    `type_of_service`   varchar(30) NOT NULL,
    `date`              date NOT NULL,
    PRIMARY KEY  (`cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `rates` (
    `rate_id`           int(11) NOT NULL auto_increment,
    `user_id`           int(11) NOT NULL,
    `food_id`           int(11) NOT NULL,
    `rate`              int(1) NOT NULL,
    PRIMARY KEY  (`rate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

CREATE TABLE `comments` (
    `comment_id`        int(11) NOT NULL auto_increment,
    `employee_id`       int(11) NOT NULL,
    `food_id`           int(11) NOT NULL,
    `comment`           varchar(1000) NOT NULL,
    PRIMARY KEY  (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3;

/* company_id -------> JE ID NEKE IT KOMPANIJE */