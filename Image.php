<?php
    class Image
    {
        public $image_id;
        public $image_path;
        public $user_id;
        public $food_id;

        public function __construct($image_id, $image_path="", $user_id, $food_id)
        {
            $this->image_id     =   $image_id;
            $this->image_path   =   $image_path;
            $this->user_id      =   $user_id;
            $this->food_id      =   $food_id;
        }
    }
?>