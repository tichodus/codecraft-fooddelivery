<?php
    class Cart
    {
        public $cart_id;
        public $user_id;
        public $food_ID;
        public $duplicate;
        public $type_of_service;
        public $date;

        public function __construct($cart_id, $user_id, $food_ID, $duplicate, $type_of_service="", $date)
        {
            $this->cart_id          =   $cart_id;
            $this->user_id          =   $user_id;
            $this->food_ID          =   $food_ID;
            $this->duplicate        =   $duplicate;
            $this->type_of_service  =   $type_of_service;
            $this->date             =   $date;
        }
    }
?>