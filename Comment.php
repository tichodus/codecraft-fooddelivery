<?php
    class Comment
    {
        public $comment_id;
        public $employee_id;
        public $food_id;
        public $comment;

        public function __construct($comment_id, $employee_id, $food_id, $comment="")
        {
            $this->comment_id       =   $comment_id;
            $this->employee_id      =   $employee_id;
            $this->food_id          =   $food_id;
            $this->comment          =   $comment;
        }
    }
?>