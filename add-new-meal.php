<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    if(logged_in_as_admin($user_data['user_id']) == true && is_catering_company($user_data['company']) == true)
    {
        $no_permission = false;
        $all_fields_required = false;
        $price_must_be_numeric = false;
        $quantity_must_be_numeric = false;
        $img_type_not_suported = false;
        $success = false;
        $categories = return_all_categories();

        $pib = $user_data['company'];

        $month = date("m");
        $year = date("Y");

        $directory = 'images/food/' . $year;
        $precise_directory = 'images/food/' . $year . '/' . $month . '/' . $pib;
        if(!file_exists($directory))
            mkdir('images/food/' . $year);

        if(!file_exists($precise_directory))
            mkdir('images/food/' . $year . '/' . $month . '/' . $pib);

        if(isset($_POST['submit']))
        {
            $unit_of_measure = $_POST['unit_of_measure'];
            $category        = $_POST['category'];

            $image_name     = $_FILES['image']['name'];
            $image_tmp_name = $_FILES['image']['tmp_name'];
            $image_type     = $_FILES['image']['type'];

            if(empty($_POST['title']) || empty($_POST['description']) || empty($_POST['quantity']))
                $all_fields_required = true;
            else if(empty($image_tmp_name))
                $all_fields_required = true;
            else if($unit_of_measure == 'not_selected' || $category == 'not_selected')
                $all_fields_required = true;
            else if(!is_numeric($_POST['quantity']))
                $quantity_must_be_numeric = true;
            else if(!is_numeric($_POST['price']))
                $price_must_be_numeric = true;
            else if($image_type != 'image/png' && $image_type != 'image/jpeg')
                $img_type_not_suported = true;
            else
            {
                if($image_type == 'image/png')
                    $extension = '.png';
                else if($image_type == 'image/jpeg')
                    $extension = '.jpg';

                $title = $_POST['title'];
                $description = $_POST['description'];
                $quantity = $_POST['quantity'];
                $unit_of_measure = $_POST['unit_of_measure'];
                $category = $_POST['category'];
                $price = $_POST['price'];
                $price_unit = $_POST['price_unit'];

                $adjusted_title = str_replace(' ', '-', $title);
                $adjusted_title = str_replace('/', '-', $adjusted_title);

                $path = $precise_directory . '/' . $adjusted_title . $extension;

                insert_image_path($path);
                move_uploaded_file($image_tmp_name, $path);

                $all_images = return_images();
                $img_counter = count($all_images);
                $last_image = $all_images[$img_counter - 1];
                $last_image = $last_image->image_id;

                $user_id = $user_data['user_id'];

                insert_food($adjusted_title, $description, $quantity, $unit_of_measure, $price, $price_unit, $last_image, $category, $user_id, 0);

                $all_meals = return_all_meals();
                $meal_counter = count($all_meals);

                update_image($all_meals[$meal_counter-1]->food_ID, $last_image);

                $users = return_all_users();
                $no_of_users = count($users);

                foreach ($users as $user_id => $user) 
                {
                    $companyyy = return_company($user->company);
                    if($companyyy[0]->company_type == 0)
                        create_rates_for_user($user->user_id, $all_meals[$meal_counter - 1]->food_ID, 0);
                }

                $success = true;
            }
        }

        $smarty->assign('all_fields_required', $all_fields_required);
        $smarty->assign('price_must_be_numeric', $price_must_be_numeric);
        $smarty->assign('success', $success);
        $smarty->assign('quantity_must_be_numeric', $quantity_must_be_numeric);
        $smarty->assign('img_type_not_suported', $img_type_not_suported);
        $smarty->assign('categories', $categories);
    }
    else
        $no_permission = true;
    
    $smarty->assign('no_permission', $no_permission);
    $smarty->display('add-new-meal.tpl');
    include 'includes/overall/footer.php';
?>