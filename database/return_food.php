<?php
	require 'core/db-init.php';

	$user_id = $user_data['user_id'];

	$food_array;
	$date=date('Ymd');

	class all_food
	{
		public $food_ID;
		public $title;
		public $description;
		public $quantity;
		public $unit_of_measure;
		public $price;
		public $price_unit;
		public $image_id;
		public $category_id;
		public $user_id;

		public $comment_array; //bool
		public $comments;
		public $usernames;

		public function all_food($fID, $ti, $des, $quantity, $unit_of_measure, $price, $price_unit, $image_id, $category_id, $user_id)
		{
			$this->food_ID=$fID;
			$this->title=$ti;
			$this->description=$des;
			$this->quantity=$quantity;
			$this->unit_of_measure=$unit_of_measure;
			$this->price=$price;
			$this->price_unit=$price_unit;
			$this->image_id=$image_id;
			$this->category_id=$category_id;
			$this->user_id=$user_id;

			$this->comment_array='FALSE';
			$this->comments='';
			$this->usernames='';
		}
	}

	    $host_name      = $GLOBALS['host_name'];
        $db_user        = $GLOBALS['db_user'];
        $db_password    = $GLOBALS['db_password'];
        $db_name        = $GLOBALS['db_name'];

		$link = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");

		/* check connection */
		if (mysqli_connect_errno()) {
			printf("Connect failed: %s\n", mysqli_connect_error());
			exit();
		}

		
		$query = "SELECT * FROM `food` WHERE 1";
		if (mysqli_multi_query($link, $query)) 
		{

				if ($result = mysqli_use_result($link)) 
				{
					$i=0;
					while ($row = mysqli_fetch_row($result)) 
					{

					
						$food_array[$i]=new all_food($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8], $row[9]);
						$i++;
					}
					mysqli_free_result($result);
				}
				
			
				if (mysqli_more_results($link))
					printf("-----------------\n");
	
		}


	for ($i=0; $i < count($food_array) ; $i++) { 
			
		$id_f=$food_array[$i]->food_ID;
		$query = "SELECT `date` FROM `orders` WHERE IDfood= $id_f AND IDemployee=$user_id";
		if (mysqli_multi_query($link, $query)) {

				
				if ($result = mysqli_use_result($link)) {
					
					while ($row = mysqli_fetch_row($result)) {

					 $row[0] = str_replace("-", "", $row[0]);



						if($row[0]<$date)
						{
							$food_array[$i]->comment_array='TRUE';

						}



				}
					
				mysqli_free_result($result);
					
				}
				
				
				if (mysqli_more_results($link)) {
					printf("-----------------\n");
				}
	
		}

	}


	for ($i=0; $i < count($food_array) ; $i++) { 
			
		$id_f=$food_array[$i]->food_ID;
		$query = "SELECT `comment`, `IDemployee` FROM `comments` WHERE IDfood=$id_f";
		$j=0;
		if (mysqli_multi_query($link, $query)) {

				
				if ($result = mysqli_use_result($link)) {
					
					while ($row = mysqli_fetch_row($result)) {

						$link1 = mysqli_connect("$host_name", "$db_user", "$db_password", "$db_name");
						$query1="SELECT `username` FROM `users` WHERE user_id=$row[1]";

						if (mysqli_multi_query($link1, $query1))
						{
							//die();
							if ($result1 = mysqli_use_result($link1)) {
					
							while ($row1 = mysqli_fetch_row($result1)) {

							$food_array[$i]->usernames[$j]=$row1[0];
							}
						}
						}

						$food_array[$i]->comments[$j]=$row[0];

						$j++;
				}
					
				mysqli_free_result($result);
					
				}
				
				
				if (mysqli_more_results($link)) {
					printf("-----------------\n");
				}

		}

	}
		mysqli_close($link);
?>