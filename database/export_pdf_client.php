<?php

include '../FoodOrders.php';
require('fpdf.php');

	$day_sel=$_POST['days_display'];
	$company_sel=$_POST['choose_company'];
	$selected_employee=$_POST['choosed_employee'];

	if(isset($_POST['exportPDF']))
	{
		$array=return_food_orders($day_sel, $company_sel);
		
	if ($selected_employee==0) {
		$pdf= new FPDF();
		$pdf->AddPage();
		$pdf->SetFont('Arial','B',12);
		$pdf->SetFillColor(51,153,255);
		$pdf->SetTextColor(255,255,255);
		$pdf->Cell(38,10,'Title',1,0,'L',true);
		//$pdf->Cell(31.5,10,'Description',1,0,'L',true);
		$pdf->Cell(38,10,'Measure',1,0,'L',true);
		$pdf->Cell(38,10,'Quantity',1,0,'L',true);
		$pdf->Cell(38,10,'Price',1,0,'L',true);
		$pdf->Cell(38,10,'Date',1,1,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(190,10,"",1,1,'L',true);

		$pdf->SetFillColor(204,229,255);
				
		
		for ($i=0; $i < count($array); $i++) 
		{ 
			$pdf->Cell(38,10,$array[$i]->title,1,0,'L',true);
			//$pdf->Cell(31.5,10,$array[$i]->description,1,0,'L',true);
			$pdf->Cell(38,10,$array[$i]->quantity * $array[$i]->quantity_orders,1,0,'L',true);
			$pdf->Cell(38,10,$array[$i]->unit_of_measure,1,0,'L',true);
			$pdf->Cell(38,10,$array[$i]->price * $array[$i]->quantity_orders." ".$array[$i]->price_unit,1,0,'L',true);
			$pdf->Cell(38,10,$array[$i]->date,1,1,'L',true);

		}
		
		$all_price=0;
		$unit='';
	    foreach ($array as &$value) 
	    {
	       $all_price+=($value->price * $value->quantity_orders);
	       $unit=$array[0]->price_unit;
	    }
	    $pdf->Cell(80,10,"Total price: ".$all_price." ".$unit."",'B',1,'C',true);

		$pdf->Output();
		}
	else
	{
			$all_price=0;
			$unit='';
			$pdf= new FPDF();
			$pdf->AddPage();
			$pdf->SetFont('Arial','B',12);
			$pdf->SetFillColor(51,153,255);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell(38,10,'Title',1,0,'L',true);
			//$pdf->Cell(31.5,10,'Description',1,0,'L',true);
			$pdf->Cell(38,10,'Measure',1,0,'L',true);
			$pdf->Cell(38,10,'Quantity',1,0,'L',true);
			$pdf->Cell(38,10,'Price',1,0,'L',true);
			$pdf->Cell(38,10,'Date',1,1,'L',true);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(190,10,"",1,1,'L',true);

			$pdf->SetFillColor(204,229,255);
					
			
			foreach ($array as &$value) {
	         	if ($value->IDemployee==$selected_employee) {
					$pdf->Cell(38,10,$value->title,1,0,'L',true);
					//$pdf->Cell(31.5,10,$array[$i]->description,1,0,'L',true);
					$pdf->Cell(38,10,$value->quantity * $value->quantity_orders,1,0,'L',true);
					$pdf->Cell(38,10,$value->unit_of_measure,1,0,'L',true);
					$pdf->Cell(38,10,$value->price * $value->quantity_orders." ".$value->price_unit,1,0,'L',true);
					$pdf->Cell(38,10,$value->date,1,1,'L',true);
					$all_price+=($value->price * $value->quantity_orders);
		       		$unit=$value->price_unit;

				}
			}
			
		    $pdf->Cell(80,10,"Total price: ".$all_price." ".$unit."",'B',1,'C',true);

			$pdf->Output();
	}
		
	

}
	
?>