-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2016 at 10:16 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `catering`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_alerts`
--

CREATE TABLE `admin_alerts` (
  `admin_alerts_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `food_ID` int(11) NOT NULL,
  `duplicate` int(11) NOT NULL,
  `type_of_service` varchar(30) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `food_ID`, `duplicate`, `type_of_service`, `date`) VALUES
(11, 5, 5, 1, 'lunch', '2016-09-19');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `type`) VALUES
(1, 'salad'),
(2, 'sandwich'),
(3, 'bakery'),
(4, 'pasta'),
(5, 'sweet'),
(6, 'drink'),
(7, 'cooked meal');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `comment` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `employee_id`, `food_id`, `comment`) VALUES
(1, 4, 1, 'Dobro jelo :)'),
(2, 3, 3, 'fino :)');

-- --------------------------------------------------------

--
-- Table structure for table `companies`
--

CREATE TABLE `companies` (
  `company_name` varchar(100) NOT NULL,
  `company_description` varchar(500) NOT NULL,
  `pib` int(11) NOT NULL,
  `company_type` int(1) NOT NULL,
  `type_of_plan` int(1) DEFAULT NULL,
  `breakfast_time` int(2) DEFAULT NULL,
  `lunch_time` int(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `companies`
--

INSERT INTO `companies` (`company_name`, `company_description`, `pib`, `company_type`, `type_of_plan`, `breakfast_time`, `lunch_time`) VALUES
('Ketering kompanija', 'Mi smo Ketering kompanija', 123456789, 1, NULL, NULL, NULL),
('Ketering 2', 'Ketering 2', 456456456, 1, NULL, NULL, NULL),
('IT kompanija', 'IT kompanija', 789789789, 0, 2, 3, 11);

-- --------------------------------------------------------

--
-- Table structure for table `delivery`
--

CREATE TABLE `delivery` (
  `delivery_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `food`
--

CREATE TABLE `food` (
  `food_ID` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(2000) DEFAULT NULL,
  `quantity` int(4) NOT NULL,
  `unit_of_measure` varchar(20) NOT NULL,
  `price` int(6) NOT NULL,
  `price_unit` varchar(20) NOT NULL,
  `image_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `food`
--

INSERT INTO `food` (`food_ID`, `title`, `description`, `quantity`, `unit_of_measure`, `price`, `price_unit`, `image_id`, `category_id`, `user_id`, `rate`) VALUES
(3, 'Burgerr', 'Mauris sagittis non massa non vestibulum. Praesent id ornare dolor. Vestibulum non est eu nisl pharetra congue vel et sem. Mauris at lacinia nisi, in aliquet massa. Phasellus auctor libero vitae justo condimentum, nec gravida ante placerat. Proin porttitor mi quis facilisis volutpat. Praesent placerat pulvinar felis. Nullam metus lorem, feugiat vitae dapibus sed, sollicitudin vel massa. Vestibulum eget sagittis mi. Curabitur blandit dignissim erat sed aliquam. Sed ornare orci id dolor blandit, at maximus enim luctus. Aenean augue eros, aliquam id ligula et, egestas ultrices orci. Cras vestibulum at nibh ut volutpat. Cras ultrices, ipsum ut dictum commodo, mauris enim consectetur massa, varius auctor justo lacus id neque. In hac habitasse platea dictumst.', 2500, 'grams', 155, 'eur', 11, 2, 4, 0),
(5, 'Spaghetti', 'Mauris sagittis non massa non vestibulum. Praesent id ornare dolor. Vestibulum non est eu nisl pharetra congue vel et sem. Mauris at lacinia nisi, in aliquet massa. Phasellus auctor libero vitae justo condimentum, nec gravida ante placerat. Proin porttitor mi quis facilisis volutpat. Praesent placerat pulvinar felis. Nullam metus lorem, feugiat vitae dapibus sed, sollicitudin vel massa. Vestibulum eget sagittis mi. Curabitur blandit dignissim erat sed aliquam. Sed ornare orci id dolor blandit, at maximus enim luctus. Aenean augue eros, aliquam id ligula et, egestas ultrices orci. Cras vestibulum at nibh ut volutpat. Cras ultrices, ipsum ut dictum commodo, mauris enim consectetur massa, varius auctor justo lacus id neque. In hac habitasse platea dictumst.', 250, 'grams', 15, 'eur', 13, 4, 1, 0),
(6, 'Meatloaf', 'Mauris sagittis non massa non vestibulum. Praesent id ornare dolor. Vestibulum non est eu nisl pharetra congue vel et sem. Mauris at lacinia nisi, in aliquet massa. Phasellus auctor libero vitae justo condimentum, nec gravida ante placerat. Proin porttitor mi quis facilisis volutpat. Praesent placerat pulvinar felis. Nullam metus lorem, feugiat vitae dapibus sed, sollicitudin vel massa. Vestibulum eget sagittis mi. Curabitur blandit dignissim erat sed aliquam. Sed ornare orci id dolor blandit, at maximus enim luctus. Aenean augue eros, aliquam id ligula et, egestas ultrices orci. Cras vestibulum at nibh ut volutpat. Cras ultrices, ipsum ut dictum commodo, mauris enim consectetur massa, varius auctor justo lacus id neque. In hac habitasse platea dictumst.', 250, 'grams', 18, 'eur', 15, 2, 1, 0),
(7, 'Fries', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut ultricies quis tellus ac vehicula. Mauris fringilla augue ac mi gravida, pulvinar tempus neque pretium. Quisque eget erat dignissim, hendrerit erat sit amet, dignissim lacus. Donec quis ligula turpis. Mauris dictum sed nibh a eleifend. Integer ornare, lorem vel vestibulum efficitur, dui lorem viverra lorem, eget ultrices ex dolor eget libero. Nullam est nulla, vestibulum eu efficitur in, consequat eget purus. Donec mattis lacus quis nibh accumsan commodo. Nulla facilisi. Quisque mollis ligula quis interdum mattis. Cras ut quam ut dui sollicitudin aliquet molestie vel neque. Proin a nunc vestibulum, dignissim velit sed, egestas arcu.', 45, 'grams', 3, 'eur', 16, 2, 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE `images` (
  `image_id` int(11) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `food_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`image_id`, `image_path`, `user_id`, `food_id`) VALUES
(1, 'images/users/profile.png', NULL, NULL),
(11, 'images/food/2016/09/456456456/Burger.jpg', NULL, 3),
(13, 'images/food/2016/09/123456789/Spaghetti.jpg', NULL, 5),
(14, 'images/users/marko.jpg', 1, NULL),
(15, 'images/food/2016/09/123456789/Meatloaf.jpg', NULL, 6),
(16, 'images/food/2016/09/456456456/Fries.jpg', NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `ID` int(11) NOT NULL,
  `IDfood` int(11) NOT NULL,
  `IDemployee` int(11) NOT NULL,
  `date` date NOT NULL,
  `notes` varchar(1000) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `type_of_meal` varchar(30) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`ID`, `IDfood`, `IDemployee`, `date`, `notes`, `company_id`, `type_of_meal`, `quantity`) VALUES
(6, 3, 3, '2016-09-18', '', 789789789, 'breakfast', 1),
(8, 5, 5, '2016-09-18', '', 789789789, 'lunch', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `rate_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `food_id` int(11) NOT NULL,
  `rate` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`rate_id`, `user_id`, `food_id`, `rate`) VALUES
(1, 3, 3, 0),
(3, 3, 5, 0),
(4, 3, 6, 0),
(5, 5, 3, 0),
(6, 5, 5, 0),
(7, 5, 6, 0),
(8, 3, 7, 0),
(9, 5, 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(32) NOT NULL,
  `last_name` varchar(32) NOT NULL,
  `email` varchar(1024) NOT NULL,
  `email_code` varchar(32) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `privileges` int(1) DEFAULT '0',
  `spent` int(5) DEFAULT '0',
  `company` int(11) NOT NULL,
  `automatic_delivery` int(1) DEFAULT '0',
  `image` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `first_name`, `last_name`, `email`, `email_code`, `active`, `privileges`, `spent`, `company`, `automatic_delivery`, `image`) VALUES
(1, 'marko', '202cb962ac59075b964b07152d234b70', 'Marko', 'Stefanovic', 'marko@gmail.com', NULL, 1, 1, 0, 123456789, 0, 14),
(3, 'nemanja', '202cb962ac59075b964b07152d234b70', 'Nemanja', 'Tanaskovic', 'nemanja@gmail.com', NULL, 1, 1, 0, 789789789, 0, 1),
(4, 'stefan', '202cb962ac59075b964b07152d234b70', 'Stefan', 'Milovanovic', 'stefan@gmail.com', NULL, 1, 1, 0, 456456456, 0, 1),
(5, 'nemanja1', '202cb962ac59075b964b07152d234b70', 'nemanja1', 'nemanja1', 'nemanja1', NULL, 1, 0, 0, 789789789, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_alerts`
--
ALTER TABLE `admin_alerts`
  ADD PRIMARY KEY (`admin_alerts_id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`pib`);

--
-- Indexes for table `delivery`
--
ALTER TABLE `delivery`
  ADD PRIMARY KEY (`delivery_id`),
  ADD KEY `food_id` (`food_id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`food_ID`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`image_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `food_id` (`food_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `company` (`company`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_alerts`
--
ALTER TABLE `admin_alerts`
  MODIFY `admin_alerts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `delivery`
--
ALTER TABLE `delivery`
  MODIFY `delivery_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `food`
--
ALTER TABLE `food`
  MODIFY `food_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `image_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `rates`
--
ALTER TABLE `rates`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_alerts`
--
ALTER TABLE `admin_alerts`
  ADD CONSTRAINT `admin_alerts_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `admin_alerts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `delivery`
--
ALTER TABLE `delivery`
  ADD CONSTRAINT `delivery_ibfk_1` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_ID`),
  ADD CONSTRAINT `delivery_ibfk_2` FOREIGN KEY (`company_id`) REFERENCES `companies` (`pib`);

--
-- Constraints for table `food`
--
ALTER TABLE `food`
  ADD CONSTRAINT `food_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `images_ibfk_2` FOREIGN KEY (`food_id`) REFERENCES `food` (`food_ID`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`company`) REFERENCES `companies` (`pib`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
