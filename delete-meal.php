<?php
    include 'core/init.php';
    include 'includes/overall/header.php';

    if(logged_in_as_admin($user_data['user_id']) == true && is_catering_company($user_data['company']) == true)
    {
        $change = false;
        $all_fields_required = false;
        $are_you_sure = false;
        $meal_id = 0;
        $meal_title = '';
        $meal_description = '';
        $meal_quantity = 0;
        $meal_unit_of_measure = '';
        $meal_price = 0;
        $meal_category_type = '';
        $meal_category_id = 0;

        $price_must_be_numeric = false;
        $quantity_must_be_numeric = false;
        $img_type_not_suported = false;

        $categories = return_all_categories();

        $user_id = $user_data['user_id'];
        $meals = return_meals($user_id);

        if(isset($_POST['change_button']))
        {
            $change = true;
            $meal_id = $_POST['change_button'];
            $meal = return_meal($meal_id);

            $meal_title = $meal[0]->title;
            $meal_description = $meal[0]->description;
            $meal_quantity = $meal[0]->quantity;
            $meal_unit_of_measure = $meal[0]->unit_of_measure;
            $meal_price = $meal[0]->price;

            $category_id = $meal[0]->category_id;
            $meal_category = return_category($category_id);

            $meal_category_id   = $meal_category[0]->category_id;
            $meal_category_type = $meal_category[0]->type;
        }

        if(isset($_POST['change_final']))
        {
            $meal = return_meal($_POST['change_final']);
            $image = get_image($meal[0]->image_id);

            $meal_id = $_POST['change_final'];

            $unit_of_measure = $_POST['unit_of_measure'];
            $category        = $_POST['category'];

            $image_name     = $_FILES['image']['name'];
            $image_tmp_name = $_FILES['image']['tmp_name'];
            $image_type     = $_FILES['image']['type'];

            if(empty($_POST['title']) || empty($_POST['description']) || empty($_POST['quantity']))
                $all_fields_required = true;
            else if(empty($image_tmp_name))
                $all_fields_required = true;
            else if($unit_of_measure == 'not_selected' || $category == 'not_selected')
                $all_fields_required = true;
            else if(!is_numeric($_POST['quantity']))
                $quantity_must_be_numeric = true;
            else if(!is_numeric($_POST['price']))
                $price_must_be_numeric = true;
            else if($image_type != 'image/png' && $image_type != 'image/jpeg')
                $img_type_not_suported = true;
            else
            {
                if($image_type == 'image/png')
                    $extension = '.png';
                else if($image_type == 'image/jpeg')
                    $extension = '.jpg';

                $title = $_POST['title'];
                $description = $_POST['description'];
                $quantity = $_POST['quantity'];
                $unit_of_measure = $_POST['unit_of_measure'];
                $category = $_POST['category'];
                $price = $_POST['price'];
                $price_unit = $_POST['price_unit'];

                $adjusted_title = str_replace(' ', '-', $title);
                $adjusted_title = str_replace('/', '-', $adjusted_title);

                move_uploaded_file($image_tmp_name, $image[0]->image_path);
                update_meal($title, $description, $quantity, $unit_of_measure, $category, $price, $price_unit, $meal_id);

                $meals = return_meals($user_id);
            }
        }

        if(isset($_POST['delete_button']))
        {
            $are_you_sure = true;
            $meal_id    = $_POST['delete_button'];
            $meal       = return_meal($meal_id);
            $meal_title = $meal[0]->title;
        }

        if(isset($_POST['no_button']))
        {
            $are_you_sure = false;
            //$change = true;
        }

        if(isset($_POST['yes_button']))
        {
            $meal_id    = $_POST['yes_button'];
            $meal       = return_meal($meal_id);
            $img_id     = $meal[0]->image_id;

            delete_image($img_id);
            delete_meal($meal_id);
            delete_rates($meal_id);

            $meals = return_meals($user_id);
        }

        $smarty->assign('are_you_sure', $are_you_sure);
        $smarty->assign('meal_category_type', $meal_category_type);
        $smarty->assign('categories', $categories);
        $smarty->assign('meal_unit_of_measure', $meal_unit_of_measure);
        $smarty->assign('meal_quantity', $meal_quantity);
        $smarty->assign('meal_id', $meal_id);
        $smarty->assign('meal_title', $meal_title);
        $smarty->assign('all_fields_required', $all_fields_required);
        $smarty->assign('meal_description', $meal_description);
        $smarty->assign('meals', $meals);
        $smarty->assign('meal_category_id', $meal_category_id);
        $smarty->assign('meal_price', $meal_price);
        $smarty->assign('change', $change);

        $smarty->assign('price_must_be_numeric', $price_must_be_numeric);
        $smarty->assign('quantity_must_be_numeric', $quantity_must_be_numeric);
        $smarty->assign('img_type_not_suported', $img_type_not_suported);
    }
    else
        $no_permission = true;

    $smarty->assign('no_permission', $no_permission);
    $smarty->display('delete-meal.tpl');
    include 'includes/overall/footer.php';
?>