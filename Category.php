<?php
    class Category
    {
        public $category_id;
        public $type;

        public function __construct($category_id, $type="")
        {
            $this->category_id  =   $category_id;
            $this->type         =   $type;
        }
    }
?>