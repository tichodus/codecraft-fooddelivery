<?php
    class Company
    {
        public $company_name;
        public $company_description;
        public $pib;
        public $company_type;
        public $type_of_plan;
        public $breakfast_time;
        public $lunch_time;

        public function __construct($company_name="", $company_description="", $pib, $company_type, $type_of_plan, $breakfast_time, $lunch_time)
        {
            $this->company_name            =   $company_name;
            $this->company_description     =   $company_description;
            $this->pib                     =   $pib;
            $this->company_type            =   $company_type;
            $this->type_of_plan            =   $type_of_plan;
            $this->breakfast_time          =   $breakfast_time;
            $this->lunch_time              =   $lunch_time;
        }
    }
?>