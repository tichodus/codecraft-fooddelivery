<?php
    class Order
    {
        public $ID;
        public $IDfood;
        public $date;
        public $notes;
        public $company_id;
        public $type_of_meal;
        public $quantity;

        public function __construct($ID, $IDfood, $date, $notes="", $company_id, $type_of_meal="", $quantity)
        {
            $this->ID           =   $ID;
            $this->IDfood       =   $IDfood;
            $this->date         =   $date;
            $this->notes        =   $notes;
            $this->company_id   =   $company_id;
            $this->type_of_meal =   $type_of_meal;
            $this->quantity     =   $quantity;
        }
    }
?>