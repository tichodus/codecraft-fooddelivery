<?php 
    include 'core/functions/cart.php';

    $user_id            = $_POST['user_id'];
    $food_id            = $_POST['food_id'];
    $type_of_service    = $_POST['type_of_service'];
    $order_date         = $_POST['order_date'];
    $today_day          = date('d');

    if($order_date > $today_day)
        $date=date('Ym').$order_date;
    else
        $date=date('Y').(date('m')+1).$order_date;

    $host_name      = $GLOBALS['host_name'];
    $db_user        = $GLOBALS['db_user'];
    $db_password    = $GLOBALS['db_password'];
    $db_name        = $GLOBALS['db_name'];

    if(item_exists($food_id, $type_of_service, $date) == true)
    {
        $duplicate = return_items_with_food_id($food_id);
        $number_of_same_meals = $duplicate[0]->duplicate;
        $number_of_same_meals = $number_of_same_meals + 1;

        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $result = $con->query("UPDATE cart set duplicate = $number_of_same_meals WHERE food_ID = $food_id AND type_of_service = '$type_of_service' AND `date` = $date");
    }
    else
    {
        $number_of_same_meals = 1;
        $con = new mysqli("$host_name", "$db_user", "$db_password", "$db_name");
        if ($con->connect_errno) 
            print ("Connection error (" . $con->connect_errno . "): $con->connect_error");
        else
            $result = $con->query("INSERT INTO cart (food_ID, user_id, duplicate, type_of_service, `date`) VALUES ($food_id, $user_id, $number_of_same_meals, '$type_of_service', $date)");
    }

    $no = return_items_from_cart($user_id);
    $counter = count($no);
    echo json_encode($counter);
?>